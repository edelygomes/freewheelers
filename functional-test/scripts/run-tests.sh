#!/bin/bash
set -e

HEADLESS_MODE="${HEADLESS:-false}"
echo "Headless mode set to: $HEADLESS_MODE"

FT_DIR=$(
  cd $(dirname "$dir")
  pwd
)
ROOT_DIR=$(
  cd $FT_DIR/..
  pwd
)

LOG_DIR=$FT_DIR/logs
API_LOG_FILE=$LOG_DIR/ft-api.log
UI_LOG_FILE=$LOG_DIR/ft-ui.log

API_PORT=8080
UI_PORT=3000

create_log_files() {
  if [ ! -e "$UI_LOG_FILE" ] || [ ! -e "$API_LOG_FILE" ]; then
    mkdir -p "$LOG_DIR"
    touch "$UI_LOG_FILE"
    touch "$API_LOG_FILE"
  fi
}

port_is_up() {
  # using this instead of nc/netcat because it needs to work on CentOS (build machine)
  (echo >/dev/tcp/localhost/$1) &>/dev/null && true || false;
}

ui_is_up() {
  port_is_up $UI_PORT
}

api_is_up() {
  port_is_up $API_PORT
}

kill_api_if_already_running() {
  # To guarantee that freewheelers_test DB is used
  if api_is_up; then
    echo "Found a local running instance of Freewheelers API on port 8080. Killing that process for a new one to be restarted..."
    API_PID=$(ps -ef | grep bootRun | grep -v grep | awk -F' ' '{print $2}')
    kill -9 $API_PID || true
    sleep 3
  fi
}

startup_api() {
  cd $ROOT_DIR/api
  ENV=test ./gradlew prepareTestDb bootRun
}

startup_ui() {
  if ! ui_is_up; then
    cd $ROOT_DIR/ui
    npm install --only=prod
    BROWSER=none npm start
  fi
}

wait_for_servers() {
  echo -e "Waiting for servers to be up..."
  while ! ui_is_up || ! api_is_up; do
    printf '.'
    sleep 1
  done
  echo -e "\nServers are up!\n"
}

cleanup() {
  API_PID=$(ps -ef | grep bootRun | grep -v grep | awk -F' ' '{print $2}')
  UI_PID=$(ps -ef | grep start.js | grep -v grep | awk -F' ' '{print $2}')

  kill -9 $API_PID $UI_PID
}

trap cleanup EXIT
create_log_files
kill_api_if_already_running
startup_api >"$API_LOG_FILE" 2>&1 &
startup_ui >$UI_LOG_FILE 2>&1 &
wait_for_servers

echo "Running functional tests..."
if [ "$HEADLESS_MODE" == "true" ]; then
    npm run functional-test-headless
else
    npm run functional-test
fi
