# Freewheelers Admin Journey Spec

## Admin can manage orders
* Account "ft_user@example.com" exists
* User "ft_user@example.com" reserves "Scattante XRL Comp Road Bike"
* Go to Freewheelers homepage
* Click "Login"
* Login with "admin@freewheelers.bike", "Yellow bikes are just amazingly awesome, right? Says Robert, my good friend!"
* Click "Admin"
* See item "Scattante XRL Comp Road Bike" reserved by "ft_user@example.com"
* Fill update order notes with "some note"
* See "some note" updated in order list
* Click "Logout"

## Admin can manage items
* Go to Freewheelers homepage
* Click "Login"
* Login with "admin@freewheelers.bike", "Yellow bikes are just amazingly awesome, right? Says Robert, my good friend!"
* Click "Admin"
* Click "Manage Items"
* Submit create item form with "New Item", "description", "FRAME", "100", "99.99"
* See "New Item" in item list
* Submit update item form with "New Item2", "description2", "ACCESSORIES", "200", "200.99"
* See "New Item2" in item list
* Delete Item in item list
* Can't see "New Item2" in item list
* Submit create item form with "Last Item", "Only 1 item left", "FRAME", "1", "99.99"
* See "Last item" in item list
* Click "Logout"