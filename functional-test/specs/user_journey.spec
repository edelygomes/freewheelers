# Freewheelers User Journey Spec

## New customer can create new account and see profile
* Go to Freewheelers homepage
* Click "Create Account"
* Fill create account form with "new_user@example.com", "Password123", "New User", "1234567890", "UK", "London", "ABC 123", "Elm Street", "97", "402"
* Submit create account form
* Login with "new_user@example.com", "Password123"
* See "Welcome new_user@example.com"
* Click "Profile"
* See "new_user@example.com", "New User", "1234567890", "UK", "London", "ABC 123", "Elm Street", "97", "402" in profile page
* Click "Logout"
* See "Login"

## Unauthenticated visitor can see items on homepage
* Go to Freewheelers homepage
* See items available for reservation

## Authenticated customer can reserve an item
* Account "ft_user@example.com" exists
* Go to Freewheelers homepage
* Click "Login"
* Login with "ft_user@example.com", "Password123"
* See items available for reservation
* Reserve item "Scattante XRL Comp Road Bike"
* See order confirmation for "Scattante XRL Comp Road Bike"
* Click "Profile"
* See "Scattante XRL Comp Road Bike"
* Click "Logout"
