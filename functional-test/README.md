Functional Tests for Freewheelers
=================================

## Prerequisites:

- install packages: `npm install`

## To run tests locally

1. Make sure you followed the README for `api` and `ui` projects.

2. In separate tabs (`CMD-T` in iTerm) or panels (`CMD-D` in iTerm),

    Prepare a clean test DB with some seed data, and start up the API with the `test` profile. (The `test` profile ensures that the API uses the test database)
   
    ```
    cd ../api
    ENV=test ./gradlew prepareTestDb bootRun
      ```

    Start up the UI:
    
    ```
    cd ../ui
    npm start
    ```

3. Run functional tests:


   To run the functional tests with npm:
   ```
   // run all the scenarios in all the .spec files in the specs/ directory:
   npm run functional-test

   // run all scenarios in a specs/user_journey.spec
   npm run functional-test -- specs/user_journey.spec

   // run only the scenario named on line 15 of specs/user_journey.spec:
   npm run functional-test -- specs/user_journey.spec:15

   // run specs or scenarios in headless mode (without opening Chromium browser):
   npm run functional-test-headless
   npm run functional-test-headless -- specs/user_journey.spec
   npm run functional-test-headless -- specs/user_journey.spec:15
   ``` 
  
   To run the tests with Gauge:
   ```
   // install Gauge
   brew install gauge

   // run specs and scenarios
   gauge run
   gauge run specs/user_journey.spec
   gauge run specs/user_journey.spec:15
   ```

### ... or have the script do everything for you 
If you want to avoid the multiple manual steps, or just want an easy way to verify tests are passing before pushing. It is also the same script that is used in the build.
```
// with Chromium browser:
./scripts/run-tests.sh

// headless:
HEADLESS=true ./scripts/run-tests.sh
```

### Loading custom data

```
run `psql -h localhost -U freewheelers --password -d ${DB_NAME} < api/db/sample_data.sql`
```
