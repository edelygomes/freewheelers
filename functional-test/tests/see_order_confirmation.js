"use strict";

const {text} = require('taiko');
const expect = require('expect');

step("See order confirmation for <itemName>", async function(itemName) {
  expect(await text('Item reserved!').exists()).toBe(true);
  expect(await text('Order #').exists()).toBe(true);
  expect(await text(itemName).exists()).toBe(true);
});
