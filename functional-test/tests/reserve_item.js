"use strict";

const {click, below, goto, write, button, textBox, scrollUp} = require('taiko');

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';


step("Reserve item <itemName>", async function(itemName) {
    await click('Reserve', below(itemName));
});

step("User <email> reserves <itemName>", async function(email, itemName) {

    await goto(`${BASE_URL}/`);
    await click("Login");

    await write(email, textBox('Email'));
    await write("Password123", textBox('Password'));
    await click(button('Login'));

    await click('Reserve', below(itemName));

    await scrollUp();
    await click("Logout");
});