"use strict";

const {text} = require('taiko');
const expect = require('expect');

step("See <expectedText>", async function(expectedText) {
    expect(await text(expectedText).exists()).toBe(true);
});