"use strict";

const {closeBrowser} = require('taiko');

afterSuite(async () => {
    await closeBrowser();
});