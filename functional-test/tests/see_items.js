"use strict";

const {text, below, scrollUp} = require('taiko');
const expect = require('expect');

step("See items available for reservation", async function() {
    expect(await text("In stock:").exists()).toBe(true);
    expect(await text("£ ").exists()).toBe(true);
});

step("See <itemName> in item list", async function(itemName) {
    expect(await text(itemName, below('Items')).exists()).toBe(true);
});

step("Can't see <expectedText> in item list", async function(expectedText) {
    expect(await text(expectedText, below('Items')).exists()).toBe(false);

    await  scrollUp();
});
