"use strict";

const {openBrowser} = require('taiko');

const headless = process.env.HEADLESS_CHROME === 'true';

beforeSuite(async () => {
    await openBrowser({headless: headless, args: ['--no-sandbox', '--disable-setuid-sandbox']});
});