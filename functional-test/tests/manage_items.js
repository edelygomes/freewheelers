"use strict";

const { click, button, write, textBox, clear, dropDown, focus, checkBox, scrollUp } = require('taiko');

step("Delete Item in item list", async function() {
    // https://github.com/getgauge/taiko/issues/511
    await click(checkBox({ class: 'update-items__item-checkbox' }))
    await click(button('Delete all selected items'));
});

step(
    "Submit create item form with <name>, <description>, <type>, <quantity>, <price>",
    async function(name, description, type, quantity, price) {

        await clear(textBox('Name:'));
        await write(name);

        await clear(textBox('Description:'));
        await write(description);

        await focus(dropDown('Type:'));
        await write(type);

        await clear(textBox('Quantity:'));
        await write(quantity);

        await clear(textBox('Price:'));
        await write(price);

        await click(button('Create Item'));

        await scrollUp();
    }
);

step(
    "Submit update item form with <name>, <description>, <type>, <quantity>, <price>",
    async function(name, description, type, quantity, price) {

        // https://github.com/getgauge/taiko/issues/511
        await click(checkBox({ class: 'update-items__item-checkbox' }))

        await clear(textBox({ class: 'update-items__name-input' }));
        await write(name);

        await clear(textBox({ class: 'update-items__description-input' }));
        await write(description);

        await focus(dropDown({ class: 'update-items__type-select' }));
        await write(type);

        await clear(textBox({ class: 'update-items__quantity-input' }));
        await write(quantity);

        await clear(textBox({ class: 'update-items__price-input' }));
        await write(price);

        await click(button('Update all selected items'));

        await scrollUp();
    }
);
