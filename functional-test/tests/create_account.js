"use strict";

const {click, clear, textBox, dropDown, button, write, select, text, goto} = require('taiko');

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

async function fillCreateAccountForm(email, password, name, phoneNumber, country, city, zipcode, street, streetNumber, unitnumber) {

    await clear(textBox('Email'));
    await write(email);

    await clear(textBox('Password'));
    await write(password);

    await clear(textBox('Name'));
    await write(name);

    await clear(textBox('Phone Number'));
    await write(phoneNumber);

    await dropDown('Country').select(country);

    await clear(textBox('City'))
    await write(city)

    await clear(textBox('Zipcode'))
    await write(zipcode)

    await clear(textBox('Street'))
    await write(street)

    await clear(textBox('Street Number'))
    await write(streetNumber)

    await clear(textBox('Unit Number'))
    await write(unitnumber)

}

async function cannotLoginAsUser(email) {
    await goto(`${BASE_URL}/`);

    await click("Login");
    await write(email, textBox('Email'));
    await write("Password123", textBox('Password'));
    await click(button('Login'));

    return await text("Your login attempt was not successful, try again.").exists();
}

step("Account <email> exists", async function(email) {

    await goto(`${BASE_URL}/`);

    if(await cannotLoginAsUser(email)) {
        await click("Create Account");
        await fillCreateAccountForm(email, "Password123", "My Name", "1234567890", "UK", "London", "ABC 123", "Elm Street", "97", "402");
        await click(button('Create Account'));
    } else {
        await click("Logout");
    }    
});

step(
    "Fill create account form with <email>, <password>, <name>, <phoneNumber>, <country>, <city>, <zipcode>, <street>, <streetNumber>, <unitnumber>",
    async function(email, password, name, phoneNumber, country, city, zipcode, street, streetNumber, unitnumber) {
        await fillCreateAccountForm(email, password, name, phoneNumber, country, city, zipcode, street, streetNumber, unitnumber);
    }
);

step("Submit create account form", async function() {
    await click(button('Create Account'));
});
