"use strict";

const {click} = require('taiko');

step("Click <name>", async function(name) {
    await click(name);
});