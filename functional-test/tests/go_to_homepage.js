"use strict";

const {goto} = require('taiko');
const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

step("Go to Freewheelers homepage", async function () {
    await goto(`${BASE_URL}/`);
});