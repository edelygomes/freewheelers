"use strict";

const {text, write, textBox, click, button} = require('taiko');
const expect = require('expect');

step("Login with <email>, <password>", async function(email, password) {
    await write(email, textBox('Email'));
    await write(password, textBox('Password'));
    await click(button('Login'));
});

step("See <email>, <name>, <telephone>, <country>, <city>, <zipcode>, <street>, <streetnumber>, <unitnumber> in profile page", async function(email, name, telephone, country, city,zipcode,street,streetnumber,unitnumber) {
    expect(await text(email).exists()).toBe(true);
    expect(await text(name).exists()).toBe(true);
    expect(await text(telephone).exists()).toBe(true);
    expect(await text(country).exists()).toBe(true);
    expect(await text(zipcode).exists()).toBe(true);
    expect(await text(street).exists()).toBe(true);
    expect(await text(streetnumber).exists()).toBe(true);
    expect(await text(unitnumber).exists()).toBe(true)
});
