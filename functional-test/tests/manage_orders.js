"use strict";

const {text, clear, write, click, textBox, button, toRightOf, scrollUp} = require('taiko');
const expect = require("expect");

step(
    "Fill update order notes with <notes>", async function(notes) {
        await clear(textBox({ class: 'manage-order-table__note' }));
        await write(notes);
        await click(button('Save Changes'));
    }
);

step("See <notes> updated in order list", async function(notes) {
    expect(await text(notes).exists()).toBe(true);
});

step("See item <itemName> reserved by <email>", async function(itemName, email) {
    const itemIsReservedByUser = await text(itemName, toRightOf(email)).exists();
    expect(itemIsReservedByUser).toBe(true);
    await scrollUp();
});

