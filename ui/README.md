# Freewheelers UI

## Prerequisites
Make sure these are installed
* Git
* Node (>= 10) 
* npm ( >= 6.9)

## Tech Stack
* JavaScript (ES6/ES2015)
* [React](https://reactjs.org/)
* JSX
* Jest/Enzyme for testing
* ESLint for linting

This project was bootstrapped with Create React App. You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Installing
In the project directory, you can run:

### `npm install`

Installs the dependencies listed in the `package.json` file.

Required before running the app for the first time or if any dependencies have changed.

## Getting started
### `npm start`
Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) and you should see the Freewheelers homepage, with items (assuming Freewheelers API is running on localhost:8080 and has data).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`
First runs lint, and then launches the test runner in the _interactive_ watch mode.<br />
By default, it runs only the tests for files that have changed since `origin/master`.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run lint:fix`
Automatically fix all your lint errors. (Remember to add them to your commit!)

### Environment variables
The `.env` file is loaded by default. In deployed environments, environment variables are read from `.env` and also the `.env.<ENV_NAME>` file, which will override any previous variables.
 <br /> 

All variables *must* begin with `REACT_APP_` or else they will be ignored; this is a built-in feature/limitation of Create React App.
<br />

Note that these variables are embedded at *build time*. See CRA's [environment configuration documentation](https://create-react-app.dev/docs/adding-custom-environment-variables) for more information.

## Learn More
### `npm run build:<env>`

Exports the relevant environment variables and builds the app for production to the `build` folder.<br />
It bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes.<br />

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

The `react-scripts` dependency is what helps to encapsulate the complex interaction of multiple dependencies (like Webpack, Babel) required to develop, test and bundle a React application. For those who aren’t satisfied with the build tool and configuration choices, or want to explore more advanced configuration, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### From the Create React App documentation:
* [Code Splitting](https://facebook.github.io/create-react-app/docs/code-splitting)
* [Analyzing the Bundle Size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)
* [Making a Progressive Web App](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)
* [Advanced Configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)
* [Deployment](https://facebook.github.io/create-react-app/docs/deployment)


