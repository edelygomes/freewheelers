#!/bin/bash

# Fail the script if an error occurs
set -e

USER=$1
HOST=$2
ENV=$3

TMP_ASSETS_FOLDER=/tmp/ui-build

echo "I am in $(pwd)"
echo "Copying build files to ${ENV} machine ${HOST}:${TMP_ASSETS_FOLDER}..."

if [ ! -e "build" ]; then
  echo "Cannot find build/ folder to scp"
  exit -1
fi

ssh ${USER}@${HOST} "mkdir -p $TMP_ASSETS_FOLDER" && scp -r build/* ${USER}@${HOST}:${TMP_ASSETS_FOLDER}

echo "Copying reactapp.conf to ${ENV} machine ${HOST}:/tmp folder..."
scp scripts/reactapp.conf ${USER}@${HOST}:/tmp

NGINX_CONF_DIR_PATH=/etc/nginx/conf.d
NGINX_CONF_FILE_PATH=$NGINX_CONF_DIR_PATH/reactapp.conf

echo "

  if [ ! -f \"$NGINX_CONF_FILE_PATH\" ]; then
    echo '$NGINX_CONF_FILE_PATH not found, copying into $NGINX_CONF_DIR_PATH'
    sudo mv /tmp/reactapp.conf $NGINX_CONF_DIR_PATH
  else
    echo '$NGINX_CONF_FILE_PATH already exists, removing copy from /tmp'
    rm /tmp/reactapp.conf
  fi

" | ssh ${USER}@${HOST} /bin/bash
