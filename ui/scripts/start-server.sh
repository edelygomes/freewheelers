#!/bin/bash

# Fail the script if an error occurs
set -e

USER=$1
HOST=$2
ENV=$3

echo "
  set -e

  sudo su appuser

  # visibly print to stderr
  function yell () {
   echo -e '\n==================================== \n' >&2
   echo \$@ >&2
   echo -e '\n==================================== \n' >&2
  }

  APP_DIRECTORY=/opt/freewheelers
  UI_APP_DIRECTORY=\$APP_DIRECTORY/ui
  if [ ! -d \"\$UI_APP_DIRECTORY\" ]; then
    echo 'Creating /opt/freewheelers/ui as it does not exist...'
    sudo mkdir -p \$UI_APP_DIRECTORY
    sudo chown -R appuser \$APP_DIRECTORY
  fi

  echo 'Removing previous build files...'
  rm -rf \$UI_APP_DIRECTORY/*

  echo 'Copying new build files into app directory...'
  sudo chown -R appuser /tmp/ui-build
  mv /tmp/ui-build/* \$UI_APP_DIRECTORY

  echo 'Stopping nginx server...'
  sudo service nginx stop

  echo 'Starting nginx server...'
  sudo service nginx start

" | ssh ${USER}@${HOST} /bin/bash
