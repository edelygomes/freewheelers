const cookieMessage =
  'Cookies on this website: We use cookies to give the best experience when you use our website. ' +
  'If you decide to continue using our website without changing the settings, we will assume that you are happy to' +
  ' receive all the cookies from our website.';

export default cookieMessage;
