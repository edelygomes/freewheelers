import axios from 'axios';
import baseURL from './base';

const CreditCardPaymentApi = {
  submitPayment: async creditCard => {
    await axios.post(`${baseURL}/paymentCreditCard`, creditCard);
  }
};

export default CreditCardPaymentApi;
