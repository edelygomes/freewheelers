import axios from 'axios';
import { removeLocalStorage, setLocalStorage } from '../utils/localstorage';
import baseURL from './base';

const LoginApi = {
  login: async ({ email, password }) => {
    const params = new URLSearchParams();
    params.append('username', email);
    params.append('password', password);

    return await axios.post(`${baseURL}/login`, params);
  },
  logout: async () => {
    removeLocalStorage('principal');
    await axios.post(`${baseURL}/logout`);
    window.location.href = '/';
  },
  principal: async () => {
    try {
      const response = await axios.get(`${baseURL}/principal`);
      setLocalStorage('principal', response.data);
    } catch (e) {
      removeLocalStorage('principal');
    }
  }
};

export default LoginApi;
