import axios from 'axios';
import baseURL from './base';

const ItemApi = {
  getAvailableItems: async () => {
    const response = await axios.get(`${baseURL}/items`);
    return response.data.items;
  },

  getItem: async itemId => {
    const response = await axios.get(`${baseURL}/items/${itemId}`);
    return response.data;
  },

  createItem: async item => {
    await axios.post(`${baseURL}/admin/items`, item);
  },

  updateItem: async items => {
    await axios.patch(`${baseURL}/admin/items`, items);
  },

  deleteItems: async itemIds => {
    await axios.delete(`${baseURL}/admin/items`, { data: itemIds });
  }
};

export default ItemApi;
