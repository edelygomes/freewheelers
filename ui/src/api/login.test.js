import axios from 'axios';
import * as localStorage from '../utils/localstorage';
import LoginApi from './login';
jest.mock('axios');
jest.mock('./base', () => 'base.url.com');
jest.mock('../utils/localstorage');

describe('LoginApi', () => {
  describe('login', () => {
    it('calls api to login', async () => {
      await LoginApi.login({});

      expect(axios.post).toHaveBeenCalledWith(
        'base.url.com/login',
        expect.any(URLSearchParams)
      );
    });

    it('throws error if api call fails', async () => {
      axios.post.mockRejectedValue(new Error());

      expect(LoginApi.login({})).rejects.toThrow(Error);
    });
  });

  describe('logout', () => {
    it('calls api, removes localstorage', async () => {
      await LoginApi.logout();

      expect(localStorage.removeLocalStorage).toHaveBeenCalledWith('principal');
      expect(axios.post).toHaveBeenCalledWith('base.url.com/logout');
    });
  });

  describe('principal', () => {
    it('sets principal in localstorage', async () => {
      axios.get.mockResolvedValue({ data: 'security principal' });

      await LoginApi.principal();

      expect(localStorage.setLocalStorage).toHaveBeenCalledWith(
        'principal',
        'security principal'
      );
    });

    it('removes principal in localstorage if there is an error', async () => {
      axios.get.mockRejectedValue(new Error());

      await LoginApi.principal();

      expect(localStorage.removeLocalStorage).toHaveBeenCalledWith('principal');
    });
  });
});
