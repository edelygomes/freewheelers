import axios from 'axios';
import featureToggles from '../mock/featureToggles';
import FeatureTogglesApi from './featureToggles';

jest.mock('axios');
jest.mock('./base', () => 'base.url.com');

describe('FeatureTogglesApi', () => {
  describe('getAllFeatureToggles', () => {
    it('makes a get request to the toggles endpoint', async () => {
      axios.get.mockResolvedValue({ data: featureToggles });

      await FeatureTogglesApi.getAllFeatureToggles();

      expect(axios.get).toHaveBeenCalledWith('base.url.com/toggles');
    });

    it('returns the data payload with a successful request', async () => {
      axios.get.mockResolvedValue({ data: featureToggles });

      const result = await FeatureTogglesApi.getAllFeatureToggles();

      expect(result).toEqual({ test: true });
    });

    it('does not hide request error', async () => {
      axios.get.mockRejectedValue(new Error());

      await expect(FeatureTogglesApi.getAllFeatureToggles()).rejects.toThrow();
    });
  });
});
