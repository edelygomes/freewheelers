import axios from 'axios';
import baseURL from './base';

const OrderApi = {
  createOrder: async itemId => {
    const response = await axios.post(`${baseURL}/orders`, { itemId });
    return response.headers.location.split('/').pop();
  },

  getOrder: async orderId => {
    const response = await axios.get(`${baseURL}/orders/${orderId}`);
    return response.data;
  },

  getOrders: async email => {
    const response = await axios.get(`${baseURL}/${email}/orders`);
    return response.data.orders;
  },

  getAllCustomerOrders: async () => {
    const response = await axios.get(`${baseURL}/admin/orders`);
    return response.data.orders;
  },

  updateOrder: async (orderId, orderInfo) => {
    await axios.patch(`${baseURL}/admin/orders/${orderId}`, orderInfo);
  }
};

export default OrderApi;
