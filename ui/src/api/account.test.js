import axios from 'axios';
import AccountApi from './account';
jest.mock('axios');
jest.mock('./base', () => 'base.url.com');

describe('AccountApi', () => {
  describe('createAccount', () => {
    it('calls api to login', async () => {
      const account = {};

      await AccountApi.createAccount(account);

      expect(axios.post).toHaveBeenCalledWith('base.url.com/accounts', account);
    });
  });

  describe('get', () => {
    it('calls api with email address', async () => {
      axios.get.mockResolvedValue({});

      await AccountApi.get('email@address.com');

      expect(axios.get).toHaveBeenCalledWith(
        'base.url.com/accounts/email@address.com'
      );
    });

    it('returns body of response', async () => {
      axios.get.mockResolvedValue({ data: 'account' });

      const result = await AccountApi.get();

      expect(result).toBe('account');
    });
  });
});
