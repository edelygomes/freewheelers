import axios from 'axios';
import baseURL from './base';

const FeatureTogglesApi = {
  getAllFeatureToggles: async () => {
    const response = await axios.get(`${baseURL}/toggles`);
    return response.data;
  }
};

export default FeatureTogglesApi;
