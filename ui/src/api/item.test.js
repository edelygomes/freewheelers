import axios from 'axios';
import mockItemsResponse from '../mock/items';
import ItemApi from './item';
jest.mock('axios');
jest.mock('./base', () => 'base.url.com');

describe('ItemApi', () => {
  describe('getAvailableItems', () => {
    it('makes a get request to the items endpoint', async () => {
      axios.get.mockResolvedValue({ data: {} });

      await ItemApi.getAvailableItems();

      expect(axios.get).toHaveBeenCalledWith('base.url.com/items');
    });

    it('returns the data payload with a successful request', async () => {
      axios.get.mockResolvedValue({ data: mockItemsResponse });

      const result = await ItemApi.getAvailableItems();

      expect(result).toEqual(mockItemsResponse.items);
    });

    it('does not hide request error', async () => {
      axios.get.mockRejectedValue(new Error());

      await expect(ItemApi.getAvailableItems()).rejects.toThrow();
    });
  });

  describe('getItem', () => {
    it('makes a request to /items to get an item', async () => {
      const itemId = 123;
      axios.get.mockResolvedValue({});

      await ItemApi.getItem(itemId);

      expect(axios.get).toHaveBeenCalledWith(`base.url.com/items/${itemId}`);
    });

    it('returns item on a successful response', async () => {
      const item = {};
      axios.get.mockResolvedValue({ data: item });

      const result = await ItemApi.getItem(123);

      expect(result).toBe(item);
    });
  });

  describe('update items', () => {
    it('makes a patch request to the items endpoint', async () => {
      const items = {
        items: [
          {
            id: 1,
            name: 'some name',
            description: 'some item description',
            price: 1,
            quantity: 1,
            type: 'FRAME'
          }
        ]
      };
      axios.patch.mockResolvedValue({ data: {} });

      await ItemApi.updateItem(items);

      expect(axios.patch).toHaveBeenCalledWith(
        'base.url.com/admin/items',
        items
      );
    });

    it('does not hide request error', async () => {
      axios.patch.mockRejectedValue(new Error());

      await expect(ItemApi.updateItem()).rejects.toThrow();
    });
  });

  describe('delete items', () => {
    it('makes a delete request to the items endpoint', async () => {
      const itemIds = { itemIds: [1, 2] };
      axios.delete.mockResolvedValue({});

      await ItemApi.deleteItems(itemIds);

      expect(axios.delete).toHaveBeenCalledWith('base.url.com/admin/items', {
        data: itemIds
      });
    });

    it('does not hide request error', async () => {
      axios.delete.mockRejectedValue(new Error());

      await expect(ItemApi.deleteItems()).rejects.toThrow();
    });
  });
});
