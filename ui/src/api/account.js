import axios from 'axios';
import baseURL from './base';

const AccountApi = {
  createAccount: async account => {
    await axios.post(`${baseURL}/accounts`, account);
  },
  get: async emailAddress => {
    const response = await axios.get(`${baseURL}/accounts/${emailAddress}`);
    return response.data;
  }
};

export default AccountApi;
