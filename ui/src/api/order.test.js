import axios from 'axios';
import OrderApi from './order';

jest.mock('axios');
jest.mock('./base', () => 'base.url.com');

describe('OrderApi', () => {
  describe('createOrder', () => {
    it('makes a request to /orders with itemID', async () => {
      axios.post.mockResolvedValue({ headers: { location: 'arbitrary' } });
      const itemId = 23;
      await OrderApi.createOrder(itemId);

      expect(axios.post).toHaveBeenCalledWith('base.url.com/orders', {
        itemId
      });
    });

    it('returns the new order id on successful response', async () => {
      const expectedOrderId = '7';
      const headers = { location: `base.url.com/orders/${expectedOrderId}` };

      axios.post.mockResolvedValue({ headers });

      const result = await OrderApi.createOrder(3);

      expect(result).toBe(expectedOrderId);
    });
  });

  describe('getOrder', () => {
    it('makes a request to /orders to get all orders', async () => {
      const orderId = 123;
      axios.get.mockResolvedValue({});

      await OrderApi.getOrder(orderId);

      expect(axios.get).toHaveBeenCalledWith(`base.url.com/orders/${orderId}`);
    });

    it('returns user order on a successful response', async () => {
      const order = {};
      axios.get.mockResolvedValue({ data: order });

      const result = await OrderApi.getOrder(123);

      expect(result).toBe(order);
    });
  });

  describe('getOrders', () => {
    it('makes a request to /orders to get all orders', async () => {
      axios.get.mockResolvedValue({ data: {} });

      await OrderApi.getOrders('test@email.com');

      expect(axios.get).toHaveBeenCalledWith(
        'base.url.com/test@email.com/orders'
      );
    });

    it('returns user orders on a successful response', async () => {
      const orders = [];
      axios.get.mockResolvedValue({ data: { orders } });

      const result = await OrderApi.getOrders('test@email.com');

      expect(result).toBe(orders);
    });
  });

  describe('update order', () => {
    it('makes a request to /admin/orders/{id} to update order', async () => {
      axios.patch.mockResolvedValue({});

      await OrderApi.updateOrder('123', {
        note: '123',
        orderStatus: 'IN_PROGRESS'
      });

      expect(axios.patch).toHaveBeenCalledWith(
        'base.url.com/admin/orders/123',
        { note: '123', orderStatus: 'IN_PROGRESS' }
      );
    });

    it('does not hide request error', async () => {
      axios.patch.mockRejectedValue(new Error());

      await expect(OrderApi.updateOrder()).rejects.toThrow();
    });
  });
});
