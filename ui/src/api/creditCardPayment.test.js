import axios from 'axios';
import CreditCardPaymentApi from './creditCardPayment';
jest.mock('axios');
jest.mock('./base', () => 'base.url.com');

describe('CreditCardPaymentApi', () => {
  describe('submitPayment', () => {
    it('calls api to submit payment', async () => {
      const creditCardPayment = {};

      await CreditCardPaymentApi.submitPayment(creditCardPayment);

      expect(axios.post).toHaveBeenCalledWith(
        'base.url.com/paymentCreditCard',
        creditCardPayment
      );
    });
  });
});
