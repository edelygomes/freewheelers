// const baseURL = process.env.REACT_APP_API_URL || 'http://localhost:8080/api' // prod
const baseURL = process.env.REACT_APP_API_URL || 'http://localhost:8080'; // local
export default baseURL;
