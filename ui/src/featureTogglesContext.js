import React from 'react';

const FeatureTogglesContext = React.createContext();
export default FeatureTogglesContext;
