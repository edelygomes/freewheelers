import React from 'react';
import { mount } from 'enzyme';
import FeatureTogglesContext from '../../featureTogglesContext';
import TermsAndConditions from './TermsAndConditions';

describe('<TermsAndConditions />', () => {
  describe('When the feature toggle termsAndConditions is enabled', () => {
    let wrapper;
    const savePdf = jest.fn();

    beforeEach(() => {
      wrapper = mount(
        <FeatureTogglesContext.Provider value={{ termsConditions: true }}>
          <TermsAndConditions savePdf={savePdf} />
        </FeatureTogglesContext.Provider>
      );
    });

    it('Should download Terms and Conditions file when I click', async () => {
      await wrapper.find('.TC-save-button').simulate('click');
      expect(savePdf).toHaveBeenCalled();
    });
  });
});
