import React from 'react';

function ServerError() {
  return (
    <div className="server-error">
      An error has occurred. Please try again later.
    </div>
  );
}

export default ServerError;
