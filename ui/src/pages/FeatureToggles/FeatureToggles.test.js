import { mount } from 'enzyme/build';
import React from 'react';
import FeatureTogglesContext from '../../featureTogglesContext';
import FeatureToggles from './FeatureToggles';

describe('FeatureToggles', () => {
  it('render feature toggles', () => {
    const featureName = 'SOME_FEATURE';
    const featureToggles = { [featureName]: true };

    const wrapper = mount(
      <FeatureTogglesContext.Provider value={featureToggles}>
        <FeatureToggles />
      </FeatureTogglesContext.Provider>
    );

    expect(wrapper.find('.feature-toggle__name').text()).toEqual(featureName);
    expect(wrapper.find('.feature-toggle__enabled').text()).toEqual('true');
  });
});
