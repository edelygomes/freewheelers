import React from 'react';
import FeatureTogglesContext from '../../featureTogglesContext';
import './FeatureToggles.css';

function renderToggleItem([name, value]) {
  return (
    <div key={name}>
      <span className="feature-toggle__name">{name}</span>
      <span className="feature-toggle__enabled">{value.toString()}</span>
    </div>
  );
}

function FeatureToggles() {
  return (
    <>
      <div>
        <span className="feature-toggle__page-title">Feature Toggles</span>
      </div>
      <div>
        <span className="feature-toggle__title">Feature Name</span>
        <span className="feature-toggle__title">Enabled</span>
      </div>
      <FeatureTogglesContext.Consumer>
        {toggles => Object.entries(toggles).map(renderToggleItem)}
      </FeatureTogglesContext.Consumer>
    </>
  );
}

export default FeatureToggles;
