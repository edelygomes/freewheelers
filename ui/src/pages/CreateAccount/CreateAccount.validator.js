import {
  getEmailError,
  getPasswordError,
  getAccountNameError,
  getPhoneNumberError,
  generateValidator,
  getCountryError,
  getCityError,
  getZipcodeError,
  getStreetError,
  getStreetNumberError,
  getUnitNumberError,
  getTermsAndConditionsError
} from '../../utils/formValidate';

const fields = {
  emailAddress: getEmailError,
  password: getPasswordError,
  accountName: getAccountNameError,
  phoneNumber: getPhoneNumberError,
  country: getCountryError,
  city: getCityError,
  zipcode: getZipcodeError,
  street: getStreetError,
  streetNumber: getStreetNumberError,
  unitNumber: getUnitNumberError,
  termsAndConditions: getTermsAndConditionsError
};

const createAccountValidator = generateValidator(fields);

export default createAccountValidator;
