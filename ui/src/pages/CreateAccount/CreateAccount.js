import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import AccountApi from '../../api/account';
import countryList from '../../constants/countryList';
import InputField from '../../components/InputField/InputField';
import SelectField from '../../components/InputField/SelectField';
import CheckboxField from '../../components/InputField/CheckboxField';
import FeatureTogglesContext from '../../featureTogglesContext';
import validator from './CreateAccount.validator';
import './createAccount.css';

class CreateAccount extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      emailAddress: '',
      password: '',
      accountName: '',
      phoneNumber: '',
      country: '',
      city: '',
      zipcode: '',
      street: '',
      streetNumber: '',
      unitNumber: '',
      termsAndConditions: false,
      errors: {}
    };
  }

  getCurrentAccountState = () => ({
    emailAddress: this.state.emailAddress,
    password: this.state.password,
    accountName: this.state.accountName,
    phoneNumber: this.state.phoneNumber,
    country: this.state.country,
    city: this.state.city,
    zipcode: this.state.zipcode,
    street: this.state.street,
    streetNumber: this.state.streetNumber,
    unitNumber: this.state.unitNumber,
    termsAndConditions: !!this.state.termsAndConditions
  });

  validateForm = account => {
    if (validator.isValid(account)) {
      return true;
    }
    this.setState({
      errors: {
        ...validator.getErrors(account),
        form: 'please correct the fields above'
      }
    });
  };

  onSubmit = toggles => async event => {
    event.preventDefault();
    const account = this.getCurrentAccountState();
    const accountWithoutToggles = {};
    Object.keys(account).forEach(key => {
      if (
        (key === 'city' ||
          key === 'zipcode' ||
          key === 'street' ||
          key === 'streetNumber' ||
          key === 'unitNumber') &&
        toggles['address']
      ) {
        accountWithoutToggles.city = account.city;
        accountWithoutToggles.zipcode = account.zipcode;
        accountWithoutToggles.street = account.street;
        accountWithoutToggles.streetNumber = account.streetNumber;
        accountWithoutToggles.unitNumber = account.unitNumber;
      } else if (
        toggles[key] === undefined &&
        key !== 'city' &&
        key !== 'zipcode' &&
        key !== 'street' &&
        key !== 'streetNumber' &&
        key !== 'unitNumber'
      ) {
        accountWithoutToggles[key] = account[key];
      } else if (toggles[key]) {
        accountWithoutToggles[key] = account[key];
      }
    });
    if (!this.validateForm(accountWithoutToggles)) return;

    try {
      await AccountApi.createAccount(accountWithoutToggles);
      this.props.history.push('/login');
    } catch (error) {
      this.setState(() => ({
        errors: { form: error.message }
      }));
    }
  };

  handleChangeFor = fieldName => event => {
    this.setState({ [fieldName]: event.target.value });
  };

  handleBlurFor = fieldName => event => {
    const validate = validator.fields[fieldName];

    this.setState({
      errors: {
        ...this.state.errors,
        [fieldName]: validate(event.target.value)
      }
    });
  };

  sortCountriesWithUKFirst = countries => {
    let firstCountryInList = 'UK';
    let sortedCountries = [...countries];
    sortedCountries.sort();
    sortedCountries = sortedCountries.filter(country => {
      return country !== firstCountryInList;
    });
    sortedCountries.unshift(firstCountryInList);

    return sortedCountries;
  };

  render() {
    return (
      <FeatureTogglesContext.Consumer>
        {toggles => {
          return (
            <form className="create-account" onSubmit={this.onSubmit(toggles)}>
              <h1 className="form-title">Create Account</h1>

              <InputField
                label="Email:"
                changeHandler={this.handleChangeFor('emailAddress')}
                blurHandler={this.handleBlurFor('emailAddress')}
                error={this.state.errors.emailAddress}
              />

              <InputField
                type="password"
                label="Password:"
                changeHandler={this.handleChangeFor('password')}
                blurHandler={this.handleBlurFor('password')}
                error={this.state.errors.password}
              />

              <InputField
                label="Name:"
                changeHandler={this.handleChangeFor('accountName')}
                blurHandler={this.handleBlurFor('accountName')}
                error={this.state.errors.accountName}
              />

              <InputField
                type="tel"
                label="Phone Number:"
                changeHandler={this.handleChangeFor('phoneNumber')}
                blurHandler={this.handleBlurFor('phoneNumber')}
                error={this.state.errors.phoneNumber}
              />

              <SelectField
                id="country"
                placeholder="Select Country"
                label="Country:"
                options={this.sortCountriesWithUKFirst(countryList)}
                changeHandler={this.handleChangeFor('country')}
                value={this.state.country}
                error={this.state.errors.country}
              />
              <p id="country-message">
                If your country is not listed then we don't ship there. Please
                check back later.
              </p>

              <FeatureTogglesContext.Consumer>
                {toggles => {
                  if (toggles['address']) {
                    return (
                      <Fragment>
                        <InputField
                          label="City:"
                          changeHandler={this.handleChangeFor('city')}
                          blurHandler={this.handleBlurFor('city')}
                          error={this.state.errors.city}
                        />
                        <InputField
                          label="Zipcode:"
                          changeHandler={this.handleChangeFor('zipcode')}
                          blurHandler={this.handleBlurFor('zipcode')}
                          error={this.state.errors.zipcode}
                        />
                        <InputField
                          label="Street:"
                          changeHandler={this.handleChangeFor('street')}
                          blurHandler={this.handleBlurFor('street')}
                          error={this.state.errors.zipcode}
                        />
                        <InputField
                          label="Street Number:"
                          changeHandler={this.handleChangeFor('streetNumber')}
                          blurHandler={this.handleBlurFor('streetNumber')}
                          error={this.state.errors.streetNumber}
                        />
                        <InputField
                          label="Unit Number:"
                          changeHandler={this.handleChangeFor('unitNumber')}
                          blurHandler={this.handleBlurFor('unitNumber')}
                          error={this.state.errors.unitNumber}
                        />
                      </Fragment>
                    );
                  }
                }}
              </FeatureTogglesContext.Consumer>

              {toggles['termsAndConditions'] && (
                <div className="termsAndConditions__wrapper">
                  <CheckboxField
                    id="termsandconditions"
                    type="checkbox"
                    changeHandler={this.handleChangeFor('termsAndConditions')}
                    error={this.state.errors.termsAndConditions}
                    label={
                      <span>
                        I accept the{' '}
                        <Link to="/terms-conditions" target="_blank">
                          Terms & Conditions
                        </Link>
                      </span>
                    }
                  />
                </div>
              )}

              <input
                type="submit"
                className="button button--primary"
                value="Create Account"
              />
              <span className="input-error">{this.state.errors.form}</span>
            </form>
          );
        }}
      </FeatureTogglesContext.Consumer>
    );
  }
}

export default CreateAccount;
