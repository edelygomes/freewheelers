import React from 'react';
import { mount } from 'enzyme';
import { Router } from 'react-router-dom';
import AccountApi from '../../api/account';
import FeatureTogglesContext from '../../featureTogglesContext';
import historyForRouter from '../../history';
import CreateAccount from './CreateAccount';
jest.mock('../../api/account');

describe('<CreateAccount />', () => {
  const history = { push: jest.fn() };
  const flushPromises = () => new Promise(setImmediate);

  describe('when feature toggle address is enabled', () => {
    const validInputs = {
      emailAddress: 'newEmail@example.com',
      password: 'Valid123',
      accountName: 'another name',
      phoneNumber: '55512345',
      country: 'UK',
      city: 'London',
      zipcode: 'EC1Y 8SY',
      street: 'Elm Street',
      streetNumber: '97',
      unitNumber: '402',
      termsAndConditions: true
    };

    let wrapper;

    beforeEach(() => {
      wrapper = mount(
        <FeatureTogglesContext.Provider
          value={{ address: true, termsAndConditions: true }}
        >
          <Router history={historyForRouter}>
            <CreateAccount history={history} />
          </Router>
        </FeatureTogglesContext.Provider>
      );
    });

    it('should create account with entered values', () => {
      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper
        .find('#password')
        .simulate('change', { target: { value: validInputs.password } });
      wrapper
        .find('#name')
        .simulate('change', { target: { value: validInputs.accountName } });
      wrapper
        .find('#phonenumber')
        .simulate('change', { target: { value: validInputs.phoneNumber } });
      wrapper
        .find('#country-select')
        .simulate('change', { target: { value: validInputs.country } });
      wrapper
        .find('#city')
        .simulate('change', { target: { value: validInputs.city } });
      wrapper
        .find('#zipcode')
        .simulate('change', { target: { value: validInputs.zipcode } });
      wrapper
        .find('#street')
        .simulate('change', { target: { value: validInputs.street } });
      wrapper
        .find('#streetnumber')
        .simulate('change', { target: { value: validInputs.streetNumber } });
      wrapper
        .find('#unitnumber')
        .simulate('change', { target: { value: validInputs.unitNumber } });
      wrapper.find('input#termsandconditions').simulate('change', {
        target: { value: validInputs.termsAndConditions }
      });

      wrapper.find('form').simulate('submit');

      expect(AccountApi.createAccount).toHaveBeenCalledWith(validInputs);
    });

    it('should redirect to login on success', async () => {
      AccountApi.createAccount.mockResolvedValue({});

      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper
        .find('#password')
        .simulate('change', { target: { value: validInputs.password } });
      wrapper
        .find('#name')
        .simulate('change', { target: { value: validInputs.accountName } });
      wrapper
        .find('#phonenumber')
        .simulate('change', { target: { value: validInputs.phoneNumber } });
      wrapper
        .find('#country-select')
        .simulate('change', { target: { value: validInputs.country } });
      wrapper
        .find('#city')
        .simulate('change', { target: { value: validInputs.city } });
      wrapper
        .find('#zipcode')
        .simulate('change', { target: { value: validInputs.zipcode } });
      wrapper
        .find('#street')
        .simulate('change', { target: { value: validInputs.street } });
      wrapper
        .find('#streetnumber')
        .simulate('change', { target: { value: validInputs.streetNumber } });
      wrapper
        .find('#unitnumber')
        .simulate('change', { target: { value: validInputs.unitNumber } });
      wrapper.find('input#termsandconditions').simulate('change', {
        target: { value: validInputs.termsAndConditions }
      });
      wrapper.find('form').simulate('submit');

      await flushPromises();
      wrapper.update();

      expect(history.push).toHaveBeenCalledWith('/login');
    });

    it('should prevent the form from submitting when inputs are invalid', () => {
      wrapper.find('form').simulate('submit');

      expect(AccountApi.createAccount).not.toHaveBeenCalled();
      expect(history.push).not.toHaveBeenCalled();
    });

    it('should show errors when submitting any invalid inputs', () => {
      wrapper.find('form').simulate('submit');

      expect(wrapper.text()).toContain('please correct the fields above');
    });

    it('should clear the error when value is corrected and blurred', () => {
      wrapper.find('form').simulate('submit');
      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper.find('#email').simulate('blur');

      expect(wrapper.text()).not.toContain('error');
    });

    it('should display message for unavailable countries for shipment', () => {
      expect(wrapper.find('#country-message').text()).toContain(
        "If your country is not listed then we don't ship there. Please check back later."
      );
    });
    it('should display the list of countries in a specific other', () => {
      const actualCountries = wrapper.find('#country').props().options;

      let expectedCountries = [...actualCountries];
      expectedCountries.sort();
      expectedCountries = expectedCountries.filter(country => {
        return country !== 'UK';
      });
      expectedCountries.unshift('UK');

      expect(actualCountries).toEqual(expectedCountries);
    });
  });

  describe('when feature toggles are disabled', () => {
    const validInputs = {
      emailAddress: 'newEmail@example.com',
      password: 'Valid123',
      accountName: 'another name',
      phoneNumber: '55512345',
      country: 'UK'
    };

    let wrapper;

    beforeEach(() => {
      wrapper = mount(
        <FeatureTogglesContext.Provider
          value={{ address: false, termsAndConditions: false }}
        >
          <CreateAccount history={history} />
        </FeatureTogglesContext.Provider>
      );
    });

    it('should create account with entered values', () => {
      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper
        .find('#password')
        .simulate('change', { target: { value: validInputs.password } });
      wrapper
        .find('#name')
        .simulate('change', { target: { value: validInputs.accountName } });
      wrapper
        .find('#phonenumber')
        .simulate('change', { target: { value: validInputs.phoneNumber } });
      wrapper
        .find('#country-select')
        .simulate('change', { target: { value: validInputs.country } });

      wrapper.find('form').simulate('submit');

      expect(AccountApi.createAccount).toHaveBeenCalledWith(validInputs);
    });

    it('should redirect to login on success', async () => {
      AccountApi.createAccount.mockResolvedValue({});

      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper
        .find('#password')
        .simulate('change', { target: { value: validInputs.password } });
      wrapper
        .find('#name')
        .simulate('change', { target: { value: validInputs.accountName } });
      wrapper
        .find('#phonenumber')
        .simulate('change', { target: { value: validInputs.phoneNumber } });
      wrapper
        .find('#country-select')
        .simulate('change', { target: { value: validInputs.country } });

      wrapper.find('form').simulate('submit');

      await flushPromises();
      wrapper.update();

      expect(history.push).toHaveBeenCalledWith('/login');
    });

    it('should prevent the form from submitting when inputs are invalid', () => {
      wrapper.find('form').simulate('submit');

      expect(AccountApi.createAccount).not.toHaveBeenCalled();
      expect(history.push).not.toHaveBeenCalled();
    });

    it('should show errors when submitting any invalid inputs', () => {
      wrapper.find('form').simulate('submit');

      expect(wrapper.text()).toContain('please correct the fields above');
    });

    it('should clear the error when value is corrected and blurred', () => {
      wrapper.find('form').simulate('submit');
      wrapper
        .find('#email')
        .simulate('change', { target: { value: validInputs.emailAddress } });
      wrapper.find('#email').simulate('blur');

      expect(wrapper.text()).not.toContain('error');
    });
  });
});
