import React from 'react';
import { mount } from 'enzyme';
import LoginApi from '../../api/login';
import Login from './Login';
jest.mock('../../api/login');

describe('<Login />', () => {
  const flushPromises = () => new Promise(setImmediate);
  const history = { push: jest.fn() };

  it('should call login api with entered values', async () => {
    const wrapper = mount(<Login history={history} />);

    wrapper
      .find('#email')
      .simulate('change', { target: { value: 'newEmail@example.com' } });
    wrapper
      .find('#password')
      .simulate('change', { target: { value: 'new password value' } });
    wrapper.find('form').simulate('submit');

    expect(LoginApi.login).toHaveBeenCalledWith({
      email: 'newEmail@example.com',
      password: 'new password value'
    });
  });

  it('should call principal api when login is successful', async () => {
    LoginApi.login.mockReturnValue({});

    const wrapper = mount(<Login history={history} />);
    wrapper
      .find('#email')
      .simulate('change', { target: { value: 'newEmail@example.com' } });
    wrapper
      .find('#password')
      .simulate('change', { target: { value: 'new password value' } });
    wrapper.find('form').simulate('submit');

    await flushPromises();
    wrapper.update();
    expect(LoginApi.principal).toHaveBeenCalledTimes(1);
  });

  it('should not call principal api when login is failed', async () => {
    const wrapper = mount(<Login history={history} />);
    LoginApi.login.mockRejectedValue(new Error());
    wrapper.find('form').simulate('submit');

    await flushPromises();
    wrapper.update();

    expect(LoginApi.principal).not.toHaveBeenCalled();
  });

  it('should prevent the form from submitting when inputs are invalid', () => {
    const wrapper = mount(<Login history={history} />);

    wrapper.find('form').simulate('submit');

    expect(LoginApi.login).not.toHaveBeenCalled();
  });

  it('should show errors when submitting any error value ', () => {
    const wrapper = mount(<Login history={history} />);

    wrapper.find('form').simulate('submit');

    expect(wrapper.text()).toContain('required');
  });
});
