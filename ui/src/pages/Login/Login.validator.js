import { generateValidator, isRequiredError } from '../../utils/formValidate';

const fields = {
  email: isRequiredError,
  password: isRequiredError
};

const loginValidator = generateValidator(fields);

export default loginValidator;
