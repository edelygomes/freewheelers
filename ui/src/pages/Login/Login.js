import React from 'react';
import LoginApi from '../../api/login';
import InputField from '../../components/InputField/InputField';
import validator from './Login.validator';

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: {}
    };
  }

  getCurrentAccountState = () => ({
    email: this.state.email,
    password: this.state.password
  });

  validateForm = account => {
    if (!validator.isValid(account)) {
      this.setState({
        errors: {
          ...validator.getErrors(account),
          form: 'please correct the fields above'
        }
      });
      return false;
    }
    return true;
  };

  onSubmit = async event => {
    event.preventDefault();

    const account = this.getCurrentAccountState();
    if (!this.validateForm(account)) return;

    try {
      await LoginApi.login(account);
      await LoginApi.principal();
      window.location.href = '/';
    } catch (error) {
      this.setState({
        errors: { form: 'Your login attempt was not successful, try again.' }
      });
    }
  };

  handleChangeFor = fieldName => event => {
    this.setState({ [fieldName]: event.target.value });
  };

  handleBlurFor = fieldName => event => {
    const validate = validator.fields[fieldName];
    this.setState({
      errors: {
        ...this.state.errors,
        [fieldName]: validate(event.target.value)
      }
    });
  };

  render() {
    return (
      <form className="login" onSubmit={this.onSubmit}>
        <h1 className="form-title">Sign In</h1>

        <InputField
          label="Email:"
          changeHandler={this.handleChangeFor('email')}
          blurHandler={this.handleBlurFor('email')}
          error={this.state.errors.email}
        />

        <InputField
          label="Password:"
          type="password"
          changeHandler={this.handleChangeFor('password')}
          blurHandler={this.handleBlurFor('password')}
          error={this.state.errors.password}
        />

        <input type="submit" className="button button--primary" value="Login" />
        <span className="input-error">{this.state.errors.form}</span>
      </form>
    );
  }
}

export default Login;
