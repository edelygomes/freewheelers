import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { getPrincipal } from '../utils/principal';
import OrderApi from '../api/order';
import FeatureTogglesContext from '../featureTogglesContext';
import App from './App';
import Home from './Home/Home';
import NotFound from './NotFound/NotFound';
import Admin from './Admin/Admin';

jest.mock('./Home/Home');
jest.mock('../utils/principal');
jest.mock('../api/order');

const featureToggles = { eu_cookie: true, termsConditions: false };

describe('router should redirect to correct path', () => {
  let wrapper;

  let initializeWrapper = initialEntriesPath => {
    wrapper = mount(
      <MemoryRouter initialEntries={[initialEntriesPath]}>
        <FeatureTogglesContext.Provider value={featureToggles}>
          <App />
        </FeatureTogglesContext.Provider>
      </MemoryRouter>
    );
  };

  test('invalid path should redirect to not found page', () => {
    getPrincipal.mockReturnValue({});

    initializeWrapper('/random');

    expect(wrapper.find(Home)).toHaveLength(0);
    expect(wrapper.find(NotFound)).toHaveLength(1);
  });

  test('default path should redirect to homepage', () => {
    getPrincipal.mockReturnValue({});

    initializeWrapper('');

    expect(wrapper.find(Home)).toHaveLength(1);
    expect(wrapper.find(NotFound)).toHaveLength(0);
  });

  test('should redirect to not found when try to access admin without login', () => {
    getPrincipal.mockReturnValue({});

    initializeWrapper('/admin');

    expect(wrapper.find(Admin)).toHaveLength(0);
    expect(wrapper.find(NotFound)).toHaveLength(1);
  });

  test('admin could visit admin page', () => {
    OrderApi.getAllCustomerOrders.mockResolvedValue([]);
    getPrincipal.mockReturnValue({ username: 'test', role: 'ROLE_ADMIN' });

    initializeWrapper('/admin');

    expect(wrapper.find(Admin)).toHaveLength(1);
    expect(wrapper.find(NotFound)).toHaveLength(0);
  });
});
