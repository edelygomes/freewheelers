import { mount, shallow } from 'enzyme/build';
import React from 'react';
import OrderApi from '../../api/order';
import Order from '../../components/Order/Order';
import FeatureTogglesContext from '../../featureTogglesContext';
import OrderConfirmation from './OrderConfirmation';

jest.mock('../../api/order');

describe('<Order />', () => {
  const flushPromises = () => new Promise(setImmediate);

  const props = {
    match: {
      params: {
        id: 2
      }
    }
  };

  it('should fetch an order', () => {
    shallow(<OrderConfirmation {...props} />);
    expect(OrderApi.getOrder).toHaveBeenCalledWith(props.match.params.id);
  });

  it('should render order after fetch order', async () => {
    OrderApi.getOrder.mockResolvedValue({
      id: 46,
      item: {
        name: 'This Item',
        description: 'This Item description',
        price: '12.30'
      },
      date: 'Some order date',
      emailAddress: 'me@example.com'
    });
    const wrapper = mount(
      <FeatureTogglesContext.Provider value={{ VAT: false }}>
        <OrderConfirmation {...props} />
      </FeatureTogglesContext.Provider>
    );

    await flushPromises();
    wrapper.update();

    expect(wrapper.find(Order)).toHaveLength(1);
    expect(wrapper.text()).toContain('Order #46');
  });
});
