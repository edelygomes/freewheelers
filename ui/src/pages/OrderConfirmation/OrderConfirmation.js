import React from 'react';
import OrderApi from '../../api/order';
import './OrderConfirmation.css';
import Order from '../../components/Order/Order';

class OrderConfirmation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      order: null
    };
  }

  async componentDidMount() {
    const order = await OrderApi.getOrder(this.props.match.params.id);
    this.setState({ order });
  }

  render() {
    const order = this.state.order;
    return order ? (
      <div className="order-confirmation">
        <div className="order-confirmation__title">Item Reserved!</div>
        <Order {...order} />
      </div>
    ) : null;
  }
}

export default OrderConfirmation;
