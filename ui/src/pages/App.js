import React from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import Navbar from '../components/Navbar/Navbar';
import { getRouterConfig } from '../utils/router';
import cookieMessage from '../constants/cookieMessage';
import CookieModal from '../components/CookieModal/CookieModal';
import Footer from '../components/Footer/Footer';

function App() {
  const getRouters = () => {
    return getRouterConfig().map((route, index) => {
      const props = {
        path: route.path,
        exact: route.exact,
        component: route.component
      };
      return <Route key={index} {...props} />;
    });
  };

  return (
    <>
      <div className="content_wrapper">
        <CookieModal message={cookieMessage} />
        <Navbar />
        <div className="app">
          <Switch>{getRouters()}</Switch>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default App;
