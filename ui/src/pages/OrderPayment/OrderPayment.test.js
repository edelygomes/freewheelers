import React from 'react';
import { mount, shallow } from 'enzyme';
import ItemApi from '../../api/item';
import OrderSummary from '../../components/OrderSummary/OrderSummary';
import PaymentCard from '../../components/PaymentCard/PaymentCard';
import OrderPayment from './OrderPayment';

jest.mock('../../api/item');

describe('<OrderPayment />', () => {
  const flushPromises = () => new Promise(setImmediate);

  const props = {
    match: {
      params: {
        id: 2
      }
    }
  };

  it('should fetch an item', () => {
    shallow(<OrderPayment {...props} />);
    expect(ItemApi.getItem).toHaveBeenCalledWith(props.match.params.id);
  });

  it('should render item after fetch order', async () => {
    const wrapper = mount(<OrderPayment {...props} />);

    await flushPromises();
    wrapper.update();

    expect(wrapper.find(OrderSummary)).toHaveLength(1);
    expect(wrapper.text()).toContain('Summary');
    expect(wrapper.find(PaymentCard)).toHaveLength(1);
    expect(wrapper.text()).toContain('How do you want to pay?');
  });
});
