import React, { Fragment } from 'react';
import ItemApi from '../../api/item';
import OrderSummary from '../../components/OrderSummary/OrderSummary';
import PaymentCard from '../../components/PaymentCard/PaymentCard';

class OrderPayment extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { item: null };
  }

  async componentDidMount() {
    const item = await ItemApi.getItem(this.props.match.params.id);
    this.setState({ item });
  }

  render() {
    return (
      <Fragment>
        <h1>Payment</h1>
        <OrderSummary price={this.state.item && this.state.item.price} />
        <PaymentCard itemId={this.props.match.params.id} />
      </Fragment>
    );
  }
}

export default OrderPayment;
