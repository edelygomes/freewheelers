import React from 'react';
import { Link } from 'react-router-dom';
import Section from '../../components/Section/Section';
import '../../components/ManageOrders/ManageOrders.css';
import ManageOrders from '../../components/ManageOrders/ManageOrders';

class Admin extends React.PureComponent {
  render() {
    return (
      <>
        <Section title="Actions">
          <Link to="/manage-items" className="admin__manage-items">
            <button className="button button--primary">Manage Items</button>
          </Link>
        </Section>

        <ManageOrders />
      </>
    );
  }
}

export default Admin;
