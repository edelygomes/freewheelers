import React from 'react';
import Item from '../../components/Item/Item';
import ItemApi from '../../api/item';
import OrderApi from '../../api/order';
import history from '../../history';
import Card from '../../components/Card/Card';
import FeatureTogglesContext from '../../featureTogglesContext';

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: null
    };
  }

  async componentDidMount() {
    const items = await ItemApi.getAvailableItems();
    this.setState({ items });
  }

  handleReserveItem = itemId => async () => {
    const orderId = await OrderApi.createOrder(itemId);
    history.push(`/order-confirmation/${orderId}`);
  };

  handleBuyItem = itemId => {
    history.push(`/order-payment/${itemId}`);
  };

  render() {
    const items = this.state.items || [];

    return (
      <div className="items">
        {items.map(item => (
          <Card key={item.id}>
            <Item {...item} />
            <FeatureTogglesContext.Consumer>
              {toggles => {
                const handleOnclick = toggles.payment
                  ? this.handleBuyItem(item.id)
                  : this.handleReserveItem(item.id);

                return (
                  <button
                    className="button button--primary"
                    onClick={handleOnclick}
                  >
                    Reserve
                  </button>
                );
              }}
            </FeatureTogglesContext.Consumer>
          </Card>
        ))}
      </div>
    );
  }
}

export default Home;
