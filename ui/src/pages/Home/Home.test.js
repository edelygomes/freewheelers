import React from 'react';
import { mount } from 'enzyme';
import Item from '../../components/Item/Item';
import ItemApi from '../../api/item';
import mockItems from '../../mock/items';
import OrderApi from '../../api/order';
import history from '../../history';
import FeatureTogglesContext from '../../featureTogglesContext';
import Home from './Home';
jest.mock('../../api/item');
jest.mock('../../api/order');
jest.mock('../../history');

describe('<Home />', () => {
  const flushPromises = () => new Promise(setImmediate);
  let wrapper;
  const featureToggles = { payment: false };

  describe('with payment feature toggle off', () => {
    it('should fetch all items in home page', async () => {
      mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();

      expect(ItemApi.getAvailableItems).toHaveBeenCalledTimes(1);
    });

    it('should render correct number of items in home page', async () => {
      ItemApi.getAvailableItems.mockResolvedValue(mockItems.items);
      wrapper = mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.find(Item)).toHaveLength(2);
    });

    it('should create an order on item reserve', async () => {
      const item = mockItems.items[0];
      ItemApi.getAvailableItems.mockResolvedValue([item]);

      wrapper = mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );

      await flushPromises();
      wrapper.update();

      wrapper.find('button').simulate('click');

      expect(OrderApi.createOrder).toHaveBeenCalledWith(item.id);
    });

    it('should redirect to /order/{orderId} on successful order creation', async () => {
      const item = mockItems.items[0];
      ItemApi.getAvailableItems.mockResolvedValue([item]);
      OrderApi.createOrder.mockResolvedValue(item.id);

      wrapper = mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );

      await flushPromises();
      wrapper.update();

      wrapper
        .find('button')
        .first()
        .simulate('click');
      await flushPromises();

      expect(history.push).toHaveBeenCalledWith('/order-confirmation/1');
    });
  });

  describe('with payment feature toggle on', () => {
    let wrapper;
    const featureToggles = { payment: true };

    it('should fetch all items in home page', async () => {
      mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();

      expect(ItemApi.getAvailableItems).toHaveBeenCalledTimes(1);
    });

    it('should render correct number of items in home page', async () => {
      ItemApi.getAvailableItems.mockResolvedValue(mockItems.items);

      wrapper = mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );

      await flushPromises();
      wrapper.update();

      expect(wrapper.find(Item)).toHaveLength(2);
    });

    it('should redirect to /order-payment/{itemId} when buying item', async () => {
      const item = mockItems.items[0];
      ItemApi.getAvailableItems.mockResolvedValue([item]);

      wrapper = mount(
        <FeatureTogglesContext.Provider value={featureToggles}>
          <Home />
        </FeatureTogglesContext.Provider>
      );

      await flushPromises();
      wrapper.update();

      wrapper
        .find('button')
        .first()
        .simulate('click');
      await flushPromises();

      expect(history.push).toHaveBeenCalledWith('/order-payment/1');
    });
  });
});
