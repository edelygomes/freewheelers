import React from 'react';
import CreateItem from '../../components/CreateItem/CreateItem';
import './ManageItems.css';
import EditItems from '../../components/EditItems/EditItems';
import ItemApi from '../../api/item';

class ManageItems extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  async componentDidMount() {
    await this.getAvailableItems();
  }

  onItemsChange = items => {
    this.setState({ items });
  };

  getAvailableItems = async () => {
    const items = await ItemApi.getAvailableItems();
    this.setState({ items });
  };

  render() {
    return (
      <div className="manage-items">
        <CreateItem onSuccess={this.getAvailableItems} />
        <EditItems
          onSuccess={this.getAvailableItems}
          onItemsChange={this.onItemsChange}
          items={this.state.items}
        />
      </div>
    );
  }
}

export default ManageItems;
