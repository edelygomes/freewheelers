import React from 'react';
import { shallow, mount } from 'enzyme';
import CreateItem from '../../components/CreateItem/CreateItem';
import ItemApi from '../../api/item';
import EditItems from '../../components/EditItems/EditItems';
import ManageItems from './ManageItems';

jest.mock('../../components/CreateItem/CreateItem');
jest.mock('../../api/item');
jest.mock('../../components/CreateItem/Item.validator');

const items = [
  {
    id: 1,
    name: 'some name',
    description: 'some item description',
    price: 1,
    quantity: 1,
    type: 'FRAME'
  },
  {
    id: 2,
    name: 'some name2',
    description: 'some item description2',
    price: 1,
    quantity: 1,
    type: 'FRAME'
  }
];

describe('ManageItems', () => {
  describe('render', () => {
    it('shows CreateItem form', () => {
      ItemApi.getAvailableItems.mockResolvedValue(items);
      const wrapper = shallow(<ManageItems />);

      expect(wrapper.find(CreateItem)).toHaveLength(1);
    });

    it('shows EditItems form', () => {
      ItemApi.getAvailableItems.mockResolvedValue(items);
      const wrapper = shallow(<ManageItems />);

      expect(wrapper.find(EditItems)).toHaveLength(1);
    });

    it('updates items when an item is created', async () => {
      ItemApi.getAvailableItems.mockResolvedValue(items);
      const wrapper = mount(<ManageItems />);

      wrapper
        .find(CreateItem)
        .props()
        .onSuccess();

      expect(ItemApi.getAvailableItems).toHaveBeenCalledTimes(2);
    });

    it('updates items when an item is updated or deleted', async () => {
      ItemApi.getAvailableItems.mockResolvedValue(items);
      const wrapper = mount(<ManageItems />);

      wrapper
        .find(EditItems)
        .props()
        .onSuccess();

      expect(ItemApi.getAvailableItems).toHaveBeenCalledTimes(2);
    });
  });
});
