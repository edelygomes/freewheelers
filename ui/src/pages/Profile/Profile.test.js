import React from 'react';
import { mount } from 'enzyme';
import { getPrincipal } from '../../utils/principal';
import AccountApi from '../../api/account';
import OrderApi from '../../api/order';
import FeatureTogglesContext from '../../featureTogglesContext';
import Profile from './Profile';

jest.mock('../../api/account');
jest.mock('../../api/order');
jest.mock('../../utils/principal');

const flushPromises = () => new Promise(setImmediate);

const account = {
  accountName: 'account name',
  emailAddress: 'email address',
  phoneNumber: 'phone number',
  country: 'country',
  city: 'city'
};

const accountCountryNotSet = {
  accountName: 'account name',
  emailAddress: 'email address',
  phoneNumber: 'phone number',
  country: 'No Country Set'
};

const accountAddressNotSet = {
  accountName: 'account name',
  emailAddress: 'email address',
  phoneNumber: 'phone number',
  country: 'country',
  city: 'No Address Set'
};

const orders = [
  { id: 1, item: { name: 'item 1' } },
  { id: 2, item: { name: 'item 2' } }
];

describe('Profile', () => {
  it("displays message 'No Country Set' in country field if user has not set is country yet", async () => {
    getPrincipal.mockReturnValue({ username: 'admin@example.com' });
    AccountApi.get.mockResolvedValue(accountCountryNotSet);
    OrderApi.getOrders.mockResolvedValue(orders);

    const wrapper = mount(
      <FeatureTogglesContext.Provider value={{ VAT: false }}>
        <Profile />
      </FeatureTogglesContext.Provider>
    );
    await flushPromises();
    wrapper.update();

    expect(wrapper.text()).toContain('Country:No Country Set');
  });

  describe('When Address Feature Toggle is Enabled', () => {
    it('shows profile and orders for user if email address is in url', async () => {
      AccountApi.get.mockResolvedValue(account);
      OrderApi.getOrders.mockResolvedValue(orders);

      const match = { params: { username: '1234' } };
      const wrapper = mount(
        <FeatureTogglesContext.Provider value={{ address: true }}>
          <Profile match={match} />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.text()).toContain('Account name:account name');
      expect(wrapper.text()).toContain('Email address:email address');
      expect(wrapper.text()).toContain('Phone number:phone number');
      expect(wrapper.text()).toContain('Country:country');
      expect(wrapper.text()).toContain('City:city');
      expect(wrapper.text()).toContain('item 1');
      expect(wrapper.text()).toContain('item 2');
    });

    it('shows profile and orders for user if user is login and email address is not in url', async () => {
      getPrincipal.mockReturnValue({ username: 'user@example.com' });
      AccountApi.get.mockResolvedValue(account);
      OrderApi.getOrders.mockResolvedValue(orders);

      const wrapper = mount(
        <FeatureTogglesContext.Provider value={{ address: true }}>
          <Profile />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.text()).toContain('Account name:account name');
      expect(wrapper.text()).toContain('Email address:email address');
      expect(wrapper.text()).toContain('Phone number:phone number');
      expect(wrapper.text()).toContain('Country:country');
      expect(wrapper.text()).toContain('City:city');
      expect(wrapper.text()).toContain('item 1');
      expect(wrapper.text()).toContain('item 2');
    });

    it("displays message 'No Address set' in city field if user has not set is city yet", async () => {
      getPrincipal.mockReturnValue({ username: 'admin@example.com' });
      AccountApi.get.mockResolvedValue(accountAddressNotSet);
      OrderApi.getOrders.mockResolvedValue(orders);

      const wrapper = mount(
        <FeatureTogglesContext.Provider value={{ address: true }}>
          <Profile />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.text()).toContain('City:No Address Set');
    });

    it('does not show anything until an account is loaded', async () => {
      getPrincipal.mockReturnValue({ username: 'user@example.com' });

      const wrapper = mount(<Profile />);

      expect(wrapper.text()).toBe('');
    });
  });

  describe('When Address Feature Toggle is Disabled', () => {
    it('shows profile and orders for user if email address is in url', async () => {
      AccountApi.get.mockResolvedValue(account);
      OrderApi.getOrders.mockResolvedValue(orders);

      const match = { params: { username: '1234' } };
      const wrapper = mount(
        <FeatureTogglesContext.Provider value={{ address: false }}>
          <Profile match={match} />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.text()).toContain('Account name:account name');
      expect(wrapper.text()).toContain('Email address:email address');
      expect(wrapper.text()).toContain('Phone number:phone number');
      expect(wrapper.text()).toContain('Country:country');
      expect(wrapper.text()).toContain('item 1');
      expect(wrapper.text()).toContain('item 2');
    });

    it('shows profile and orders for user if user is login and email address is not in url', async () => {
      getPrincipal.mockReturnValue({ username: 'user@example.com' });
      AccountApi.get.mockResolvedValue(account);
      OrderApi.getOrders.mockResolvedValue(orders);

      const wrapper = mount(
        <FeatureTogglesContext.Provider value={{ address: false }}>
          <Profile />
        </FeatureTogglesContext.Provider>
      );
      await flushPromises();
      wrapper.update();

      expect(wrapper.text()).toContain('Account name:account name');
      expect(wrapper.text()).toContain('Email address:email address');
      expect(wrapper.text()).toContain('Phone number:phone number');
      expect(wrapper.text()).toContain('Country:country');
      expect(wrapper.text()).toContain('item 1');
      expect(wrapper.text()).toContain('item 2');
    });

    it('does not show anything until an account is loaded', async () => {
      getPrincipal.mockReturnValue({ username: 'user@example.com' });

      const wrapper = mount(<Profile />);

      expect(wrapper.text()).toBe('');
    });
  });
});
