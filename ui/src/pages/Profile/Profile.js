import React, { Fragment } from 'react';
import { getPrincipal } from '../../utils/principal';
import AccountApi from '../../api/account';
import OrderApi from '../../api/order';
import Section from '../../components/Section/Section';
import Order from '../../components/Order/Order';
import FeatureTogglesContext from '../../featureTogglesContext';

class Profile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const usernameFromUrl = this.props?.match?.params?.username;
    const username = usernameFromUrl || getPrincipal().username;

    const account = await AccountApi.get(username);
    const orders = await OrderApi.getOrders(username);

    this.setState({ account, orders });
  }

  renderAccount() {
    const NO_COUNTRY_MESSAGE = 'No Country Set';
    const NO_ADDRESS_MESSAGE = 'No Address Set';

    const { account } = this.state;

    return (
      (account && (
        <Section title="Your details">
          <div>
            <span className="label">Account name:</span>
            <span>{account.accountName}</span>
          </div>

          <div>
            <span className="label">Email address:</span>
            <span>{account.emailAddress}</span>
          </div>

          {account.phoneNumber && (
            <div>
              <span className="label">Phone number:</span>
              <span>{account.phoneNumber}</span>
            </div>
          )}

          <div>
            <span className="label">Country:</span>
            <span>{account.country || NO_COUNTRY_MESSAGE}</span>
          </div>

          <FeatureTogglesContext.Consumer>
            {toggle => {
              if (toggle['address']) {
                return (
                  <Fragment>
                    <div>
                      <span className="label">City:</span>
                      <span>{account.city || NO_ADDRESS_MESSAGE}</span>
                    </div>
                    <div>
                      <span className="label">Zipcode:</span>
                      <span>{account.zipcode || NO_ADDRESS_MESSAGE}</span>
                    </div>
                    <div>
                      <span className="label">Street:</span>
                      <span>{account.street || NO_ADDRESS_MESSAGE}</span>
                    </div>
                    <div>
                      <span className="label">Street Number:</span>
                      <span>{account.streetNumber || NO_ADDRESS_MESSAGE}</span>
                    </div>
                    <div>
                      <span className="label">Unit Number:</span>
                      <span>{account.unitNumber || '-'}</span>
                    </div>
                  </Fragment>
                );
              }
            }}
          </FeatureTogglesContext.Consumer>
        </Section>
      )) ||
      null
    );
  }

  renderOrders() {
    const { orders } = this.state;
    return (
      (orders && (
        <Section title="Orders">
          {orders.map(order => (
            <Order key={order.id} {...order} />
          ))}
        </Section>
      )) ||
      null
    );
  }

  render() {
    return (
      <>
        {this.renderAccount()}
        {this.renderOrders()}
      </>
    );
  }
}

export default Profile;
