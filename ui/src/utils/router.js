import {
  BASIC_ROUTER_CONFIG,
  GLOBAL_ERROR_ROUTER_CONFIG,
  AUTH_ROUTER_CONFIG
} from './RouterConfig/routerConfig';
import { getPrincipal } from './principal';

const getRouterConfigWithRole = role =>
  AUTH_ROUTER_CONFIG.filter(router => router.auth.includes(role));

export const getRouterConfig = () => {
  const role = getPrincipal().role;

  const routerConfig = role
    ? BASIC_ROUTER_CONFIG.concat(getRouterConfigWithRole(role))
    : BASIC_ROUTER_CONFIG;

  return routerConfig.concat(GLOBAL_ERROR_ROUTER_CONFIG);
};
