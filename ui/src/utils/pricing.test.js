import { getFinalPrice, getVATAmount } from './pricing';

describe('pricing', () => {
  describe('getFinalPrice', () => {
    it('should return 25.00 when price is 20 and VAT is 0.2', () => {
      expect(getFinalPrice(20, { UK: 0.2 })).toEqual('25.00');
    });

    it('should return 0.00 when price is 0 and VAT is 0.2', () => {
      expect(getFinalPrice(0, { UK: 0.2 })).toEqual('0.00');
    });

    it('should return 5000.00 when price is 1000 and VAT is 0.8', () => {
      expect(getFinalPrice(1000, { UK: 0.8 })).toEqual('5000.00');
    });
  });

  describe('getVATAmount', () => {
    it('should return 5.00 when price is 20 and VAT is 0.2', () => {
      expect(getVATAmount(20, { UK: 0.2 })).toEqual('5.00');
    });

    it('should return 0.00 when price is 0 and VAT is 0.2', () => {
      expect(getVATAmount(0, { UK: 0.2 })).toEqual('0.00');
    });

    it('should return 4000.00 when price is 1000 and VAT is 0.8', () => {
      expect(getVATAmount(1000, { UK: 0.8 })).toEqual('4000.00');
    });
  });
});
