import { getRouterConfig } from './router';
import { getPrincipal } from './principal';

jest.mock('./principal');
jest.mock('./RouterConfig/routerConfig', () => ({
  BASIC_ROUTER_CONFIG: [
    {
      path: '/',
      exact: true,
      component: null,
      auth: []
    }
  ],
  GLOBAL_ERROR_ROUTER_CONFIG: [
    {
      path: '(/not-found|/*)',
      exact: false,
      component: null,
      auth: []
    }
  ],
  AUTH_ROUTER_CONFIG: [
    {
      path: '/admin',
      exact: false,
      component: null,
      auth: ['ROLE_ADMIN']
    },
    {
      path: '/profile',
      exact: false,
      component: null,
      auth: ['ROLE_USER']
    }
  ]
}));

describe('get router config', () => {
  it('should get router without role', () => {
    getPrincipal.mockReturnValue({});
    const routers = getRouterConfig();
    expect(routers).toHaveLength(2);
  });

  it('should get router without role', () => {
    getPrincipal.mockReturnValue({ username: 'test', role: 'ROLE_ADMIN' });
    const routers = getRouterConfig();
    expect(routers).toHaveLength(3);
  });
});
