import {
  isRequiredError,
  getAccountNameError,
  getEmailError,
  getPasswordError,
  getPhoneNumberError,
  generateValidator,
  getPriceError,
  getQuantityError,
  getCountryError,
  getCityError,
  getZipcodeError,
  getStreetError,
  getStreetNumberError,
  getCardNumberError,
  getCardCSCError,
  getCardExpireDateError
} from './formValidate';

describe('createAccountValidator', () => {
  const validator = generateValidator({
    emailAddress: getEmailError,
    password: getPasswordError,
    accountName: getAccountNameError,
    phoneNumber: getPhoneNumberError,
    country: getCountryError,
    city: getCityError,
    zipcode: getZipcodeError,
    street: getStreetError,
    streetNumber: getStreetNumberError
  });

  const invalidAccount = {
    emailAddress: '',
    password: '',
    accountName: '',
    phoneNumber: '',
    country: '',
    city: '',
    zipcode: '',
    street: '',
    streetNumber: '',
    unitNumber: 'This value is too long for this field'
  };

  const validAccount = {
    emailAddress: 'newEmail@example.com',
    password: 'N3w password value',
    accountName: 'another name',
    phoneNumber: '55512345',
    country: 'UK',
    city: 'London',
    zipcode: 'ABC 123',
    street: 'Elm Street',
    streetNumber: '97'
  };

  describe('isValid', () => {
    it('is valid when all fields are valid', () => {
      expect(validator.isValid(validAccount)).toBe(true);
    });

    it('is invalid when any field is invalid', () => {
      const result = validator.isValid({
        ...validAccount,
        emailAddress: ''
      });

      expect(result).toBe(false);
    });
  });

  describe('getErrors', () => {
    it('does not show errors when no fields have errors', () => {
      expect(validator.getErrors(validAccount)).toEqual({});
    });

    it('shows errors in all fields with validation', () => {
      expect(validator.getErrors(invalidAccount)).toEqual({
        accountName: 'required',
        emailAddress: 'required',
        password: 'required',
        phoneNumber: 'required',
        country: 'Please select a country to continue.',
        city: 'This field is Mandatory',
        zipcode: 'This field is Mandatory',
        street: 'This field is Mandatory',
        streetNumber: 'This field is Mandatory'
      });
    });
  });
});

describe('fields', () => {
  describe('isRequired', () => {
    it('shows error with empty field value', () => {
      expect(isRequiredError('')).toBe('required');
    });

    it('shows error with undefined field value', () => {
      expect(isRequiredError(undefined)).toBe('required');
    });

    it('does not return anything when is required field is valid', () => {
      expect(isRequiredError('required field value')).toBeUndefined();
    });
  });

  describe('price', () => {
    it('errors when price is empty', () => {
      expect(getPriceError('')).toBe('required');
    });

    it('errors when price is invalid', () => {
      expect(getPriceError('10.111')).toBe('invalid price');
    });

    it('errors when price is zero', () => {
      expect(getPriceError('0')).toBe('invalid price');
    });

    it('does not return anything when price is valid', () => {
      expect(getPriceError('12230.23')).toBeUndefined();
    });
  });

  describe('quantity', () => {
    it('errors when quantity is empty', () => {
      expect(getQuantityError('')).toBe('required');
    });

    it('errors when quantity is invalid', () => {
      expect(getQuantityError('1.1')).toBe('invalid quantity');
    });

    it('errors when quantity is zero', () => {
      expect(getQuantityError('0')).toBe('invalid quantity');
    });

    it('does not return anything when quantity is valid', () => {
      expect(getQuantityError('1')).toBeUndefined();
    });
  });

  describe('email', () => {
    it('shows error with email format', () => {
      expect(getEmailError('@a')).toBe('incorrect format');
    });

    it('does not return anything when email is valid', () => {
      expect(getEmailError('a@a.com')).toBeUndefined();
    });
  });

  describe('password', () => {
    it('shows password is too short', () => {
      expect(getPasswordError('1234567')).toEqual('too short');
    });

    it('shows password needs at least 1 upper case letter', () => {
      expect(getPasswordError('aaaaaaaa1')).toBe(
        'requires at least one upper and lower case letter and number'
      );
    });

    it('shows password needs at least 1 lower case letter', () => {
      expect(getPasswordError('AAAAAAAA1')).toBe(
        'requires at least one upper and lower case letter and number'
      );
    });

    it('shows password needs at least 1 number', () => {
      expect(getPasswordError('Aaaaaaaa')).toBe(
        'requires at least one upper and lower case letter and number'
      );
    });

    it('does not return anything when there are no password errors', () => {
      expect(getPasswordError('Aaaaaaa1')).toBeUndefined();
    });
  });

  describe('accountName', () => {
    it('shows name cannot be blank', () => {
      expect(getAccountNameError('')).toBe('required');
    });

    it('does not return anything when name is valid', () => {
      expect(getAccountNameError('John')).toBeUndefined();
    });
  });

  describe('phoneNumber', () => {
    it('shows phone number cannot be blank', () => {
      expect(getPhoneNumberError('')).toBe('required');
    });

    it('shows phone number cannot contain non-numeric characters', () => {
      expect(getPhoneNumberError('my phone number')).toBe(
        'only numbers allowed'
      );
      expect(getPhoneNumberError('$999')).toBe('only numbers allowed');
      expect(getPhoneNumberError('1-2')).toBe('only numbers allowed');
      expect(getPhoneNumberError('+123456789')).toBe('only numbers allowed');
    });

    it('does not return anything when name is valid', () => {
      expect(getPhoneNumberError('123456789')).toBeUndefined();
    });
  });
  describe('country', () => {
    it('should not be able to select a country that is not in the list', () => {
      expect(getCountryError('Brazil')).toBe('Please select a valid country!');
    });
  });

  describe('address', () => {
    it('does not return anything when city is valid', () => {
      expect(getCityError('Salvador')).toBeUndefined();
    });
    it('does not return anything when zipcode is valid', () => {
      expect(getZipcodeError('ABC 1234')).toBeUndefined();
    });
  });
});

describe('credit card validator', () => {
  const validator = generateValidator({
    creditCardNumber: getCardNumberError,
    csc: getCardCSCError,
    expireDate: getCardExpireDateError
  });

  const validCreditCard = {
    creditCardNumber: '378282246310005',
    csc: '7997',
    expireDate: '09-2025'
  };

  it('is valid when all fields are valid', () => {
    expect(validator.isValid(validCreditCard)).toBe(true);
  });

  it('is invalid when any field is invalid', () => {
    const result = validator.isValid({
      ...validCreditCard,
      creditCardNumber: ''
    });

    expect(result).toBe(false);
  });

  it('is invalid when credit card number has less than 15 digits', () => {
    const result = validator.isValid({
      ...validCreditCard,
      creditCardNumber: '41111111111111'
    });

    expect(result).toBe(false);
  });

  it('is invalid when credit card number has more than 16 digits', () => {
    const result = validator.isValid({
      ...validCreditCard,
      creditCardNumber: '60111111111111178'
    });

    expect(result).toBe(false);
  });

  it('is invalid when csc number has less than 3 digits', () => {
    const result = validator.isValid({
      ...validCreditCard,
      csc: '12'
    });

    expect(result).toBe(false);
  });

  it('is invalid when csc number has more than 4 digits', () => {
    const result = validator.isValid({
      ...validCreditCard,
      csc: '12345'
    });

    expect(result).toBe(false);
  });

  it('is invalid when expire date', () => {
    const result = validator.isValid({
      ...validCreditCard,
      expireDate: '09-20'
    });

    expect(result).toBe(false);
  });
});
