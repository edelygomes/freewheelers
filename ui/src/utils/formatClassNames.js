function formatClassNames(...classes) {
  return classes
    .filter(cls => cls)
    .map(cls => cls.trim())
    .join(' ');
}

export default formatClassNames;
