function fixToTwoDecimal(num) {
  return Number(num).toFixed(2);
}

export const getFinalPrice = (price, vat) => {
  const finalPrice = price / (1 - vat.UK);
  return fixToTwoDecimal(finalPrice);
};

export const getVATAmount = (price, vat) => {
  const VATAmount = getFinalPrice(price, vat) * vat.UK;
  return fixToTwoDecimal(VATAmount);
};
