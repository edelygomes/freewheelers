import Profile from '../../pages/Profile/Profile';
import Home from '../../pages/Home/Home';
import Admin from '../../pages/Admin/Admin';
import ManageItems from '../../pages/ManageItems/ManageItems';
import Login from '../../pages/Login/Login';
import CreateAccount from '../../pages/CreateAccount/CreateAccount';
import OrderConfirmation from '../../pages/OrderConfirmation/OrderConfirmation';
import OrderPayment from '../../pages/OrderPayment/OrderPayment';
import TermsAndConditions from '../../pages/TermsAndConditions/TermsAndConditions';
import FeatureToggles from '../../pages/FeatureToggles/FeatureToggles';
import ServerError from '../../pages/ServerError/ServerError';
import NotFound from '../../pages/NotFound/NotFound';

export const BASIC_ROUTER_CONFIG = [
  {
    path: '/',
    exact: true,
    component: Home,
    auth: []
  },
  {
    path: '/create-account',
    exact: false,
    component: CreateAccount,
    auth: []
  },
  {
    path: '/login',
    exact: false,
    component: Login,
    auth: []
  },
  {
    path: '/terms-conditions',
    exact: false,
    component: TermsAndConditions,
    auth: []
  }
];

export const AUTH_ROUTER_CONFIG = [
  {
    path: '/admin',
    exact: false,
    component: Admin,
    auth: ['ROLE_ADMIN']
  },
  {
    path: '/manage-items',
    exact: false,
    component: ManageItems,
    auth: ['ROLE_ADMIN']
  },
  {
    path: '/profile/:username',
    exact: false,
    component: Profile,
    auth: ['ROLE_ADMIN']
  },
  {
    path: '/profile/:username',
    exact: false,
    component: Home,
    auth: ['ROLE_USER']
  },
  {
    path: '/profile',
    exact: false,
    component: Profile,
    auth: ['ROLE_ADMIN', 'ROLE_USER']
  },
  {
    path: '/order-confirmation/:id',
    exact: false,
    component: OrderConfirmation,
    auth: ['ROLE_ADMIN', 'ROLE_USER']
  },
  {
    path: '/feature-toggles',
    exact: false,
    component: FeatureToggles,
    auth: ['ROLE_ADMIN']
  },
  {
    path: '/order-payment/:id',
    exact: false,
    component: OrderPayment,
    auth: ['ROLE_ADMIN', 'ROLE_USER']
  }
];

export const GLOBAL_ERROR_ROUTER_CONFIG = [
  {
    path: '/server-error',
    exact: false,
    component: ServerError,
    auth: []
  },
  {
    path: '(/not-found|/*)',
    exact: false,
    component: NotFound,
    auth: []
  }
];
