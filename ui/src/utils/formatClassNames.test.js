import formatClassNames from './formatClassNames';

describe('formatClassNames', () => {
  it('should join multiple classes into a space-separated string', () => {
    const generated = formatClassNames('one', 'two', 'three');
    expect(generated).toEqual('one two three');
  });

  it('should trim any leading or trailing whitespace from each element', () => {
    const generated = formatClassNames(
      'trailing-space ',
      '  many-spaces  ',
      ' leading-space'
    );
    expect(generated).toEqual('trailing-space many-spaces leading-space');
  });

  it('should filter out undefined or null', () => {
    const generated = formatClassNames('one', undefined, 'two', null);
    expect(generated).toEqual('one two');
  });

  it('should return empty string if there are no elements', () => {
    const generated = formatClassNames('', undefined);
    expect(generated).toEqual('');
  });
});
