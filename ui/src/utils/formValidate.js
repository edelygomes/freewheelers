import countryList from '../constants/countryList';

const isRequiredError = formValue => {
  if (!formValue || formValue.length === 0) {
    return 'required';
  }
};

const getPriceError = price => {
  if (price.length === 0) {
    return 'required';
  }
  if (!/^\d+.?\d{0,2}$/.test(price) || price <= 0) {
    return 'invalid price';
  }
};

const getQuantityError = quantity => {
  if (quantity.length === 0) {
    return 'required';
  }
  if (!/^\d+$/.test(quantity) || quantity < 1) {
    return 'invalid quantity';
  }
};

const getEmailError = emailAddress => {
  if (emailAddress.length === 0) {
    return 'required';
  }

  if (!/.+@.+\..+/.test(emailAddress)) {
    return 'incorrect format';
  }
};

const getPasswordError = password => {
  if (password.length === 0) {
    return 'required';
  }

  if (password.length < 8) {
    return 'too short';
  }

  if (
    !/[A-Z]/.test(password) ||
    !/[a-z]/.test(password) ||
    !/[0-9]/.test(password)
  ) {
    return 'requires at least one upper and lower case letter and number';
  }
};

const getCountryError = country => {
  if (country === undefined) return;

  if (country.length === 0) {
    return 'Please select a country to continue.';
  } else if (!countryList.includes(country)) {
    return 'Please select a valid country!';
  }
};

const getTermsAndConditionsError = termsAndConditions => {
  if (termsAndConditions === undefined) return;
  if (!termsAndConditions) {
    return "You must accept this site's terms and conditions to proceed.";
  }
};

const getAccountNameError = name => {
  if (!name || name.length === 0) {
    return 'required';
  }
};

const getPhoneNumberError = phoneNumber => {
  if (!phoneNumber || phoneNumber.length === 0) {
    return 'required';
  }

  if (/\D+/.test(phoneNumber)) {
    return 'only numbers allowed';
  }
};

const getCityError = city => {
  if (city === undefined) return;

  if (!city || city.length === 0) {
    return 'This field is Mandatory';
  }
};

const getZipcodeError = zipcode => {
  if (zipcode === undefined) return;

  if (!zipcode || zipcode.length === 0) {
    return 'This field is Mandatory';
  }
};

const getStreetError = street => {
  if (street === undefined) return;

  if (!street || street.length === 0) {
    return 'This field is Mandatory';
  }
};

const getStreetNumberError = streetNumber => {
  if (streetNumber === undefined) return;

  if (!streetNumber || streetNumber.length === 0) {
    return 'This field is Mandatory';
  }
};

const getUnitNumberError = streetNumber => {
  if (streetNumber === undefined) return;

  if (!streetNumber || streetNumber.length > 15) {
    return 'This value is too long for this field';
  }
};

const getCardNumberError = number => {
  if (!number || number.length === 0) {
    return 'required';
  }

  if (!number || number.length > 16) {
    return 'This value is too long for this field';
  }
  if (!number || number.length < 15) {
    return 'This value is too short for this field';
  }
};

const getCardExpireDateError = expireDate => {
  if (!expireDate || expireDate.length === 0) {
    return 'required';
  }
  if (!expireDate.match(/^[0-9]{2}-[0-9]{4}$/)) {
    return 'format should be MM-YYYY';
  }
};

const getCardCSCError = csc => {
  if (!csc || csc.length === 0) {
    return 'required';
  }
  if (!csc || csc.length > 4) {
    return 'This value is too long for this field';
  }
  if (!csc || csc.length < 3) {
    return 'This value is too short for this field';
  }
};

const generateValidator = fields => {
  return {
    getErrors: objectToBeValidated => {
      const errors = {};
      Object.entries(fields).forEach(([field, getFieldError]) => {
        errors[field] = getFieldError(objectToBeValidated[field]);
      });
      return errors;
    },
    isValid: objectToBeValidated =>
      Object.entries(fields).every(
        ([field, getFieldError]) => !getFieldError(objectToBeValidated[field])
      ),
    fields
  };
};

export {
  isRequiredError,
  getQuantityError,
  getPriceError,
  getEmailError,
  getAccountNameError,
  getPasswordError,
  getPhoneNumberError,
  generateValidator,
  getCountryError,
  getCityError,
  getZipcodeError,
  getStreetError,
  getStreetNumberError,
  getUnitNumberError,
  getTermsAndConditionsError,
  getCardNumberError,
  getCardExpireDateError,
  getCardCSCError
};
