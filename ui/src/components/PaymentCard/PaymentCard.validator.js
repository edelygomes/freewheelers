import {
  getCardNumberError,
  getCardExpireDateError,
  getCardCSCError,
  generateValidator
} from '../../utils/formValidate';

const fields = {
  creditCardNumber: getCardNumberError,
  expireDate: getCardExpireDateError,
  csc: getCardCSCError
};

const createCardValidator = generateValidator(fields);

export default createCardValidator;
