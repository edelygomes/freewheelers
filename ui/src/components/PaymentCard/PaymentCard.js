import React from 'react';
import InputField from '../InputField/InputField';
import CreditCardPaymentApi from '../../api/creditCardPayment';
import validator from './PaymentCard.validator';

class PaymentCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cardPayment: true,
      creditCardNumber: '',
      expireDate: '',
      csc: '',
      errors: {}
    };
  }

  togglePaymentByCash = () => {
    this.setState({ cardPayment: false });
  };

  togglePaymentByCard = () => {
    this.setState({ cardPayment: true });
  };

  handleChangeFor = fieldName => event => {
    this.setState({ [fieldName]: event.target.value });
  };

  handleBlurFor = fieldName => event => {
    const validate = validator.fields[fieldName];

    this.setState({
      errors: {
        ...this.state.errors,
        [fieldName]: validate(event.target.value)
      }
    });
  };

  getCurrentCardState = () => ({
    creditCardNumber: this.state.creditCardNumber,
    expireDate: this.state.expireDate,
    csc: this.state.csc
  });

  validateForm = card => {
    if (validator.isValid(card)) {
      return true;
    }
    this.setState({
      errors: {
        ...validator.getErrors(card),
        form: 'please correct the fields above'
      }
    });
  };

  handleSubmit = () => async event => {
    event.preventDefault();
    const card = this.getCurrentCardState();
    const cardWithItemId = {
      ...card,
      itemId: this.props.itemId
    };

    if (!this.validateForm(card)) return;
    try {
      await CreditCardPaymentApi.submitPayment(cardWithItemId);
    } catch (error) {
      this.setState(() => ({
        errors: { form: error.message }
      }));
    }
  };

  displayCashContent = () => {
    return (
      <div className="cash-information-message">
        <p>We will send you an e-mail with all your order information.</p>
      </div>
    );
  };

  displayCardForm = () => {
    return (
      <form className="card-information-form" onSubmit={this.handleSubmit()}>
        <InputField
          label="Number:"
          changeHandler={this.handleChangeFor('creditCardNumber')}
          blurHandler={this.handleBlurFor('creditCardNumber')}
          error={this.state.errors.creditCardNumber}
        />
        <InputField
          label="Expire date:"
          changeHandler={this.handleChangeFor('expireDate')}
          blurHandler={this.handleBlurFor('expireDate')}
          error={this.state.errors.expireDate}
        />
        <InputField
          label="CSC:"
          changeHandler={this.handleChangeFor('csc')}
          blurHandler={this.handleBlurFor('csc')}
          error={this.state.errors.csc}
        />
        <input
          type="submit"
          className="button button--primary"
          value="Finish"
        />
        <span className="input-error">{this.state.errors.form}</span>
      </form>
    );
  };

  render() {
    return (
      <div className="payment-card-container">
        <h2>How do you want to pay?</h2>
        <div>
          <button onClick={this.togglePaymentByCard} className="card-button">
            Credit card
          </button>
          <button onClick={this.togglePaymentByCash} className="cash-button">
            By cash in delivery
          </button>
          {this.state.cardPayment
            ? this.displayCardForm()
            : this.displayCashContent()}
        </div>
      </div>
    );
  }
}

export default PaymentCard;
