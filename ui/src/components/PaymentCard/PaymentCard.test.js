import React from 'react';
import { mount } from 'enzyme';
import CreditCardPaymentApi from '../../api/creditCardPayment';
import PaymentCard from './PaymentCard';
jest.mock('../../api/creditCardPayment');

describe('<CreateAccount />', () => {
  const validInputs = {
    creditCardNumber: '123456789012345',
    expireDate: '05-2020',
    csc: '436'
  };

  let wrapper;

  beforeEach(() => {
    wrapper = mount(<PaymentCard />);
  });

  it('should display message if payment by cash is chosen', function() {
    wrapper.find('.cash-button').simulate('click');
    expect(wrapper.find('.cash-information-message').text()).toContain(
      'We will send you an e-mail with all your order information.'
    );

    wrapper.find('.card-button').simulate('click');
    expect(wrapper.find('.card-information-form').text()).toContain('Number');
    expect(wrapper.find('.card-information-form').text()).toContain(
      'Expire date'
    );
    expect(wrapper.find('.card-information-form').text()).toContain('CSC');
  });

  it('should show errors when submitting any invalid inputs', () => {
    wrapper.find('form').simulate('submit');

    expect(wrapper.text()).toContain('please correct the fields above');
  });

  it('should clear the error when value is corrected and blurred', () => {
    wrapper.find('form').simulate('submit');
    wrapper
      .find('#number')
      .simulate('change', { target: { value: validInputs.creditCardNumber } });
    wrapper.find('#number').simulate('blur');

    expect(wrapper.text()).not.toContain('error');
  });

  it('should generate a payment with entered values', () => {
    CreditCardPaymentApi.submitPayment.mockResolvedValue({});

    wrapper.find('.card-button').simulate('click');
    wrapper
      .find('#number')
      .simulate('change', { target: { value: validInputs.creditCardNumber } });
    wrapper
      .find('#expiredate')
      .simulate('change', { target: { value: validInputs.expireDate } });
    wrapper
      .find('#csc')
      .simulate('change', { target: { value: validInputs.csc } });

    wrapper.find('form').simulate('submit');

    expect(CreditCardPaymentApi.submitPayment).toHaveBeenCalledWith(
      validInputs
    );
  });
});
