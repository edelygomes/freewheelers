import React from 'react';
import formatClassNames from '../../utils/formatClassNames';
import './AlertMessage.css';

function AlertMessage(props) {
  const Tag = props.inline ? 'span' : 'div';

  return (
    <Tag
      className={formatClassNames(
        'alert-message',
        props.isSuccessful ? 'input-success' : 'input-error',
        props.className
      )}
    >
      {props.message}
    </Tag>
  );
}

export default AlertMessage;
