import React from 'react';
import { shallow } from 'enzyme';
import AlertMessage from './AlertMessage';

jest.mock('../../api/order');
jest.mock('../../history');

describe('<Item />', () => {
  it('should render a successful alert message', () => {
    const wrapper = shallow(
      <AlertMessage isSuccessful={true} message="Action was successful!" />
    );

    expect(wrapper.find('.input-success').exists()).toBe(true);
    expect(wrapper.find('div').text()).toEqual('Action was successful!');
  });

  it('should render a failure alert message', () => {
    const wrapper = shallow(
      <AlertMessage isSuccessful={false} message="Action failed" />
    );

    expect(wrapper.find('.input-error').exists()).toBe(true);
  });

  it('should render an inline message', () => {
    const wrapper = shallow(
      <AlertMessage
        isSuccessful={false}
        message="Inline message"
        inline={true}
      />
    );

    expect(wrapper.find('span').text()).toEqual('Inline message');
  });
});
