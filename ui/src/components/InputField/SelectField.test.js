import React from 'react';
import { shallow } from 'enzyme/build';
import SelectField from './SelectField';

describe('<SelectField>', () => {
  const defaultProps = {
    id: 'id',
    label: 'Value',
    error: '',
    options: ['Option 1', 'Option 2'],
    placeholder: 'Please select an option'
  };

  it('sets id and label', () => {
    const wrapper = shallow(<SelectField {...defaultProps} />);

    expect(wrapper.find('.form-label').text()).toEqual('Value');
    expect(wrapper.find('select').prop('id')).toEqual('id-select');
    expect(wrapper.find('.input-error').exists()).toBe(false);
  });

  it('calls changehandler on change', () => {
    const onChangeHandler = jest.fn();

    const wrapper = shallow(
      <SelectField {...defaultProps} changeHandler={onChangeHandler} />
    );

    wrapper.find('select').simulate('change', { value: 'some option' });

    expect(onChangeHandler).toHaveBeenCalledWith({ value: 'some option' });
  });

  it('calls blurhandler on blur', () => {
    const onBlurHandler = jest.fn();

    const wrapper = shallow(
      <SelectField {...defaultProps} blurHandler={onBlurHandler} />
    );

    wrapper.find('select').simulate('blur', { value: 'some option 2' });

    expect(onBlurHandler).toHaveBeenCalledWith({ value: 'some option 2' });
  });

  it('displays placeholder and options', () => {
    const wrapper = shallow(<SelectField {...defaultProps} />);

    const options = wrapper.find('option');

    expect(options).toHaveLength(3);
    expect(options.at(0).text()).toEqual('Please select an option');
    expect(options.at(1).text()).toEqual('Option 1');
    expect(options.at(2).text()).toEqual('Option 2');
  });

  it('displays the error message if error prop is present', () => {
    const wrapper = shallow(
      <SelectField {...defaultProps} error="Something is wrong" />
    );

    expect(wrapper.find('.input-error').text()).toEqual('Something is wrong');
  });
});
