import React from 'react';

function doNothing() {}

const CheckboxField = props => {
  const { id, label, error, changeHandler, ...rest } = props;

  const onChange = changeHandler || doNothing;

  return (
    <label htmlFor={id}>
      <input
        id={id}
        type={'checkbox'}
        onChange={event => onChange(event)}
        {...rest}
      />

      <span className="form-label">{label}</span>

      {error && <span className="input-error">{error}</span>}
    </label>
  );
};

export default CheckboxField;
