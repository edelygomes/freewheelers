import React from 'react';
import { shallow } from 'enzyme/build';
import InputField from './InputField';

describe('<InputField>', () => {
  const defaultProps = {
    id: 123,
    label: 'Email address',
    type: 'email'
  };

  it('should set id, label and type', () => {
    const wrapper = shallow(<InputField {...defaultProps} />);

    expect(wrapper.find('.form-label').text()).toEqual('Email address');
    expect(wrapper.find('label').prop('htmlFor')).toEqual(123);
    expect(wrapper.find('input').prop('id')).toEqual(123);
    expect(wrapper.find('input').prop('type')).toEqual('email');
    expect(wrapper.find('.input-error').exists()).toBe(false);
  });

  it('should set rest of the props on input', () => {
    const wrapper = shallow(<InputField {...defaultProps} foo="bar" />);

    expect(wrapper.find('input').prop('foo')).toEqual('bar');
  });

  it('should generate an ID to associate label and input if no id is given', () => {
    const wrapper = shallow(<InputField label="Phone Number" />);

    expect(wrapper.find('input').prop('id')).toEqual('phonenumber');
  });

  it('should default to type=text if no type is given', () => {
    const wrapper = shallow(<InputField label="Password" />);

    expect(wrapper.find('input').prop('type')).toEqual('text');
  });

  it('should display the error message if error prop is present', () => {
    const wrapper = shallow(<InputField {...defaultProps} error="Bad input" />);

    expect(wrapper.find('.input-error').text()).toEqual('Bad input');
  });

  it('should call changeHandler function when a change is detected', () => {
    const mockEvent = { target: { value: 'hi' } };
    const mockChangeHandler = jest.fn();
    const wrapper = shallow(
      <InputField {...defaultProps} changeHandler={mockChangeHandler} />
    );

    wrapper.find('input').simulate('change', mockEvent);
    wrapper.update();

    expect(mockChangeHandler).toHaveBeenCalledWith(mockEvent);
  });

  it('should not fail if a changeHandler is not passed in', () => {
    const wrapper = shallow(<InputField {...defaultProps} />);

    wrapper.find('input').simulate('change', { target: { value: '' } });
    wrapper.update();

    expect(wrapper.find('input').text()).toEqual('');
  });

  it('should call changeHandler function when a change is detected', () => {
    const mockEvent = { target: { value: 'hi' } };
    const mockBlurHandler = jest.fn();
    const wrapper = shallow(
      <InputField {...defaultProps} blurHandler={mockBlurHandler} />
    );

    wrapper.find('input').simulate('blur', mockEvent);
    wrapper.update();

    expect(mockBlurHandler).toHaveBeenCalledWith(mockEvent);
  });

  it('should not fail if a blurHandler is not passed in', () => {
    const wrapper = shallow(<InputField {...defaultProps} />);

    wrapper.find('input').simulate('blur');
    wrapper.update();

    expect(wrapper.find('input').text()).toEqual('');
  });
});
