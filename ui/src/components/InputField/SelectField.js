import React from 'react';

function SelectField(props) {
  const {
    value,
    options,
    placeholder,
    id,
    label,
    changeHandler,
    blurHandler,
    error
  } = props;
  const fieldId = id + '-select';
  return (
    <label htmlFor={fieldId}>
      {label && <span className="form-label">{label}</span>}
      <select
        id={fieldId}
        onChange={changeHandler}
        onBlur={blurHandler}
        value={value}
      >
        {placeholder && (
          <option value="" disabled>
            {placeholder}
          </option>
        )}
        {options.map(option => (
          <option key={option}>{option}</option>
        ))}
      </select>
      {error && <span className="input-error">{error}</span>}
    </label>
  );
}

export default SelectField;
