import React from 'react';
import { shallow } from 'enzyme/build';
import CheckboxField from './CheckboxField';

describe('<CheckboxField>', () => {
  const defaultProps = {
    id: 123,
    label: <p>Terms and Conditions</p>
  };

  it('should set id and label', () => {
    const wrapper = shallow(<CheckboxField {...defaultProps} />);

    expect(wrapper.find('.form-label p').text()).toEqual(
      'Terms and Conditions'
    );
    expect(wrapper.find('label').prop('htmlFor')).toEqual(123);
    expect(wrapper.find('input').prop('id')).toEqual(123);
    expect(wrapper.find('input').prop('type')).toEqual('checkbox');
    expect(wrapper.find('.input-error').exists()).toBe(false);
  });

  it('should display the error message if error prop is present', () => {
    const wrapper = shallow(
      <CheckboxField {...defaultProps} error="Bad checkbox" />
    );

    expect(wrapper.find('.input-error').text()).toEqual('Bad checkbox');
  });

  it('should call changeHandler function when a change is detected', () => {
    const mockEvent = { target: { value: true } };
    const mockChangeHandler = jest.fn();
    const wrapper = shallow(
      <CheckboxField {...defaultProps} changeHandler={mockChangeHandler} />
    );

    wrapper.find('input').simulate('change', mockEvent);
    wrapper.update();

    expect(mockChangeHandler).toHaveBeenCalledWith(mockEvent);
  });
});
