import React from 'react';
import { mount } from 'enzyme';
import FeatureTogglesContext from '../../featureTogglesContext';
import Item from './Item';

jest.mock('../../api/order');
jest.mock('../../history');

describe('<Item />', () => {
  const props = {
    id: 2,
    name: 'Bike gloves',
    type: 'ACCESSORIES',
    description: 'Description of cool item',
    quantity: 1,
    price: 10.99
  };

  it('should render an item', () => {
    const wrapper = mount(
      <FeatureTogglesContext.Provider value={{ VAT: false }}>
        <Item {...props} />
      </FeatureTogglesContext.Provider>
    );

    expect(wrapper.find('.item').exists()).toBe(true);
    expect(wrapper.find('.item__quantity').text()).toEqual('In stock: 1');
    expect(wrapper.find('.item__price').text()).toEqual('£ 10.99');
  });

  it('should price + vat value', function() {
    const wrapper = mount(
      <FeatureTogglesContext.Provider value={{ VAT: true }}>
        <Item {...props} />
      </FeatureTogglesContext.Provider>
    );

    expect(wrapper.find('.item__price').text()).toEqual('£ 13.74');
    wrapper.find('.vat__info').simulate('mouseenter');
    expect(wrapper.find('#vat__details').text()).toEqual(
      '£ 2.75 (20% UK VAT)Product = £ 10.99'
    );
  });
});
