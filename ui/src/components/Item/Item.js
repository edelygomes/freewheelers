import React from 'react';
import './Item.css';
import FeatureTogglesContext from '../../featureTogglesContext';
import { getVATAmount, getFinalPrice } from '../../utils/pricing';

import ico_info from '../../assets/icons/ico_info.png';

class Item extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
      VAT: {
        UK: 0.2
      }
    };
  }

  handleHover = isHover => {
    this.setState({ isHover: isHover });
  };

  showVATDetails = () => {
    return (
      <div id="vat__details" className="bubble-text">
        <p>
          £ {getVATAmount(this.props.price, this.state.VAT)} (
          {this.state.VAT.UK * 100}% UK VAT)
        </p>
        <p>Product = £ {this.props.price}</p>
      </div>
    );
  };

  render() {
    return (
      <div className="item">
        <div className="item__name">{this.props.name}</div>
        <div className="item__type">{this.props.type}</div>
        <div className="item__description">{this.props.description}</div>
        {this.props.quantity && (
          <div className="item__quantity">In stock: {this.props.quantity}</div>
        )}
        <FeatureTogglesContext.Consumer>
          {toggles => {
            if (toggles['VAT']) {
              return (
                <div className="price__container">
                  <div>
                    <div className="item__price">
                      £ {getFinalPrice(this.props.price, this.state.VAT)}
                    </div>
                    <div className="vat__container">
                      <p>VAT included</p>
                      <span
                        role="button"
                        tabIndex={0}
                        className="vat__info"
                        onMouseEnter={() => this.handleHover(true)}
                        onMouseLeave={() => this.handleHover(false)}
                      >
                        <img className="vat__icon" src={ico_info} alt="info" />
                      </span>
                    </div>
                  </div>
                  {this.state.isHover ? this.showVATDetails() : null}
                </div>
              );
            } else {
              return <div className="item__price">£ {this.props.price}</div>;
            }
          }}
        </FeatureTogglesContext.Consumer>
      </div>
    );
  }
}

export default Item;
