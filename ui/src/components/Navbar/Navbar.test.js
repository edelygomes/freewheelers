import React from 'react';
import { shallow, mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import * as principal from '../../utils/principal';
import Navbar from './Navbar';
jest.mock('../../utils/principal');
jest.mock('../../api/login');

describe('<Navbar />', () => {
  beforeEach(() => {
    principal.getPrincipal.mockReturnValue({ username: undefined });
  });

  it('should render a navbar logo', () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.find('.navbar__logo')).toHaveLength(1);
  });

  it('should render navbar links', () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.exists('Link')).toEqual(true);
  });

  it('should go to home page when clicking on the logo image', () => {
    const wrapper = mount(
      <MemoryRouter>
        <Navbar />
      </MemoryRouter>
    );
    wrapper.find('a.navbar__home-link').simulate('click');
    expect(wrapper.instance().history.location.pathname).toEqual('/');
  });

  describe('not logged in', () => {
    it('should go to login page after click login link', () => {
      const wrapper = mount(
        <MemoryRouter>
          <Navbar />
        </MemoryRouter>
      );

      //https://github.com/airbnb/enzyme/issues/516
      wrapper.find('a.navbar__login').simulate('click', { button: 0 });

      expect(wrapper.instance().history.location.pathname).toEqual('/login');
    });

    it('should go to create account page after click create account link', () => {
      const wrapper = mount(
        <MemoryRouter>
          <Navbar />
        </MemoryRouter>
      );

      wrapper.find('a.navbar__create-account').simulate('click', { button: 0 });

      expect(wrapper.instance().history.location.pathname).toEqual(
        '/create-account'
      );
    });
  });

  describe('logged in user', () => {
    beforeEach(() => {
      principal.getPrincipal.mockReturnValue({
        username: 'someUser',
        role: 'ROLE_USER'
      });
    });

    it('should not display "Create Account" if user is logged in', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.exists('.navbar__login')).toEqual(false);
    });

    it('should not display "Login" if user is logged in', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.exists('.navbar__create-account')).toEqual(false);
    });

    it('should not display "Admin" for a ROLE_USER user', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.exists('.navbar__admin')).toEqual(false);
    });

    it('displays welcome message', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.find('.navbar__welcome').text()).toEqual(
        'Welcome someUser'
      );
    });

    it('should go to user profile page after clicking profile link', () => {
      const wrapper = mount(
        <MemoryRouter>
          <Navbar />
        </MemoryRouter>
      );

      //https://github.com/airbnb/enzyme/issues/516
      wrapper.find('a.navbar__profile').simulate('click', { button: 0 });

      expect(wrapper.instance().history.location.pathname).toEqual('/profile');
    });
  });

  describe('logged in admin', () => {
    beforeEach(() => {
      principal.getPrincipal.mockReturnValue({
        username: 'theAdmin',
        role: 'ROLE_ADMIN'
      });
    });

    it('should display "Admin" for a ROLE_ADMIN user', () => {
      const wrapper = shallow(<Navbar />);
      expect(wrapper.exists('.navbar__admin')).toEqual(true);
    });
  });
});
