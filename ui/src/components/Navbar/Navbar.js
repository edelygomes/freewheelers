import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import './Navbar.css';
import { getPrincipal } from '../../utils/principal';
import LoginApi from '../../api/login';

class Navbar extends React.PureComponent {
  onLogoutClick = async () => {
    await LoginApi.logout();
  };

  render() {
    const username = getPrincipal().username;
    const role = getPrincipal().role;

    return (
      <div className="navbar">
        <Link to="/" className="navbar__home-link">
          <img
            className="navbar__logo"
            width="20px"
            src={logo}
            alt="logo.png"
          />
        </Link>

        <ul className="navbar__links">
          {role === 'ROLE_ADMIN' && (
            <li>
              <Link to="/admin" className="navbar__admin">
                Admin
              </Link>
            </li>
          )}

          {!username && (
            <>
              <li>
                <Link to="/login" className="navbar__login">
                  Login
                </Link>
              </li>
              <li>
                <Link to="/create-account" className="navbar__create-account">
                  Create Account
                </Link>
              </li>
            </>
          )}

          {username && (
            <>
              <li>
                <Link to="/profile" className="navbar__profile">
                  Profile
                </Link>
              </li>
              <li className="navbar__welcome">Welcome {username}</li>
              <li>
                <button
                  className="navbar__logout-button"
                  onClick={this.onLogoutClick}
                >
                  Logout
                </button>
              </li>
            </>
          )}
        </ul>
      </div>
    );
  }
}

export default Navbar;
