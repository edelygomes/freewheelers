import React from 'react';
import formatClassNames from '../../utils/formatClassNames';
import './Card.css';

function Card(props) {
  const { children, className, ...rest } = props;

  return (
    <div className={formatClassNames('card', className)} {...rest}>
      {children}
    </div>
  );
}

export default Card;
