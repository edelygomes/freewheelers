import React from 'react';
import { mount } from 'enzyme';
import { Router } from 'react-router-dom';
import history from '../../history';
import FeatureTogglesContext from '../../featureTogglesContext';
import Footer from './Footer';

describe('<Footer />', () => {
  describe('when feature toggle termsAndConditions is enable', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = mount(
        <FeatureTogglesContext.Provider value={{ termsAndConditions: true }}>
          <Router history={history}>
            {' '}
            <Footer />{' '}
          </Router>
          ;
        </FeatureTogglesContext.Provider>
      );
    });

    it('should render the Terms And Conditions Links.', () => {
      expect(wrapper.exists('#terms-and-conditions-link')).toEqual(true);
    });
  });
});
