import React from 'react';
import { Link } from 'react-router-dom';
import FeatureTogglesContext from '../../featureTogglesContext';
import './Footer.css';

function Footer() {
  return (
    <FeatureTogglesContext.Consumer>
      {toggles => {
        if (toggles['termsAndConditions'])
          return (
            <footer className="footer">
              <div className="footer__links">
                <Link
                  id="terms-and-conditions-link"
                  to="/terms-conditions"
                  target="_blank"
                >
                  Terms & Conditions
                </Link>
              </div>
              <p className="footer__copyright">
                Copyright © 2010-2020 FreeWheelers Company all rights reserved.
              </p>
            </footer>
          );
      }}
    </FeatureTogglesContext.Consumer>
  );
}

export default Footer;
