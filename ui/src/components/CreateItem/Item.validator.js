import {
  isRequiredError,
  getPriceError,
  generateValidator,
  getQuantityError
} from '../../utils/formValidate';

const fields = {
  name: isRequiredError,
  description: isRequiredError,
  type: isRequiredError,
  quantity: getQuantityError,
  price: getPriceError
};

const itemValidator = generateValidator(fields);

export default itemValidator;
