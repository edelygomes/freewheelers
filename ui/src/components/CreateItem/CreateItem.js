import React from 'react';
import ItemApi from '../../api/item';
import InputField from '../InputField/InputField';
import AlertMessage from '../AlertMessage/AlertMessage';
import SelectField from '../InputField/SelectField';
import validator from './Item.validator';

class CreateItem extends React.PureComponent {
  defaultState = {
    name: '',
    description: '',
    type: '',
    price: '',
    quantity: '',
    errors: {}
  };

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultState,
      createSuccess: false,
      createResultMessage: ''
    };
  }

  itemState = () => ({
    name: this.state.name,
    description: this.state.description,
    type: this.state.type,
    price: this.state.price,
    quantity: this.state.quantity
  });

  handleChangeFor = fieldName => event => {
    this.setState({ [fieldName]: event.target.value });
  };

  handleBlurFor = fieldName => event => {
    const validate = validator.fields[fieldName];
    this.setState({
      errors: {
        ...this.state.errors,
        [fieldName]: validate(event.target.value)
      }
    });
  };

  createItem = async event => {
    event.preventDefault();
    if (!validator.isValid(this.itemState())) {
      this.setState({
        errors: validator.getErrors(this.itemState())
      });
      return;
    }
    try {
      await ItemApi.createItem(this.itemState());
      if (this.props.onSuccess) {
        await this.props.onSuccess();
      }

      this.setState({
        ...this.defaultState,
        createSuccess: true,
        createResultMessage: 'Created successfully'
      });
    } catch (error) {
      this.setState({
        createSuccess: false,
        createResultMessage: 'Create failed'
      });
    }
  };

  render() {
    return (
      <form className="create-item" onSubmit={this.createItem}>
        <div className="form-title">Create Item</div>

        <InputField
          label="Name:"
          maxLength="255"
          value={this.state.name}
          changeHandler={this.handleChangeFor('name')}
          blurHandler={this.handleBlurFor('name')}
          error={this.state.errors.name}
        />

        <InputField
          label="Description:"
          maxLength="255"
          value={this.state.description}
          changeHandler={this.handleChangeFor('description')}
          blurHandler={this.handleBlurFor('description')}
          error={this.state.errors.description}
        />

        <SelectField
          id="type"
          label="Type:"
          changeHandler={this.handleChangeFor('type')}
          blurHandler={this.handleBlurFor('type')}
          value={this.state.type}
          options={['FRAME', 'ACCESSORIES']}
          errors={this.state.errors.type}
          placeholder="Select a type"
        />

        <InputField
          label="Quantity:"
          value={this.state.quantity}
          changeHandler={this.handleChangeFor('quantity')}
          blurHandler={this.handleBlurFor('quantity')}
          error={this.state.errors.quantity}
        />

        <InputField
          label="Price:"
          value={this.state.price}
          changeHandler={this.handleChangeFor('price')}
          blurHandler={this.handleBlurFor('price')}
          error={this.state.errors.price}
        />

        <input
          type="submit"
          className="button button--primary"
          value="Create Item"
        />

        <AlertMessage
          isSuccessful={this.state.createSuccess}
          message={this.state.createResultMessage}
          inline={true}
        />
      </form>
    );
  }
}

export default CreateItem;
