import React from 'react';
import { mount } from 'enzyme/build';
import ItemApi from '../../api/item';
import AlertMessage from '../../components/AlertMessage/AlertMessage';
import CreateItem from './CreateItem';
jest.mock('../../api/item');

describe('CreateItem', () => {
  const flushPromises = () => new Promise(setImmediate);

  const item = {
    name: 'some name',
    description: 'some description',
    type: 'FRAME',
    quantity: 3,
    price: '12.30'
  };

  const fillName = (wrapper, value) => {
    wrapper.find('#name').simulate('change', { target: { value } });
  };

  function fillDescription(wrapper, value) {
    wrapper.find('#description').simulate('change', { target: { value } });
  }

  function fillType(wrapper, value) {
    wrapper.find('#type select').simulate('change', { target: { value } });
  }

  function fillQuantity(wrapper, value) {
    wrapper.find('#quantity').simulate('change', { target: { value } });
  }

  function fillPrice(wrapper, value) {
    wrapper.find('#price').simulate('change', { target: { value } });
  }

  describe('success', () => {
    beforeEach(() => {
      ItemApi.createItem.mockReturnValue({});
    });

    it('should create item with entered values', async () => {
      // arrange
      const onSuccess = jest.fn();
      const wrapper = mount(<CreateItem onSuccess={onSuccess} />);

      fillName(wrapper, item.name);
      fillDescription(wrapper, item.description);
      fillType(wrapper, item.type);
      fillQuantity(wrapper, item.quantity);
      fillPrice(wrapper, item.price);

      // act
      wrapper.find('form').simulate('submit');
      await flushPromises();
      wrapper.update();

      // assert
      expect(ItemApi.createItem).toHaveBeenCalledWith(item);
    });

    it('should show a message indicating success', async () => {
      const wrapper = mount(<CreateItem />);
      fillName(wrapper, item.name);
      fillDescription(wrapper, item.description);
      fillType(wrapper, item.type);
      fillQuantity(wrapper, item.quantity);
      fillPrice(wrapper, item.price);

      wrapper.find('form').simulate('submit');
      await flushPromises();
      wrapper.update();

      expect(wrapper.find(AlertMessage).prop('isSuccessful')).toBe(true);
      expect(wrapper.find(AlertMessage).text()).toEqual('Created successfully');
    });

    it('should call onSuccess handler property if supplied to notify parent', async () => {
      const onSuccess = jest.fn();
      const wrapper = mount(<CreateItem onSuccess={onSuccess} />);
      fillName(wrapper, item.name);
      fillDescription(wrapper, item.description);
      fillType(wrapper, item.type);
      fillQuantity(wrapper, item.quantity);
      fillPrice(wrapper, item.price);

      wrapper.find('form').simulate('submit');
      await flushPromises();

      expect(onSuccess).toHaveBeenCalled();
    });

    it('should reset inputs to be ready to enter another item', async () => {
      const wrapper = mount(<CreateItem />);
      fillName(wrapper, item.name);
      fillDescription(wrapper, item.description);
      fillType(wrapper, item.type);
      fillQuantity(wrapper, item.quantity);
      fillPrice(wrapper, item.price);

      wrapper
        .find('#name')
        .simulate('change', { target: { value: item.name } });
      wrapper.find('form').simulate('submit');
      await flushPromises();
      wrapper.update();

      expect(wrapper.instance().state.name).toEqual('');
    });
  });

  it('should prevent the form from submitting when inputs are invalid', () => {
    const wrapper = mount(<CreateItem />);

    wrapper.find('form').simulate('submit');

    expect(ItemApi.createItem).not.toHaveBeenCalled();
    expect(
      wrapper
        .find('.input-error')
        .at(0)
        .text()
    ).toContain('required');
  });

  it('should show create failed message when api failed', async () => {
    ItemApi.createItem.mockRejectedValue({});
    const wrapper = mount(<CreateItem />);
    wrapper.find('#name').simulate('change', { target: { value: item.name } });
    fillName(wrapper, item.name);
    fillDescription(wrapper, item.description);
    fillType(wrapper, item.type);
    fillQuantity(wrapper, item.quantity);
    fillPrice(wrapper, item.price);

    wrapper.find('form').simulate('submit');

    await flushPromises();
    wrapper.update();

    expect(wrapper.find(AlertMessage).prop('isSuccessful')).toBe(false);
    expect(wrapper.find(AlertMessage).text()).toEqual('Create failed');
  });
});
