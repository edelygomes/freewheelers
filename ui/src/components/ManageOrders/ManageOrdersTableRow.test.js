import React from 'react';
import { mount } from 'enzyme/build';
import { MemoryRouter } from 'react-router-dom';
import ManageOrdersTableRow from './ManageOrdersTableRow';

describe('ManageOrdersTableRow', () => {
  let order;
  let wrapper;
  let onOrderChange;

  beforeEach(() => {
    order = {
      id: 1,
      date: 'Thu Dec 19 18:47:12 HKT 2019',
      item: {
        name: 'Hangers',
        price: 10.0,
        type: 'ACCESSORIES'
      },
      orderStatus: 'PAID',
      note: 'some note'
    };
    onOrderChange = jest.fn();

    wrapper = mount(
      <MemoryRouter>
        <table>
          <tbody>
            <ManageOrdersTableRow order={order} onOrderChange={onOrderChange} />
          </tbody>
        </table>
      </MemoryRouter>
    );
  });

  it('shows order info', () => {
    expect(wrapper.find('.manage-order-table__id').text()).toEqual('1');
    expect(wrapper.find('.manage-order-table__name').text()).toEqual('Hangers');
    expect(wrapper.find('.manage-order-table__type').text()).toEqual(
      'ACCESSORIES'
    );
    expect(wrapper.find('.manage-order-table__price').text()).toEqual('10');
    expect(wrapper.find('.manage-order-table__date').text()).toEqual(
      'Thu Dec 19 18:47:12 HKT 2019'
    );
    expect(
      wrapper.find('.manage-order-table__order-status').props().value
    ).toEqual('PAID');
    expect(wrapper.find('.manage-order-table__note').text()).toEqual(
      'some note'
    );
  });

  it('calls callback with order details to be updated', () => {
    const orderStatus = 'IN_PROGRESS';
    let note = 'updated note';

    wrapper
      .find('.manage-order-table__order-status')
      .simulate('change', { target: { value: orderStatus } });
    wrapper
      .find('.manage-order-table__note')
      .simulate('change', { target: { value: note } });

    wrapper.find('.manage-order-table__save-changes').simulate('click');

    const expectedOrder = { ...order, orderStatus, note };
    expect(onOrderChange).toHaveBeenCalledWith(expectedOrder);
  });
});
