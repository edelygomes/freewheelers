import { mount } from 'enzyme/build';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import OrderApi from '../../api/order';
import mockOrderResponse from '../../mock/orders.json';
import ManageOrdersTable from './ManageOrdersTable';
import ManageOrders from './ManageOrders';

jest.mock('../../api/order');

describe('ManageOrders', () => {
  const flushPromises = () => new Promise(setImmediate);

  beforeEach(() => {
    OrderApi.getAllCustomerOrders.mockResolvedValue(mockOrderResponse.orders);
  });

  it('passes orders to table', async () => {
    const wrapper = mount(
      <MemoryRouter>
        <ManageOrders />
      </MemoryRouter>
    );

    await flushPromises();
    wrapper.update();

    let actualOrders = wrapper.find(ManageOrdersTable).prop('orders');
    expect(actualOrders).toHaveLength(3);
  });

  it('calls update order when click order save changes button', async () => {
    const id = 1;
    const orderStatus = 'IN_PROGRESS';
    const note = 'new note';
    const updatedOrder = { id, orderStatus, note };

    const wrapper = mount(
      <MemoryRouter>
        <ManageOrders />
      </MemoryRouter>
    );

    await flushPromises();
    wrapper.update();

    wrapper.find(ManageOrdersTable).prop('onOrderChange')(updatedOrder)();

    expect(OrderApi.updateOrder).toHaveBeenCalledWith(id, {
      note,
      orderStatus
    });
  });

  it('should update and then reload orders when click order save changes button', async () => {
    const updatedOrder = {
      id: 1,
      orderStatus: 'IN_PROGRESS',
      note: 'new note'
    };

    OrderApi.updateOrder.mockResolvedValue({});

    const wrapper = mount(
      <MemoryRouter>
        <ManageOrders />
      </MemoryRouter>
    );

    await flushPromises();
    wrapper.update();

    wrapper.find(ManageOrdersTable).prop('onOrderChange')(updatedOrder)();

    await flushPromises();

    expect(OrderApi.updateOrder).toBeCalledTimes(1);
    expect(OrderApi.getAllCustomerOrders).toBeCalledTimes(2);
  });
});
