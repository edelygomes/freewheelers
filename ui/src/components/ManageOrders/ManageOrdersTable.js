import React from 'react';
import ManageOrdersTableRow from './ManageOrdersTableRow';

class ManageOrdersTable extends React.PureComponent {
  render() {
    return (
      <table className="manage-orders-table">
        <thead>
          <tr>
            <th>User</th>
            <th>Order Id</th>
            <th>Item name</th>
            <th>Item type</th>
            <th>Price</th>
            <th>Order placed</th>
            <th>Status</th>
            <th>Notes</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {this.props.orders.map(order => (
            <ManageOrdersTableRow
              key={order.id}
              order={order}
              onOrderChange={this.props.onOrderChange}
            />
          ))}
        </tbody>
      </table>
    );
  }
}

export default ManageOrdersTable;
