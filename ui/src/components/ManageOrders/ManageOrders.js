import React from 'react';
import AlertMessage from '../AlertMessage/AlertMessage';
import Section from '../Section/Section';
import OrderApi from '../../api/order';
import ManageOrdersTable from './ManageOrdersTable';

class ManageOrders extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      actionSuccess: false,
      actionMessage: ''
    };
  }

  onOrderChange = order => async () => {
    try {
      await OrderApi.updateOrder(order.id, {
        note: order.note,
        orderStatus: order.orderStatus
      });
      await this.getAllCustomerOrders();

      this.setState({
        actionSuccess: true,
        actionMessage: 'Saved successfully!'
      });
    } catch (error) {
      this.setState({
        actionSuccess: false,
        actionMessage: 'Save Failed'
      });
    }
  };

  getAllCustomerOrders = async () => {
    const orders = await OrderApi.getAllCustomerOrders();
    this.setState({ orders });
  };

  async componentDidMount() {
    await this.getAllCustomerOrders();
  }

  render() {
    return (
      <Section title="Customer orders">
        <AlertMessage
          isSuccessful={this.state.actionSuccess}
          message={this.state.actionMessage}
        />

        <ManageOrdersTable
          orders={this.state.orders}
          onOrderChange={this.onOrderChange}
        />
      </Section>
    );
  }
}

export default ManageOrders;
