import { MemoryRouter } from 'react-router-dom';
import React from 'react';
import { mount } from 'enzyme/build';
import mockOrderResponse from '../../mock/orders';
import ManageOrdersTableRow from './ManageOrdersTableRow';
import ManageOrdersTable from './ManageOrdersTable';

describe('ManageOrdersTable', () => {
  let wrapper;
  let onOrderChange;

  beforeEach(() => {
    onOrderChange = jest.fn();
    wrapper = mount(
      <MemoryRouter>
        <ManageOrdersTable
          orders={mockOrderResponse.orders}
          onOrderChange={onOrderChange}
        />
      </MemoryRouter>
    );
  });

  it('displays all orders', () => {
    expect(wrapper.find(ManageOrdersTableRow)).toHaveLength(3);
  });

  it('passes save changes callback to rows', () => {
    let actualCallback = wrapper
      .find(ManageOrdersTableRow)
      .first()
      .prop('onOrderChange');
    expect(actualCallback).toEqual(onOrderChange);
  });
});
