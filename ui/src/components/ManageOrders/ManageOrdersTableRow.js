import React from 'react';
import { Link } from 'react-router-dom';

class ManageOrdersTableRow extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      order: props.order
    };
  }

  handleChangeFor = fieldName => event => {
    const order = {
      ...this.state.order,
      [fieldName]: event.target.value
    };
    this.setState({ order });
  };

  render() {
    const { order } = this.state;

    return (
      <tr className="manage-order">
        <td>
          <Link to={`/profile/${order.emailAddress}`}>
            {order.emailAddress}
          </Link>
        </td>
        <td className="manage-order-table__id">{order.id}</td>
        <td className="manage-order-table__name">{order.item?.name}</td>
        <td className="manage-order-table__type">{order.item?.type}</td>
        <td className="manage-order-table__price">{order.item?.price}</td>
        <td className="manage-order-table__date">{order.date}</td>
        <td>
          <select
            className="manage-order-table__order-status"
            value={order.orderStatus}
            onChange={this.handleChangeFor('orderStatus')}
            onBlur={this.handleChangeFor('orderStatus')}
          >
            <option value="NEW">New</option>
            <option value="IN_PROGRESS">In progress</option>
            <option value="READY_FOR_SHIPMENT">Ready for shipment</option>
            <option value="PAID">Paid</option>
          </select>
        </td>
        <td>
          <textarea
            className="manage-order-table__note"
            onChange={this.handleChangeFor('note')}
            onBlur={this.handleChangeFor('note')}
            cols="15"
            rows="3"
            value={order.note}
          />
        </td>
        <td>
          <button
            onClick={this.props.onOrderChange(this.state.order)}
            className="button button--primary manage-order-table__save-changes"
          >
            Save Changes
          </button>
        </td>
      </tr>
    );
  }
}

export default ManageOrdersTableRow;
