import React from 'react';
import formatClassNames from '../../utils/formatClassNames';
import './Section.css';

function Section(props) {
  return (
    <section className={formatClassNames('section', props.className)}>
      <h2 className="section-title">{props.title}</h2>
      {props.children}
    </section>
  );
}

export default Section;
