import React from 'react';
import { mount } from 'enzyme';
import Item from '../Item/Item';
import Card from '../Card/Card';
import FeatureTogglesContext from '../../featureTogglesContext';
import Order from './Order';

describe('<Order />', () => {
  const order = {
    id: 46,
    item: {
      name: 'This Item',
      description: 'This Item description',
      price: '12.30'
    },
    date: 'Some order date',
    emailAddress: 'me@example.com'
  };

  it('should render an order', () => {
    const wrapper = mount(
      <FeatureTogglesContext.Provider value={{ VAT: false }}>
        <Order {...order} />
      </FeatureTogglesContext.Provider>
    );

    expect(wrapper.find('.order__id').text()).toEqual('Order #46');
    expect(wrapper.find('.order__date').text()).toEqual(
      'Order placed: Some order date'
    );
    expect(wrapper.find(Item)).toHaveLength(1);
    expect(wrapper.find(Card)).toHaveLength(1);
  });
});
