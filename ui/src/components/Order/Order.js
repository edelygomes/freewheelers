import React from 'react';
import './Order.css';
import Item from '../Item/Item';
import Card from '../Card/Card';

function Order(props) {
  return (
    <Card className="order">
      <div className="order__metadata">
        <h3 className="order__id">Order #{props.id}</h3>
        <div>
          <div className="order__date">Order placed: {props.date}</div>
          <div className="order__status">Status: {props.orderStatus}</div>
        </div>
      </div>
      <Item {...props.item} />
    </Card>
  );
}

export default Order;
