import React from 'react';
import itemValidator from '../CreateItem/Item.validator';

class EditItemsTableRow extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errors: {}
    };
  }

  handleChangeFor = fieldName => event => {
    const item = {
      ...this.props.item,
      [fieldName]: event.target.value
    };
    this.props.onItemChange(item);
  };

  handleBlurFor = fieldName => event => {
    const validate = itemValidator.fields[fieldName];
    const errors = {
      ...this.state.errors,
      [fieldName]: validate(event.target.value)
    };
    this.setState({ errors });
  };

  handleItemSelect = event => {
    this.props.onItemSelect(this.props.item.id, event.target.checked);
  };

  render() {
    const { item, selected } = this.props;
    const { errors } = this.state;
    return (
      <tr>
        <td>
          <input
            className="update-items__item-checkbox"
            checked={selected}
            type="checkbox"
            onChange={this.handleItemSelect}
          />
        </td>
        <td>
          <input
            className="update-items__name-input"
            type="text"
            value={item.name}
            onChange={this.handleChangeFor('name')}
            onBlur={this.handleBlurFor('name')}
          />
          <span className="input-error">{errors.name}</span>
        </td>
        <td>
          <input
            className="update-items__price-input"
            value={item.price}
            onChange={this.handleChangeFor('price')}
            onBlur={this.handleBlurFor('price')}
          />
          <span className="input-error">{errors.price}</span>
        </td>
        <td>
          <input
            className="update-items__description-input"
            type="text"
            value={item.description}
            onChange={this.handleChangeFor('description')}
            onBlur={this.handleBlurFor('description')}
          />
          <span className="input-error">{errors.description}</span>
        </td>
        <td>
          <select
            className="update-items__type-select"
            value={item.type}
            onChange={this.handleChangeFor('type')}
            onBlur={this.handleBlurFor('type')}
          >
            <option>FRAME</option>
            <option>ACCESSORIES</option>
          </select>
        </td>
        <td>
          <input
            className="update-items__quantity-input"
            value={item.quantity}
            onChange={this.handleChangeFor('quantity')}
            onBlur={this.handleBlurFor('quantity')}
          />
          <span className="input-error">{errors.quantity}</span>
        </td>
      </tr>
    );
  }
}

export default EditItemsTableRow;
