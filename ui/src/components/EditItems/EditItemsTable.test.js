import { mount } from 'enzyme';
import React from 'react';
import EditItemsTable from './EditItemsTable';
import EditItemsTableRow from './EditItemsTableRow';

describe('EditItemsTable', () => {
  let item1;
  let item2;
  let wrapper;
  let onToggleSelectAll;
  let onItemSelect;
  let onItemChange;

  beforeEach(() => {
    item1 = {
      id: 1,
      name: 'some name',
      description: 'some item description',
      price: 1,
      quantity: 1,
      type: 'FRAME'
    };
    item2 = {
      id: 2,
      name: 'some name2',
      description: 'some item description2',
      price: 1,
      quantity: 1,
      type: 'FRAME'
    };

    onToggleSelectAll = jest.fn();
    onItemSelect = jest.fn();
    onItemChange = jest.fn();

    wrapper = mount(
      <EditItemsTable
        items={[item1, item2]}
        selectedItemIds={[item1.id]}
        onToggleSelectAll={onToggleSelectAll}
        onItemSelect={onItemSelect}
        onItemChange={onItemChange}
      />
    );
  });

  it('passes item to rows', async () => {
    expect(
      wrapper
        .find(EditItemsTableRow)
        .at(0)
        .prop('item')
    ).toEqual(item1);
    expect(
      wrapper
        .find(EditItemsTableRow)
        .at(1)
        .prop('item')
    ).toEqual(item2);
  });

  it('selects row if item in list of selected', async () => {
    expect(
      wrapper
        .find(EditItemsTableRow)
        .at(0)
        .prop('selected')
    ).toEqual(true);
    expect(
      wrapper
        .find(EditItemsTableRow)
        .at(1)
        .prop('selected')
    ).toEqual(false);
  });

  it('passes item selection callback to row', () => {
    expect(
      wrapper
        .find(EditItemsTableRow)
        .first()
        .prop('onItemSelect')
    ).toEqual(onItemSelect);
  });

  it('passes item change callback to row', () => {
    expect(
      wrapper
        .find(EditItemsTableRow)
        .first()
        .prop('onItemChange')
    ).toEqual(onItemChange);
  });

  describe('select all checkbox', () => {
    it('calls callback when toggles select all', () => {
      const checked = true;
      wrapper
        .find('.update-items__select-all-checkbox')
        .simulate('change', { target: { checked } });

      expect(onToggleSelectAll).toHaveBeenCalledWith(checked);
    });

    it('marks all selected if all items in selected list', () => {
      wrapper = mount(
        <EditItemsTable
          items={[item1, item2]}
          selectedItemIds={[item1.id, item2.id]}
          onToggleSelectAll={onToggleSelectAll}
        />
      );

      expect(
        wrapper.find('.update-items__select-all-checkbox').prop('checked')
      ).toEqual(true);
    });
  });
});
