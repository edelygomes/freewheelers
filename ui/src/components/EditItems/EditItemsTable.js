import React from 'react';
import EditItemsTableRow from './EditItemsTableRow';

class EditItemsTable extends React.PureComponent {
  render() {
    return (
      <table>
        <thead>
          <tr>
            <td>
              <input
                className="update-items__select-all-checkbox"
                type="checkbox"
                checked={this.isAllItemsSelected()}
                onChange={this.onToggleSelectAll}
              />
            </td>
            <td>Name</td>
            <td>Price</td>
            <td>Description</td>
            <td>Type</td>
            <td>Quantity</td>
          </tr>
        </thead>
        <tbody>
          {this.props.items?.map(item => (
            <EditItemsTableRow
              key={item.id}
              item={item}
              selected={this.isItemSelected(item)}
              onItemSelect={this.props.onItemSelect}
              onItemChange={this.props.onItemChange}
            />
          ))}
        </tbody>
      </table>
    );
  }

  onToggleSelectAll = event => {
    this.props.onToggleSelectAll(event.target.checked);
  };

  isItemSelected = item => {
    return this.props.selectedItemIds.includes(item.id);
  };

  isAllItemsSelected = () => {
    return this.props.selectedItemIds.length === this.props.items.length;
  };
}

export default EditItemsTable;
