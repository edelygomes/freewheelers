import React from 'react';
import ItemApi from '../../api/item';
import './EditItems.css';
import itemValidator from '../../components/CreateItem/Item.validator';
import AlertMessage from '../AlertMessage/AlertMessage';
import EditItemsTable from './EditItemsTable';

class EditItems extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedItemIds: [],
      editSuccess: false,
      editItemsResultMessage: ''
    };
  }

  isAnySelectedItemInvalid = () => {
    return !this.props.items
      .filter(item => this.state.selectedItemIds.includes(item.id))
      .every(item => itemValidator.isValid(item));
  };

  getAllSelectedItems = () => {
    return this.props.items.filter(item =>
      this.state.selectedItemIds.includes(item.id)
    );
  };

  onItemChange = updatedItem => {
    const items = this.props.items.map(item =>
      item.id === updatedItem.id ? updatedItem : item
    );
    this.props.onItemsChange(items);
  };

  onItemSelect = (itemId, checked) => {
    const selectedItemIds = [...this.state.selectedItemIds];
    if (checked) {
      selectedItemIds.push(itemId);
      this.setState({
        selectedItemIds
      });
    } else {
      this.setState({
        selectedItemIds: selectedItemIds.filter(
          selectedItemId => selectedItemId !== itemId
        )
      });
    }
  };

  onToggleSelectAll = checked => {
    if (checked) {
      this.setState({
        selectedItemIds: this.props.items.map(item => item.id)
      });
    } else {
      this.setState({
        selectedItemIds: []
      });
    }
  };

  handleDeleteItemsClick = async () => {
    if (this.state.selectedItemIds.length <= 0) {
      return;
    }

    try {
      await ItemApi.deleteItems({ itemIds: this.state.selectedItemIds });
      this.props.onSuccess && (await this.props.onSuccess());

      this.setState({
        editSuccess: true,
        editResultMessage: 'Deleted successfully',
        selectedItemIds: []
      });
    } catch (error) {
      this.setState({
        editSuccess: false,
        editResultMessage: 'Delete failed'
      });
    }
  };

  handleUpdateItemsClick = async () => {
    if (this.state.selectedItemIds.length <= 0) {
      return;
    }

    if (this.isAnySelectedItemInvalid()) {
      return;
    }

    try {
      await ItemApi.updateItem({ items: this.getAllSelectedItems() });
      this.props.onSuccess && (await this.props.onSuccess());

      this.setState({
        editSuccess: true,
        editResultMessage: 'Updated successfully',
        selectedItemIds: []
      });
    } catch (error) {
      this.setState({
        editSuccess: false,
        editResultMessage: 'Update failed'
      });
    }
  };
  render() {
    return (
      <div className="update-items">
        <section className="update-items__current-items">
          <div className="update-items__current-items-title">Items</div>
          <EditItemsTable
            items={this.props.items}
            selectedItemIds={this.state.selectedItemIds}
            onItemSelect={this.onItemSelect}
            onItemChange={this.onItemChange}
            onToggleSelectAll={this.onToggleSelectAll}
          />
        </section>
        <button
          onClick={this.handleUpdateItemsClick}
          className="button button--primary update-items__update-all"
        >
          Update all selected items
        </button>
        <button
          onClick={this.handleDeleteItemsClick}
          className="button button--primary update-items__delete-all"
        >
          Delete all selected items
        </button>

        <AlertMessage
          isSuccessful={this.state.editSuccess}
          message={this.state.editResultMessage}
          inline={true}
        />
      </div>
    );
  }
}

export default EditItems;
