import React from 'react';
import { mount } from 'enzyme';
import EditItemsTableRow from './EditItemsTableRow';

describe('EditItemsTableRow', () => {
  let wrapper;
  let item;
  let onItemSelect;
  let onItemChange;

  beforeEach(() => {
    item = {
      id: 1,
      name: 'some name',
      description: 'some item description',
      price: 5,
      quantity: 1,
      type: 'FRAME'
    };

    onItemSelect = jest.fn(() => jest.fn());
    onItemChange = jest.fn();
    wrapper = mount(
      <table>
        <tbody>
          <EditItemsTableRow
            item={item}
            onItemSelect={onItemSelect}
            onItemChange={onItemChange}
            selected={true}
          />
        </tbody>
      </table>
    );
  });

  it('shows item detail', () => {
    expect(wrapper.find('.update-items__name-input').prop('value')).toEqual(
      'some name'
    );
    expect(wrapper.find('.update-items__price-input').prop('value')).toEqual(5);
    expect(
      wrapper.find('.update-items__description-input').prop('value')
    ).toEqual('some item description');
    expect(wrapper.find('.update-items__type-select').prop('value')).toEqual(
      'FRAME'
    );
    expect(wrapper.find('.update-items__quantity-input').prop('value')).toEqual(
      1
    );
  });

  it('shows item as selected', () => {
    expect(
      wrapper.find('.update-items__item-checkbox').prop('checked')
    ).toEqual(true);
  });

  it('calls callback with item details to be updated', () => {
    const name = 'updated name';
    const price = 10;
    const description = 'updated description';
    const type = 'ACCESSORIES';
    const quantity = 2;

    wrapper
      .find('.update-items__name-input')
      .simulate('change', { target: { value: name } });
    expect(onItemChange).toHaveBeenCalledWith({ ...item, name });

    wrapper
      .find('.update-items__price-input')
      .simulate('change', { target: { value: price } });
    expect(onItemChange).toHaveBeenCalledWith({ ...item, price });

    wrapper
      .find('.update-items__description-input')
      .simulate('change', { target: { value: description } });
    expect(onItemChange).toHaveBeenCalledWith({ ...item, description });

    wrapper
      .find('.update-items__type-select')
      .simulate('change', { target: { value: type } });
    expect(onItemChange).toHaveBeenCalledWith({ ...item, type });

    wrapper
      .find('.update-items__quantity-input')
      .simulate('change', { target: { value: quantity } });
    expect(onItemChange).toHaveBeenCalledWith({ ...item, quantity });
  });

  it('calls callback when item is selected', () => {
    wrapper
      .find('.update-items__item-checkbox')
      .simulate('change', { target: { checked: true } });

    expect(onItemSelect).toHaveBeenCalledWith(item.id, true);
  });
});
