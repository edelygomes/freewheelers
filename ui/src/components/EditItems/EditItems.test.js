import React from 'react';
import { mount } from 'enzyme';
import ItemApi from '../../api/item';
import validator from '../../components/CreateItem/Item.validator';
import EditItems from './EditItems';
import EditItemsTable from './EditItemsTable';

jest.mock('../../components/CreateItem/CreateItem');
jest.mock('../../api/item');
jest.mock('../../components/CreateItem/Item.validator');

const items = [
  {
    id: 1,
    name: 'some name',
    description: 'some item description',
    price: 1,
    quantity: 1,
    type: 'FRAME'
  },
  {
    id: 2,
    name: 'some name2',
    description: 'some item description2',
    price: 1,
    quantity: 1,
    type: 'FRAME'
  }
];

describe('EditItems', () => {
  const flushPromises = () => new Promise(setImmediate);

  describe('render', () => {
    it('shows item table', () => {
      const wrapper = mount(<EditItems items={items} />);

      let editItemsTable = wrapper.find(EditItemsTable);
      expect(editItemsTable).toHaveLength(1);
      expect(editItemsTable.prop('items')).toEqual(items);
    });
  });

  describe('update items', () => {
    it('not update items when no checkbox is checked', () => {
      validator.isValid.mockReturnValue(true);

      const wrapper = mount(
        <EditItems items={items} onItemsChange={jest.fn()} />
      );

      wrapper.find(EditItemsTable).prop('onItemChange')(items[0]);

      expect(ItemApi.updateItem).toHaveBeenCalledTimes(0);
    });

    it('not update items when update item value is invalid', async () => {
      ItemApi.updateItem.mockResolvedValue({});
      validator.isValid.mockReturnValue(false);

      const wrapper = mount(<EditItems items={items} />);
      await flushPromises();

      wrapper.find(EditItemsTable).prop('onItemSelect')(items[0].id, true);

      wrapper.find('.update-items__update-all').simulate('click');
      await flushPromises();

      expect(ItemApi.updateItem).toHaveBeenCalledTimes(0);
    });

    it('should update all selected items then reload items when any checkbox is checked', async () => {
      ItemApi.updateItem.mockResolvedValue({});
      validator.isValid.mockReturnValue(true);
      const onSuccess = jest.fn();
      const wrapper = mount(<EditItems items={items} onSuccess={onSuccess} />);
      await flushPromises();

      wrapper.find(EditItemsTable).prop('onItemSelect')(items[0].id, true);

      wrapper.find('.update-items__update-all').simulate('click');
      await flushPromises();

      expect(ItemApi.updateItem).toHaveBeenCalledTimes(1);
      expect(onSuccess).toHaveBeenCalled();
    });
  });

  describe('delete items', () => {
    it('not update items when no checkbox is checked', () => {
      const wrapper = mount(<EditItems items={items} />);

      wrapper.find('.update-items__delete-all').simulate('click');

      expect(ItemApi.deleteItems).toHaveBeenCalledTimes(0);
    });

    it('should delete all selected items then reload items when any checkbox is checked', async () => {
      ItemApi.deleteItems.mockResolvedValue({});
      const onSuccess = jest.fn();

      const wrapper = mount(<EditItems items={items} onSuccess={onSuccess} />);
      await flushPromises();

      wrapper.find(EditItemsTable).prop('onItemSelect')(items[0].id, true);

      wrapper.find('.update-items__delete-all').simulate('click');
      await flushPromises();

      expect(ItemApi.deleteItems).toHaveBeenCalledWith({
        itemIds: [items[0].id]
      });
      expect(onSuccess).toHaveBeenCalled();
    });
  });
});
