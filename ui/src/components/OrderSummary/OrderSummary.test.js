import React from 'react';
import { mount } from 'enzyme';
import OrderSummary from './OrderSummary';

jest.mock('../../api/order');
jest.mock('../../history');

describe('<OrderSummary />', () => {
  const props = {
    price: 89.99
  };

  it('should render an OrderSummary', () => {
    const wrapper = mount(<OrderSummary {...props} />);

    expect(wrapper.find('.order-summary-container').exists()).toBe(true);
    expect(wrapper.find('.price').text()).toEqual(
      'Sub-Total (excl of VAT)£ 89.99'
    );
    expect(wrapper.find('.vat-detail').text()).toEqual('VAT (20.00%)£ 22.50');
    expect(wrapper.find('.total-price').text()).toEqual('Total£ 112.49');
  });

  describe('<OrderSummary />', () => {
    const props = {
      price: 1124.99
    };

    it('should render an OrderSummary', () => {
      const wrapper = mount(<OrderSummary {...props} />);

      expect(wrapper.find('.order-summary-container').exists()).toBe(true);
      expect(wrapper.find('.price').text()).toEqual(
        'Sub-Total (excl of VAT)£ 1124.99'
      );
      expect(wrapper.find('.vat-detail').text()).toEqual(
        'VAT (20.00%)£ 281.25'
      );
      expect(wrapper.find('.total-price').text()).toEqual('Total£ 1406.24');
    });
  });
});
