import React from 'react';
import './OrderSummary.css';
import { getFinalPrice, getVATAmount } from '../../utils/pricing';
class OrderSummary extends React.PureComponent {
  render() {
    return (
      <div className="order-summary-container">
        <h2>Summary</h2>
        <div className="order-summary-details price">
          <h4>Sub-Total (excl of VAT)</h4>
          <p>£ {this.props.price}</p>
        </div>
        <div className="order-summary-details">
          <h4>Delivery charge</h4>
          <p>£ 0.00</p>
        </div>
        <div className="order-summary-details">
          <h3>Total before Vat</h3>
          <p>£ {this.props.price}</p>
        </div>
        <hr />
        <div className="order-summary-details vat-detail">
          <h4>VAT (20.00%)</h4>
          <p>£ {getVATAmount(this.props.price, { UK: 0.2 })}</p>
        </div>
        <hr />
        <div className="order-summary-details total-price">
          <h3>Total</h3>
          <p>£ {getFinalPrice(this.props.price, { UK: 0.2 })}</p>
        </div>
      </div>
    );
  }
}

export default OrderSummary;
