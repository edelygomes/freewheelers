import React from 'react';
import { mount } from 'enzyme/build';
import Cookies from 'universal-cookie';
import FeatureTogglesContext from '../../featureTogglesContext';
import CookieModal from './CookieModal';

describe('<CookieModal />', () => {
  let wrapper;
  const cookies = new Cookies();

  beforeEach(() => {
    const featureToggles = { eu_cookie: true };
    cookies.set('FreeWheelersCookie', 'false');
    wrapper = mount(
      <FeatureTogglesContext.Provider value={featureToggles}>
        <CookieModal message="A simple cookie model here" />
      </FeatureTogglesContext.Provider>
    );
  });

  it('should render a cookie modal', () => {
    expect(wrapper.find('.modal-content #modal-message').text()).toEqual(
      'A simple cookie model here'
    );
    expect(cookies.get('FreeWheelersCookie')).toBe('false');
  });

  it('should hide when on close and create a cookie', () => {
    wrapper.find('.close').simulate('click');
    expect(wrapper.find('.modal-content').exists()).toBe(false);
    expect(cookies.get('FreeWheelersCookie')).toBe('true');
  });

  it('cookie should expire after time is over', () => {
    cookies.set('FreeWheelersCookie', 'false', {
      path: '/',
      expires: new Date()
    });
    setTimeout(expect(cookies.get('FreeWheelersCookie')).toBe(undefined), 500);
  });
});
