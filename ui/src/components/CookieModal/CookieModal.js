import React from 'react';
import './CookieModal.css';
import Cookies from 'universal-cookie';
import FeatureTogglesContext from '../../featureTogglesContext';

import ico_cookie from '../../assets/icons/ico_cookie.png';
import ico_close from '../../assets/icons/ico_close.png';

const cookies = new Cookies();

const Modal = ({ onClick, onKeyDown, message }) => (
  <div className="modal-content">
    <span
      className="close"
      onClick={onClick}
      onKeyDown={onKeyDown}
      role="button"
      tabIndex={0}
    >
      <img src={ico_close} alt="Close button" />
    </span>
    <div>
      <div style={{ float: 'left' }}>
        <img src={ico_cookie} alt="Cookie Icon" className="img-icon" />
      </div>
      <span id="modal-message">{message}</span>
    </div>
  </div>
);

class CookieModal extends React.PureComponent {
  defaultState = {
    message: '',
    modalIsShowing: false
  };

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultState,
      modalIsShowing: this.showModal()
    };
    if (!this.cookieExists()) {
      this.initalizeCookie();
    }
  }

  cookieExists = () => {
    return cookies.get('FreeWheelersCookie') !== undefined;
  };

  showModal = () => {
    if (!this.cookieExists()) {
      return true;
    } else {
      return this.cookieExists && cookies.get('FreeWheelersCookie') === 'false';
    }
  };

  handleChangeFor = () => {
    let expirationDateFromCookie = new Date(
      cookies.get('FistAccesDateFreeWheelers')
    );

    cookies.set('FreeWheelersCookie', 'true', {
      path: '/',
      expires: expirationDateFromCookie
    });

    this.setState({ modalIsShowing: false });

    cookies.remove('FistAccesDateFreeWheelers');
  };

  initalizeCookie = () => {
    let expirationDate = this.getExpirationDate(3);

    cookies.set('FistAccesDateFreeWheelers', expirationDate.toString());

    cookies.set('FreeWheelersCookie', 'false', {
      path: '/',
      expires: expirationDate
    });
  };

  getExpirationDate = cookieDurationInMonths => {
    let currentDate = new Date();
    return new Date(
      currentDate.setMonth(currentDate.getMonth() + cookieDurationInMonths)
    );
  };

  render() {
    return (
      <FeatureTogglesContext.Consumer>
        {toggles => {
          if (toggles['eu_cookie']) {
            return (
              <React.Fragment>
                {this.state.modalIsShowing && (
                  <div className="cookie-modal">
                    <Modal
                      onClick={this.handleChangeFor}
                      onKeyDown={this.handleChangeFor}
                      message={this.props.message}
                    />
                  </div>
                )}
              </React.Fragment>
            );
          }
        }}
      </FeatureTogglesContext.Consumer>
    );
  }
}

export default CookieModal;
