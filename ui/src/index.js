import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import './normalize.css';
import './index.css';
import App from './pages/App';
import './api/axiosConfig';
import history from './history';
import LoginApi from './api/login';
import FeatureTogglesApi from './api/featureToggles';
import FeatureTogglesContext from './featureTogglesContext';

Promise.all([
  FeatureTogglesApi.getAllFeatureToggles(),
  LoginApi.principal()
]).then(([toggles]) => {
  ReactDOM.render(
    <FeatureTogglesContext.Provider value={toggles}>
      <Router history={history}>
        <App />
      </Router>
    </FeatureTogglesContext.Provider>,
    document.getElementById('root')
  );
});
