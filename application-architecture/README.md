# Application Architecture

This directory contains the definition of a set of views of the FreeWheelers application architecture using the C4 
approach.  (See https://c4model.com) 

## Generate Diagrams
Pre-requisites: PlantUML is used to generate the diagrams from code.

To install plantuml:

`brew install plantuml`

To generate the diagrams (into /dist folder) run the following from this project directory:

`plantuml -o ../dist ./src/*.puml` 