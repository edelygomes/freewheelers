# FreeWheelers Test Strategy

## Purpose
The purpose of this document is to have a shared understanding of the overall approach, tools, targets and timing of test activities. 

## Audience
The whole team.

## Current Risks

    - No strategy for manual tests
    - No strategy for security tests
    - Go to production without enough tests

## QA activities for stories
Owner: everyone from team with the QA hat.

- Check stories “In Analysis” for completeness and testability
- Story kick-off (“Ready for Dev")
- Design test cases (“In Dev”)
- Desk check before stories go to “Ready for QA”
- Test stories (“In QA”)
- Deploy on Showcase environment and work with team to present story to PO (“Ready for Showcase”) 
- Deploy to staging environment and check in the staging environment.
- Deploy to production and check in production environment (“In Production”)
- Plan Bug bash based on team context and facilitate it and collect issues & feedback.

#### Entry Criteria (In Dev -> Ready for QA)
Testing will only commence once all of the following criteria are met

    1. Story implementation is complete
    2. Automated tests passed (unit, integration, functional)
    3. Successful desk check
    4. No outstanding high priority defects

#### Exit Criteria (In QA -> Ready for Showcase)
Testing is considered done once all of the following criteria have been met

    1. Test execution is complete
    2. Main functionality passed for supported browsers and platforms
    3. No outstanding high priority defects
    4. All outstanding minor and medium defects have been recorded

## Type of Tests

| Type | Tools |
| ------ | ------ |
| Unit (java), Unit (JS), Integration | Junit, Mockito, Spring-test, Karma.js, Jacoco report |
| Functional | Taiko |
| Manual exploratory | _TBD_ |

## Defect Management

| Priority | Severity | Frequency | Functional Impact | Workaround |
| ------ | ------ | ------ | ------ | ------ |
| High | Showstopper | < 1 hourly | Severe impact on the primary functionality. Main functionality e.g. network routing or remote management is not working | None available |
| Medium | Major | Daily | Main functionality affected intermittently | Workaround available but difficult or unreliable |
| Low | Minor | Infrequent | Minor impact on the functionality | Good workaround available |

## Story Life Cycle

![](story_lifecycle.png)


## Defect Life Cycle

![](defect_lifecycle.png)