#!/bin/bash

# Fail the script if an error occurs
set -e

USER=$1
HOST=$2
ENV=$3

echo "begin to deploy package to environment: ${ENV}"

if [ $# -ne 3 ]; then
  echo usage: scripts/install.sh ${USER} ${HOST} ${ENV}
  exit -1
fi

if [ ! -e "dist/freewheelers.zip" ]; then
  echo "cannot find dist/freewheelers.zip to deploy"
  exit -1
fi

scp dist/freewheelers.zip ${USER}@${HOST}:/tmp

echo "
  set -e

  sudo su appuser

  # visibly print to stderr
  function yell () {
   echo -e '\n==================================== \n' >&2
   echo \$@ >&2
   echo -e '\n==================================== \n' >&2
  }


  INSTALL_DIRECTORY=/home/appuser/freewheelers
  #Remove old directories
  cd \$INSTALL_DIRECTORY
  find . -type d -name "20*" -exec rm -r {} +

  #Create new app directory and move app
  TIMESTAMP=\$(date +'%Y-%m-%d-%HH%MM%Ss')
  APP_DIRECTORY=\$INSTALL_DIRECTORY/\$TIMESTAMP

  mkdir -p \$APP_DIRECTORY
  cd \$APP_DIRECTORY
  sudo chown appuser:user /tmp/freewheelers.zip
  mv /tmp/freewheelers.zip \$APP_DIRECTORY

  echo 'Unzipping freewheelers.zip...'
  unzip -q freewheelers.zip

  ln -nfs \$APP_DIRECTORY \$INSTALL_DIRECTORY/current_version

  #Stop app service
  echo 'Stop freewheelers service...'
  sudo service freewheelers stop || echo Service was already stopped

  #DB migrations
  function mybatis_migration () { sh db/migrations/mybatis/bin/migrate --path=./db/migrations \$@; }

  mybatis_migration up

  # Fail build if migrations are pending.
  if [[ -n \$( mybatis_migration status | grep '..pending..' ) ]]; then
    yell 'Pending migrations. Check your migration timestamps'
    mybatis_migration status
    exit 1
  fi

  #Start app service
  echo 'Start freewheelers service...'
  sudo bash -c 'echo \"export ENV=$ENV\" > /etc/default/freewheelers\'
  sudo cp scripts/freewheelers.init /etc/init.d/freewheelers
  sudo chmod 0755 /etc/init.d/freewheelers
  sudo chown root:root /etc/init.d/freewheelers
  sudo service freewheelers start
  sudo systemctl enable freewheelers

" | ssh ${USER}@${HOST} /bin/bash
