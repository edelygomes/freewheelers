#!/bin/bash

load_environment() {
  [ -f /etc/default/freewheelers ] && . /etc/default/freewheelers
}

PATH_TO_FREEWHEELERS=/home/appuser/freewheelers/current_version

load_environment
printf "\n" | java -jar $PATH_TO_FREEWHEELERS/api.jar &
