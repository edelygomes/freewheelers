package com.freewheelers.client;

import com.freewheelers.client.dto.CreditCardPaymentAPIRequest;
import com.freewheelers.client.dto.CreditCardPaymentAPIResponse;
import com.freewheelers.model.CreditCardPaymentDetails;
import com.freewheelers.model.builder.CreditCardPaymentDetailsBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class CreditCardPaymentAPIClientTest {
    private final String url = "https://any-url.com";
    
    @Mock
    private RestTemplate restTemplate;
    
    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void shouldCallPaymentAPIWithTheCorrectParameters() {
        CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().build();
        CreditCardPaymentAPIClient creditCardPaymentAPIClient = new CreditCardPaymentAPIClient(restTemplate, url);
        CreditCardPaymentAPIRequest creditCardPaymentAPIRequest = new CreditCardPaymentAPIRequest(creditCardPaymentDetails, new BigDecimal(0));

        creditCardPaymentAPIClient.pay(creditCardPaymentAPIRequest);

        verify(restTemplate).postForObject(url + "/authorise", creditCardPaymentAPIRequest, CreditCardPaymentAPIResponse.class);
    }
}