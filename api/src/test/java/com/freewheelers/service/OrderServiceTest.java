package com.freewheelers.service;

import com.freewheelers.mapper.OrderMapper;
import com.freewheelers.model.*;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.model.builder.ItemBuilder;
import com.freewheelers.model.builder.OrderBuilder;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class OrderServiceTest {

    @Mock
    private OrderMapper orderMapperMock;

    @Mock
    private AccountService accountServiceMock;

    @Mock
    private ItemService itemServiceMock;

    @Mock
    private CalculationPricesService calculationPricesServiceMock;

    private OrderService orderService;

    @BeforeEach
    void setUp() {
        initMocks(this);

        orderService = new OrderService(orderMapperMock, itemServiceMock, accountServiceMock, calculationPricesServiceMock);
    }

    @Test
    void shouldInsertReservedOrderWhenSavingNewOrder() {
        Order newOrder = new OrderBuilder().withOrderId(null).build();

        orderService.save(newOrder);

        verify(orderMapperMock).insert(newOrder);
    }

    @Test
    void shouldSaveReservedOrderWhenUpdatingExistingOrder() {
        Order existingOrder = new OrderBuilder().withOrderId(24).build();

        orderService.save(existingOrder);

        verify(orderMapperMock).save(existingOrder);
    }

    @Test
    void shouldSaveOrderWithUpdatedStatusAndNoteWhenUpdatingOrderDetails() throws OrderNotFoundException {
        int orderId = 12;
        OrderStatus updatedStatus = OrderStatus.PAID;
        String updatedNote = "updated note value";
        when(orderMapperMock.findById(orderId)).thenReturn(Optional.of(new OrderBuilder().build()));

        orderService.updateDetails(orderId, updatedStatus, updatedNote);

        ArgumentCaptor<Order> argumentCaptor = ArgumentCaptor.forClass(Order.class);
        verify(orderMapperMock).save(argumentCaptor.capture());

        Order updatedOrder = argumentCaptor.getValue();
        assertThat(updatedOrder.getNote(), is(updatedNote));
        assertThat(updatedOrder.getStatus(), is(updatedStatus));
    }

    @Test
    void shouldCreateOrderForUser() throws ItemNotFoundException, AccountNotFoundException {
        int itemId = 3;
        BigDecimal price = BigDecimal.valueOf(100.99);
        String userEmail = "loggedInUser@example.org";
        PaymentMethod paymentMethod = PaymentMethod.CASH;

        Item actualItem = new ItemBuilder().withItemId(itemId).withPrice(price).build();
        when(itemServiceMock.findById(itemId)).thenReturn(Optional.of(actualItem));

        Account userAccount = new AccountBuilder().withEmailAddress(userEmail).build();
        when(accountServiceMock.findByEmailAddress(userEmail)).thenReturn(Optional.of(userAccount));

        when(calculationPricesServiceMock.calculateFinalPrice(actualItem)).thenReturn(BigDecimal.valueOf(126.24));

        orderService.createForUser(itemId, userEmail, paymentMethod);

        ArgumentCaptor<Order> captor = ArgumentCaptor.forClass(Order.class);
        verify(orderMapperMock).insert(captor.capture());
        Order createdOrder = captor.getValue();

        assertThat(createdOrder.getAccount(), is(userAccount));
        assertThat(createdOrder.getItem().getItemId(), is(actualItem.getId()));
        assertThat(createdOrder.getStatus(), is(OrderStatus.NEW));
        assertThat(createdOrder.getTotalPrice(), is(BigDecimal.valueOf(126.24)));
        assertThat(createdOrder.getPaymentMethod(), is(paymentMethod.name()));

        verify(itemServiceMock).decreaseQuantityByOne(actualItem);
    }

    @Test
    void itemWithZeroQuantityGetsItemNotFoundException() {
        Item zeroQuantityItem = new ItemBuilder().withQuantity(0L).build();
        String emailAddress = "arbitrary@example.com";
        Account userAccount = new AccountBuilder().withEmailAddress(emailAddress).build();
        PaymentMethod paymentMethod = PaymentMethod.CREDIT_CARD;

        when(accountServiceMock.findByEmailAddress(emailAddress)).thenReturn(Optional.of(userAccount));
        when(itemServiceMock.findById(5)).thenReturn(Optional.of(zeroQuantityItem));

        assertThrows(ItemNotFoundException.class,
                () -> orderService.createForUser(5, emailAddress, paymentMethod));
    }

    @Test
    void retrievesOrderFromMapper() {
        Optional<Order> expectedOptionalOrder = Optional.of(new OrderBuilder().build());
        when(orderMapperMock.findById(4)).thenReturn(expectedOptionalOrder);

        assertThat(orderService.findById(4), is(expectedOptionalOrder));
    }

    @Test
    void findAllByAccountEmail() throws AccountNotFoundException {
        Integer accountId = 1;
        String accountEmail = "me@example.com";
        Account account = new AccountBuilder()
                .withAccountId(accountId)
                .withEmailAddress(accountEmail)
                .build();
        List<Order> orders = singletonList(new OrderBuilder().build());

        when(accountServiceMock.findByEmailAddress(accountEmail)).thenReturn(Optional.of(account));
        when(orderMapperMock.findAllByAccountId(accountId)).thenReturn(orders);

        List<Order> actualOrders = orderService.findAllByAccountEmail(accountEmail);

        assertThat(actualOrders, is(orders));
    }

    @Test
    void findAllOrders() {
        List<Order> orders = singletonList(new OrderBuilder().build());
        when(orderMapperMock.findAll()).thenReturn(orders);

        List<Order> actualOrders = orderService.findAll();

        assertThat(actualOrders, is(orders));
    }
    
    @Test
    void updateOrderStatus() throws OrderNotFoundException {
        int orderId = 12;
        OrderStatus updatedStatus = OrderStatus.PAID;
        when(orderMapperMock.findById(orderId)).thenReturn(Optional.of(new OrderBuilder().build()));
    
        orderService.updateStatus(orderId, updatedStatus);
    
        ArgumentCaptor<Order> argumentCaptor = ArgumentCaptor.forClass(Order.class);
        verify(orderMapperMock).save(argumentCaptor.capture());
    
        Order updatedOrder = argumentCaptor.getValue();
        assertThat(updatedOrder.getStatus(), is(updatedStatus));
    }
}
