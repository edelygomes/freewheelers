package com.freewheelers.service;

import com.freewheelers.mapper.ItemMapper;
import com.freewheelers.model.Item;
import com.freewheelers.model.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ItemServiceTest {

    @Mock
    ItemMapper itemMapper;

    private ItemService itemService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        itemService = new ItemService(itemMapper);
    }

    @Test
    void shouldCreateItem() {
        Item item = new ItemBuilder().withItemId(null).build();
        itemService.save(item);

        verify(itemMapper).insert(item);
    }

    @Test
    void shouldUpdateItemIfAlreadyExists() {
        Item item = new ItemBuilder().withItemId(2).build();
        itemService.save(item);

        verify(itemMapper).update(item);
    }

    @Test
    void shouldUpdateItemWithDecrementedQuantity() {
        Item item = new ItemBuilder().withQuantity(4L).build();
        itemService.decreaseQuantityByOne(item);

        ArgumentCaptor<Item> argumentCaptor = ArgumentCaptor.forClass(Item.class);
        verify(itemMapper).update(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getQuantity(), is(3L));
    }

    @Test
    void shouldGetAllAvailableItems() {
        itemService.findAllAvailable();

        verify(itemMapper).findAllAvailable();
    }

    @Test
    void shouldGetItem() {
        int itemId = 55;
        Item item = new ItemBuilder().withItemId(itemId).build();
        when(itemMapper.findById(itemId)).thenReturn(Optional.of(item));

        itemService.findById(itemId);

        verify(itemMapper).findById(itemId);
    }

    @Test
    void shouldGetAllItems() {
        itemService.findAll();

        verify(itemMapper).findAll();
    }

    @Test
    void shouldDeleteItem() {
        Item expectedItem = new ItemBuilder().build();
        itemService.delete(expectedItem);

        verify(itemMapper).delete(expectedItem);
    }

    @Test
    void shouldSaveAllItems() {
        Item expectedItem1 = new ItemBuilder().withItemId(5).build();
        Item expectedItem2 = new ItemBuilder().withItemId(6).build();

        itemService.saveAll(Arrays.asList(
                expectedItem1,
                expectedItem2
        ));

        verify(itemMapper).update(expectedItem1);
        verify(itemMapper).update(expectedItem2);
    }

    @Test
    void shouldDeleteAllItems() {
        Item expectedItem1 = new ItemBuilder().withItemId(1).build();
        Item expectedItem2 = new ItemBuilder().withItemId(2).build();

        itemService.deleteAll(Arrays.asList(
                expectedItem1,
                expectedItem2
        ));

        verify(itemMapper).delete(expectedItem1);
        verify(itemMapper).delete(expectedItem2);
    }
}
