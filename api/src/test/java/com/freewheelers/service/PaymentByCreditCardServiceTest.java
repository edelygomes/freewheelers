package com.freewheelers.service;

import com.freewheelers.client.CreditCardPaymentAPIClient;
import com.freewheelers.client.dto.CreditCardPaymentAPIRequest;
import com.freewheelers.client.dto.CreditCardPaymentAPIResponse;
import com.freewheelers.model.CreditCardPaymentDetails;
import com.freewheelers.model.OrderStatus;
import com.freewheelers.model.PaymentMethod;
import com.freewheelers.model.builder.CreditCardPaymentDetailsBuilder;
import com.freewheelers.model.builder.OrderBuilder;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.CreditCardPaymentDeclinedException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentByCreditCardServiceTest {
		private PaymentByCreditCardService paymentByCreditCardService;
		private final String url = "https://test-payment-gateway.freewheelers.bike/authorise";
		private final String accountEmail = "me@example.com";
		private CreditCardPaymentAPIResponse creditCardPaymentAPIResponse;
		
		@Mock
		private OrderService orderServiceMock;
		
		@Mock
		private CreditCardPaymentAPIClient creditCardPaymentAPIClient;
		
		@BeforeEach
		public void setUp() {
				initMocks(this);
				
				paymentByCreditCardService = new PaymentByCreditCardService(orderServiceMock, creditCardPaymentAPIClient);
		}
		
		@Test
		void CreditCardPaymentAPIClientFailGetsCreditCardPaymentDeclinedException() throws AccountNotFoundException, ItemNotFoundException {
				creditCardPaymentAPIResponse = new CreditCardPaymentAPIResponse("UNAUTH", "1");
				CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().build();
				
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().build());
				when(creditCardPaymentAPIClient.pay(any(CreditCardPaymentAPIRequest.class)
								)).thenReturn(creditCardPaymentAPIResponse);
				
				assertThrows(CreditCardPaymentDeclinedException.class,
								() -> paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, accountEmail));
		}
		
		@Test
		void shouldCallCreditCardPaymentAPIClient() throws ItemNotFoundException, CreditCardPaymentDeclinedException, AccountNotFoundException, OrderNotFoundException {
				creditCardPaymentAPIResponse = new CreditCardPaymentAPIResponse("SUCCESS", "1");
				CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().build();
				
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().withOrderId(1).build());
				when(creditCardPaymentAPIClient.pay(any(CreditCardPaymentAPIRequest.class)
								)).thenReturn(creditCardPaymentAPIResponse);
				
				paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, accountEmail);
				
				verify(creditCardPaymentAPIClient).pay(any(CreditCardPaymentAPIRequest.class));
		}
		
		@Test
		void shouldCreateOrderForUser() throws ItemNotFoundException, CreditCardPaymentDeclinedException, AccountNotFoundException, OrderNotFoundException {
				creditCardPaymentAPIResponse = new CreditCardPaymentAPIResponse("SUCCESS", "1");
				CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().withItemId(5).build();
				
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().withOrderId(1).build());
				when(creditCardPaymentAPIClient.pay(any(CreditCardPaymentAPIRequest.class)
								)).thenReturn(creditCardPaymentAPIResponse);
				
				paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, accountEmail);
				
				verify(orderServiceMock).createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD);
		}
		
		@Test
		void shouldUpdateOrderStatusToPaid() throws ItemNotFoundException, CreditCardPaymentDeclinedException, AccountNotFoundException, OrderNotFoundException {
				creditCardPaymentAPIResponse = new CreditCardPaymentAPIResponse("SUCCESS", "1");
				CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().withItemId(5).build();
				
				when(creditCardPaymentAPIClient.pay(any(CreditCardPaymentAPIRequest.class)
								)).thenReturn(creditCardPaymentAPIResponse);
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().withOrderId(1).build());
				
				paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, accountEmail);
				
				verify(orderServiceMock).updateStatus(1, OrderStatus.PAID);
		}
		
		@Test
		void shouldReturnNewOrderId() throws ItemNotFoundException, CreditCardPaymentDeclinedException, AccountNotFoundException, OrderNotFoundException {
				creditCardPaymentAPIResponse = new CreditCardPaymentAPIResponse("SUCCESS", "1");
				CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().withItemId(5).build();
				
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().build());
				when(creditCardPaymentAPIClient.pay(any(CreditCardPaymentAPIRequest.class)
								)).thenReturn(creditCardPaymentAPIResponse);
				when(orderServiceMock.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD))
								.thenReturn(new OrderBuilder().withOrderId(1).build());
				
				int orderId = paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, accountEmail);
				
				assertEquals(orderId, 1);
		}
}
