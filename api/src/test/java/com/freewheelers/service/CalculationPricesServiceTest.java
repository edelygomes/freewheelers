package com.freewheelers.service;

import com.freewheelers.model.Item;
import com.freewheelers.model.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class CalculationPricesServiceTest {

    private CalculationPricesService calculationPricesService;

    private Item actualItem;

    @BeforeEach
    void setUp() {
        calculationPricesService = new CalculationPricesService();

        int itemId = 2;
        BigDecimal price = BigDecimal.valueOf(100.99);
        actualItem = new ItemBuilder().withItemId(itemId).withPrice(price).build();
    }

    @Test
    public void shouldCalculateFinalPriceForOrder() {
        BigDecimal finalPrice = calculationPricesService.calculateFinalPrice(actualItem);
        assertThat(finalPrice, is(BigDecimal.valueOf(126.24)));
    }





}
