package com.freewheelers.service;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.AccountRoleMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.AccountRole;
import com.freewheelers.model.builder.AccountBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AccountServiceTest {

    private AccountService accountService;
    
    @Mock
    FeatureToggleService featureToggleServiceMock;

    @Mock
    AccountMapper accountMapper;

    @Mock
    AccountRoleMapper accountRoleMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);

        accountService = new AccountService(accountMapper, accountRoleMapper, featureToggleServiceMock);
    }

    @Test
    void shouldCreateAccountWhenThereAreNoValidationErrors(){
        Account account = new AccountBuilder().build();

        accountService.createUser(account);

        verify(accountMapper).insert(account);
        verify(accountRoleMapper).insert(any(AccountRole.class));
    }


    @Test
    void shouldCreateAnAccountWithGivenCountry() {
        String expectedCountry = "Brazil";
        Account account = new AccountBuilder().withCountry(expectedCountry).build();

        accountService.createUser(account);

        assertThat(account.getCountry(), is(expectedCountry));
    }

    @Test
    void shouldCreateAnAccountWithGivenCityIfAddressFeatureToggleEnabled() {
        String expectedCity = "Manchester";
        when(featureToggleServiceMock.isEnabled("address")).thenReturn(true);
        Account account = new AccountBuilder().withCity(expectedCity).build();
        
        accountService.createUser(account);
        
        assertThat(account.getCity(), is(expectedCity));
    }

    @Test
    void shouldCreateAnAccountWithEmptyCityIfFeatureToggleDisabled() {
        when(featureToggleServiceMock.isEnabled("address")).thenReturn(false);
        Account account = new AccountBuilder().withCity(null).build();

        accountService.createUser(account);

        assertThat(account.getCity(), is(""));
    }

    @Test
    void shouldCreateAnAccountWithGivenTermsAndCoditionIfTermsAndConditionsFeatureToggleEnabled() {
        boolean expectedTermsAndConditions = true;

        when(featureToggleServiceMock.isEnabled("termsAndConditions")).thenReturn(true);

        Account account = new AccountBuilder().withTermsAndConditions(expectedTermsAndConditions).build();

        accountService.createUser(account);

        assertThat(account.getTermsAndConditions(), is(expectedTermsAndConditions));
    }

    @Test
    void shouldCreateAnAccountWithGivenTermsAndCoditionIfTermsAndConditionsIfFeatureToggleDisabled() {
        when(featureToggleServiceMock.isEnabled("termsAndConditions")).thenReturn(false);

        Account account = new AccountBuilder().withTermsAndConditions(false).build();

        accountService.createUser(account);

        assertThat(account.getTermsAndConditions(), is(false));
    }
}
