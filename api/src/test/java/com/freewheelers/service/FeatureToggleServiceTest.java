package com.freewheelers.service;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FeatureToggleServiceTest {

    @Test
    void shouldReturnAllFeatureToggles() {

        HashMap<String, Boolean> featureToggles = new HashMap<>();
        featureToggles.put("enabled-feature", true);
        featureToggles.put("disabled-feature", false);

        FeatureToggleService featureToggleService = new FeatureToggleService(featureToggles);

        Map<String, Boolean> allFeatureToggles = featureToggleService.findAll();

        assertThat(allFeatureToggles.size(), is(2));
        assertThat(allFeatureToggles.get("enabled-feature"), is(true));
        assertThat(allFeatureToggles.get("disabled-feature"), is(false));
    }

    @Test
    void shouldUpdateFeatureToggle() {
        HashMap<String, Boolean> featureToggles = new HashMap<>();
        featureToggles.put("my-feature", true);
        FeatureToggleService featureToggleService = new FeatureToggleService(featureToggles);

        assertTrue(featureToggleService.isEnabled("my-feature"));

        featureToggleService.update("my-feature", false);
        assertFalse(featureToggleService.isEnabled("my-feature"));
    }

    @Test
    void isEnabledRetunsFalseForAnUnknownFeature() {
        FeatureToggleService featureToggleService = new FeatureToggleService(new HashMap<>());

        assertFalse(featureToggleService.isEnabled("some-feature"));
    }
}
