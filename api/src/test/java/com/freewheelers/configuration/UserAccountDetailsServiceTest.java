package com.freewheelers.configuration;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.AccountRoleMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.AccountRole;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.model.builder.AccountRoleBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
class UserAccountDetailsServiceTest {
    @Mock
    AccountMapper accountMapper;
    @Mock
    AccountRoleMapper accountRoleMapper;

    UserAccountDetailsService userAccountDetailsService;

    @BeforeEach
    void setUp() {
        initMocks(this);

        userAccountDetailsService = new UserAccountDetailsService(accountMapper, accountRoleMapper);
    }

    @Test
    void loadUserByUsername() {
        Integer id = 1;
        String emailAddress = "user@email.com";
        String password = "password";
        String role = "USER";

        Account account = new AccountBuilder()
                .withAccountId(id)
                .withEmailAddress(emailAddress)
                .withPassword(password)
                .build();

        AccountRole accountRole = new AccountRoleBuilder().withRole(role).build();

        when(accountMapper.findByEmailAddress(emailAddress)).thenReturn(Optional.of(account));
        when(accountRoleMapper.findByAccountId(id)).thenReturn(Optional.of(accountRole));

        UserDetails userDetails = userAccountDetailsService.loadUserByUsername(emailAddress);

        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(emailAddress);
        assertThat(userDetails.getPassword()).isEqualTo(password);

        GrantedAuthority[] authorities = userDetails.getAuthorities().toArray(new GrantedAuthority[0]);
        assertThat(authorities).isNotEmpty();
        GrantedAuthority authority = authorities[0];
        assertThat(authority.getAuthority()).isEqualTo(role);
    }

    @Test
    void loadUserByUsernameWhenUserNotFoundThrowsException() {
        String emailAddress = "user@email.com";

        when(accountMapper.findByEmailAddress(emailAddress)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userAccountDetailsService.loadUserByUsername(emailAddress))
                .isInstanceOf(UsernameNotFoundException.class);
    }
}