package com.freewheelers.model.builder;

import com.freewheelers.model.AccountRole;

public class AccountRoleBuilder {

    private Integer roleId = null;
    private Integer accountId = 1;
    private String role = "ROLE_USER";

    public AccountRoleBuilder withRoleId(Integer roleId) {
        this.roleId = roleId;
        return this;
    }

    public AccountRoleBuilder withAccountId(Integer accountId) {
        this.accountId = accountId;
        return this;
    }

    public AccountRoleBuilder withRole(String role) {
        this.role = role;
        return this;
    }

    public AccountRole build() {
        AccountRole accountRole = new AccountRole(accountId, role);
        accountRole.setId(roleId);
        return accountRole;
    }
}
