package com.freewheelers.model.builder;

import com.freewheelers.model.*;

import java.math.BigDecimal;
import java.util.Date;

public class OrderBuilder {

    private Integer orderId = null;
    private Account account = new Account("arbitrary", "arbitrary", "arbitrary@example.com", "123456789","Canada", "Vancouver", "BATMAN 1234", "Elm Street", "97","402", true);
    private Item item = new ItemBuilder().build();
    private OrderStatus status = OrderStatus.NEW;
    private String note = "default note";
    private Date reserveTime = new Date();
    private BigDecimal finalPrice = BigDecimal.valueOf(12.49);
    private PaymentMethod paymentMethod = PaymentMethod.CREDIT_CARD;

    public OrderBuilder withOrderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public OrderBuilder withAccount(Account account) {
        this.account = account;
        return this;
    }

    public OrderBuilder withItem(Item item) {
        this.item = item;
        return this;
    }

    public OrderBuilder withOrderStatus(OrderStatus orderStatus) {
        this.status = orderStatus;
        return this;
    }

    public OrderBuilder withNote(String note) {
        this.note = note;
        return this;
    }

    public OrderBuilder withReserveTime(Date reserveTime) {
        this.reserveTime = reserveTime;
        return this;
    }

    public OrderBuilder withPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public Order build() {
        Order order = new Order(account, item, reserveTime, finalPrice);
        order.setId(orderId);
        order.setStatus(status);
        order.setNote(note);
        order.setPaymentMethod(paymentMethod);
        return order;
    }
}
