package com.freewheelers.model.builder;

import com.freewheelers.model.Item;
import com.freewheelers.model.ItemType;

import java.math.BigDecimal;

public class ItemBuilder {

    private Integer itemId = null;
    private String name = "Some item name";
    private String description = "Some item description";
    private BigDecimal price = BigDecimal.valueOf(9.99);
    private Long quantity = 1L;
    private ItemType type = ItemType.ACCESSORIES;

    public ItemBuilder withItemId(Integer itemId) {
        this.itemId = itemId;
        return this;
    }

    public ItemBuilder withName(String itemName) {
        this.name = itemName;
        return this;
    }

    public ItemBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ItemBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public ItemBuilder withQuantity(Long quantity){
        this.quantity = quantity;
        return this;
    }

    public ItemBuilder withType(ItemType itemType) {
        this.type = itemType;
        return this;
    }

    public Item build() {
        Item item = new Item();
        item.setId(itemId);
        item.setName(name);
        item.setDescription(description);
        item.setQuantity(quantity);
        item.setPrice(price);
        item.setType(type);
        return item;
    }
}
