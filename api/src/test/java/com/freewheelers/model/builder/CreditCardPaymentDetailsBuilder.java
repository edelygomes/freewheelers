package com.freewheelers.model.builder;

import com.freewheelers.model.CreditCardPaymentDetails;

import java.math.BigDecimal;

public class CreditCardPaymentDetailsBuilder {
    private String cardNumber = "378282246310005";
    private String csc = "7997";
    private String expiry = "09-2025";
    private Integer itemId = 1;

    public CreditCardPaymentDetailsBuilder withCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public CreditCardPaymentDetailsBuilder withCsc(String csc) {
        this.csc = csc;
        return this;
    }

    public CreditCardPaymentDetailsBuilder withExpiry(String expiry) {
        this.expiry = expiry;
        return this;
    }
    
    public CreditCardPaymentDetailsBuilder withItemId(Integer itemId) {
        this.itemId = itemId;
        return this;
    }

    public CreditCardPaymentDetails build() {
        return new CreditCardPaymentDetails(cardNumber, csc, expiry, itemId);
    }
}
