package com.freewheelers.model.builder;

import com.freewheelers.model.Account;

import static java.util.UUID.randomUUID;

public class AccountBuilder {

    private Integer accountId = null;
    private String accountName = "Some Body";
    private String emailAddress = randomUUID() + "some.body@example.com";
    private String password = "V3ry S3cret";
    private String phoneNumber = "12345";
    private String country = "UK";
    private String city = "London";
    private String zipcode = "BATMAN 1234";
    private String street = "Elm Street";
    private String streetNumber = "97";
    private String unitNumber = "402";
    private boolean termsAndConditions = true;

    public AccountBuilder withAccountId(Integer accountId) {
        this.accountId = accountId;
        return this;
    }

    public AccountBuilder withAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public AccountBuilder withEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public AccountBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public AccountBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public AccountBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public AccountBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public AccountBuilder withZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }

    public  AccountBuilder withSteet(String street){
        this.street = street;
        return  this;
    }

    public  AccountBuilder withStreetNumber(String streetNumber){
        this.streetNumber = streetNumber;
        return  this;
    }

    public  AccountBuilder withUnitNumber(String unitNumber){
        this.unitNumber = unitNumber;
        return  this;
    }

    public AccountBuilder withTermsAndConditions(boolean termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
        return this;
    }

    public Account build() {
        return new Account(accountId, accountName, password, emailAddress, phoneNumber, country, city, zipcode,street, streetNumber,unitNumber, termsAndConditions);
    }

}
