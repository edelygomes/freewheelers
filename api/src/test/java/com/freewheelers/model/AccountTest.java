package com.freewheelers.model;

import com.freewheelers.model.builder.AccountBuilder;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class AccountTest {

    @Test
    void testCreatingNewAccounts() {
        Account account = new AccountBuilder().build();
        account.setAccountName("Bob");
        account.setPassword("password");
        account.setEmailAddress("foo@bar.com");
        account.setPhoneNumber("123443245");
        account.setCountry("Canada");
        account.setCity("Kazajstan");
        account.setZipcode("BATMAN 1234");
        account.setStreet("Elm Street");
        account.setStreetNumber("97");

        assertThat(account.getAccountName(), is("Bob"));
        assertThat(account.getPassword(), is("password"));
        assertThat(account.getEmailAddress(), is("foo@bar.com"));
        assertThat(account.getPhoneNumber(), is("123443245"));
        assertThat(account.getCountry(), is("Canada"));
        assertThat(account.getCity(), is("Kazajstan"));
        assertThat(account.getZipcode(), is("BATMAN 1234"));
        assertThat(account.getStreet(), is("Elm Street"));
        assertThat(account.getStreetNumber(), is("97"));
    }
}
