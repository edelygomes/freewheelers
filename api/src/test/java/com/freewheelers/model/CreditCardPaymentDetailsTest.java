package com.freewheelers.model;

import com.freewheelers.model.builder.CreditCardPaymentDetailsBuilder;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CreditCardPaymentDetailsTest {
    @Test
    public void testCreatingNewCreditCard() {
        CreditCardPaymentDetails creditCardPaymentDetails = new CreditCardPaymentDetailsBuilder().build();

        assertThat(creditCardPaymentDetails.getCardNumber(), is("378282246310005"));
        assertThat(creditCardPaymentDetails.getCsc(), is("7997"));
        assertThat(creditCardPaymentDetails.getExpiry(), is("09-2025"));
        assertThat(creditCardPaymentDetails.getItemId(), is(1));
    }
}
