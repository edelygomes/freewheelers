package com.freewheelers.model;

import com.freewheelers.model.builder.ItemBuilder;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ItemTest {
    @Test
    void isAvailableWhenQuantityIsGreaterThanZero() {
        Item item = new ItemBuilder().withQuantity(1L).build();
        assertThat(item.isAvailable()).isTrue();
    }

    @Test
    void isNotAvailableWhenQuantityIsMinorThanOne() {
        Item item = new ItemBuilder().withQuantity(0L).build();
        assertThat(item.isAvailable()).isFalse();
    }

    @Test
    void isNotAvailableWhenQuantityIsNull() {
        Item item = new ItemBuilder().withQuantity(null).build();
        assertThat(item.isAvailable()).isFalse();
    }
}