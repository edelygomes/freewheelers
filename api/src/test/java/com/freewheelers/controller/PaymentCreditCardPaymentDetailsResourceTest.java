package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.model.CreditCardPaymentDetails;
import com.freewheelers.service.FeatureToggleService;
import com.freewheelers.service.ItemService;
import com.freewheelers.service.PaymentByCreditCardService;
import com.freewheelers.service.exception.CreditCardPaymentDeclinedException;
import com.freewheelers.service.exception.ItemNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PaymentCreditCardResource.class)
public class PaymentCreditCardPaymentDetailsResourceTest {
    private final String accountEmail = "someUser@example.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @MockBean
    FeatureToggleService featureToggleService;

    @MockBean
    PaymentByCreditCardService paymentByCreditCardService;

    @MockBean
    ItemService itemService;

    @Test
    @WithMockUser(username = accountEmail)
    public void submitCreditCardPayment() throws Exception {
        String creditCardNumber = "4111111111111111";
        String csc = "534";
        String expireDate = "11-2020";
        int itemId = 1;

        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + creditCardNumber + "\", " +
                "\"csc\": \"" + csc + "\", " +
                "\"expireDate\": \"" + expireDate + "\", " +
                "\"itemId\": \"" + itemId + "\"" +
                "}";

        when(featureToggleService.isEnabled("payment")).thenReturn(true);

        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = accountEmail)
    void returns400BadRequestWhenRequiredFieldsMissing() throws Exception {
        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + "4111111111111111" + "}";

        when(featureToggleService.isEnabled("payment")).thenReturn(true);

        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = accountEmail)
    public void returns400BadRequestWhenRequiredFieldsInvalid() throws Exception {
        String creditCardNumber = "4111111111111111";
        String csc = "badCSC";
        String expireDate = "11-2025";
        int itemId = 1;

        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + creditCardNumber + "\", " +
                "\"csc\": \"" + csc + "\", " +
                "\"expireDate\": \"" + expireDate + "\", " +
                "\"itemId\": \"" + itemId + "\"" +
                "}";

        when(featureToggleService.isEnabled("payment")).thenReturn(true);

        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = accountEmail)
    public void returns501NotImplementedWhenFeatureToggleIsDisabled() throws Exception {
        String creditCardNumber = "4111111111111111";
        String csc = "534";
        String expireDate = "11-2025";
        int itemId = 1;

        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + creditCardNumber + "\", " +
                "\"csc\": \"" + csc + "\", " +
                "\"expireDate\": \"" + expireDate + "\", " +
                "\"itemId\": \"" + itemId + "\"" +
                "}";

        when(featureToggleService.isEnabled("payment")).thenReturn(false);

        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isNotImplemented());
    }

    @Test
    @WithMockUser(username = accountEmail)
    public void returns500WhenServiceThrowsAnItemNotFoundException() throws Exception {
        String creditCardNumber = "4111111111111111";
        String csc = "534";
        String expireDate = "11-2025";
        int itemId = 1;

        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + creditCardNumber + "\", " +
                "\"csc\": \"" + csc + "\", " +
                "\"expireDate\": \"" + expireDate + "\", " +
                "\"itemId\": \"" + itemId + "\"" +
                "}";

        when(featureToggleService.isEnabled("payment")).thenReturn(true);
        when(paymentByCreditCardService.submitPaymentForUser(any(CreditCardPaymentDetails.class), eq(accountEmail))).thenThrow(new ItemNotFoundException());

        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isInternalServerError());
    }
    
    @Test
    @WithMockUser(username = accountEmail)
    public void returns500WhenServiceThrowsAnCreditCardPaymentDeclinedException() throws Exception {
        String creditCardNumber = "4111111111111111";
        String csc = "534";
        String expireDate = "11-2025";
        int itemId = 1;
        
        String creditCardPaymentAsJson = "{ \"creditCardNumber\": \"" + creditCardNumber + "\", " +
                "\"csc\": \"" + csc + "\", " +
                "\"expireDate\": \"" + expireDate + "\", " +
                "\"itemId\": \"" + itemId + "\"" +
                "}";
        
        when(featureToggleService.isEnabled("payment")).thenReturn(true);
        when(paymentByCreditCardService.submitPaymentForUser(any(CreditCardPaymentDetails.class), eq(accountEmail))).thenThrow(new CreditCardPaymentDeclinedException());
        
        mockMvc.perform(
                post("/paymentCreditCard")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(creditCardPaymentAsJson))
                .andExpect(status().isInternalServerError());
    }
}