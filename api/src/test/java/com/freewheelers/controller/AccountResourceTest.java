package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.model.Account;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.service.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountResource.class)
class AccountResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @Test
    void createsAnAccount() throws Exception {
        String emailAddress = "test@example.com";
        String password = "Super5ecur3Passw0rd";
        String accountName = "arbitrary name";
        String phoneNumber = "55512345";
        String country = "UK";

        String accountAsJson = "{ \"emailAddress\": \"" + emailAddress + "\", " +
                "\"password\": \"" + password + "\", " +
                "\"accountName\": \"" + accountName + "\", " +
                "\"phoneNumber\": \"" + phoneNumber + "\", " +
                "\"country\": \"" + country + "\"" +
                "}";

        Account createdAccount = new AccountBuilder()
                .withEmailAddress(emailAddress)
                .withPassword(password)
                .withPhoneNumber(phoneNumber)
                .withAccountName(accountName)
                .withCountry(country)
                .build();

        mockMvc.perform(
                post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(accountAsJson))
                .andExpect(status().isCreated());

        verify(accountService).createUser(createdAccount);
    }

    @Test
    void returns400BadRequestWhenInputsAreInvalid() throws Exception {
        String emailAddress = "bad email";
        String password = "badpassword";
        String accountName = "";
        String phoneNumber = "bad phone number";

        String accountAsJson = "{ \"emailAddress\": \"" + emailAddress + "\", " +
                "\"password\": \"" + password + "\", " +
                "\"accountName\": \"" + accountName + "\", " +
                "\"phoneNumber\": \"" + phoneNumber + "\"" +
                "}";

        mockMvc.perform(
                post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(accountAsJson))
                .andExpect(status().isBadRequest());

        verify(accountService, never()).createUser(ArgumentMatchers.any());
    }

    @Test
    void returns400BadRequestWhenRequiredFieldsMissing() throws Exception {
        String accountAsJson = "{ \"accountName\": \"" + "Account with missing fields" + "}";

        mockMvc.perform(
                post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(accountAsJson))
                .andExpect(status().isBadRequest());

        verify(accountService, never()).createUser(ArgumentMatchers.any());
    }

    @Test
    @WithMockUser(username = "user@example.com", authorities = "ROLE_USER")
    void getsAccountByID() throws Exception {
        Account account = new AccountBuilder()
                .withAccountName("account name")
                .withEmailAddress("email address")
                .withPhoneNumber("phone number")
                .build();
        when(accountService.findByEmailAddress(anyString())).thenReturn(Optional.of(account));

        mockMvc.perform(get("/accounts/user@example.com"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountName").value(account.getAccountName()))
                .andExpect(jsonPath("$.emailAddress").value(account.getEmailAddress()))
                .andExpect(jsonPath("$.phoneNumber").value(account.getPhoneNumber()));
    }

    @Test
    @WithMockUser(username = "user@example.com", authorities = "ROLE_USER")
    void returnsNotFoundErrorIfThereIsNoAccountByEmail() throws Exception {
        when(accountService.findByEmailAddress(anyString())).thenReturn(Optional.empty());

        mockMvc.perform(get("/accounts/user@example.com"))
                .andExpect(status().isNotFound());
    }

    @Test
    void rejectsUnauthenticatedAccountRequests() throws Exception {
        mockMvc.perform(get("/accounts/user@example.com"))
                .andExpect(status().isUnauthorized());
    }
}
