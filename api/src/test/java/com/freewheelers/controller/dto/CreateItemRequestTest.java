package com.freewheelers.controller.dto;

import com.freewheelers.controller.dto.builder.ItemRequestBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class CreateItemRequestTest {
    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder().buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void emptyItemNameShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withName("")
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be blank"));
    }

    @Test
    void noItemNameShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withName(null)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be blank"));
    }

    @Test
    void noPriceShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withPrice(null)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void zeroPriceShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withPrice(BigDecimal.valueOf(0))
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be greater than 0"));
    }

    @Test
    void nagetivePriceShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withPrice(BigDecimal.valueOf(-1))
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be greater than 0"));
    }

    @Test
    void emptyDescriptionShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withDescription("")
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be blank"));
    }

    @Test
    void noTypeShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withType(null)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void zeroQuantityShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withQuantity(0L)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be greater than or equal to 1"));
    }

    @Test
    void negativeQuantityShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withQuantity(-1L)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be greater than or equal to 1"));
    }

    @Test
    void noQuantityShouldFailValidator() {
        CreateItemRequest createItemRequest = new ItemRequestBuilder()
                .withQuantity(null)
                .buildCreateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(createItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }
}
