package com.freewheelers.controller.dto.builder;

import com.freewheelers.controller.dto.CreateItemRequest;
import com.freewheelers.controller.dto.UpdateItemRequest;
import com.freewheelers.model.ItemType;

import java.math.BigDecimal;

public class ItemRequestBuilder {
    private Integer id;
    private String name;
    private BigDecimal price;
    private String description;
    private ItemType type;
    private Long quantity;

    public ItemRequestBuilder() {
        this.name = "some name";
        this.price = BigDecimal.valueOf(1);
        this.description = "some description";
        this.type = ItemType.FRAME;
        this.quantity = 1L;
    }

    public CreateItemRequest buildCreateItemRequest() {
        return new CreateItemRequest(name, price, description, type, quantity);
    }

    public UpdateItemRequest buildUpdateItemRequest() {
        return new UpdateItemRequest(name, price, description, type, quantity, id);
    }

    public ItemRequestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ItemRequestBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public ItemRequestBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ItemRequestBuilder withType(ItemType type) {
        this.type = type;
        return this;
    }

    public ItemRequestBuilder withQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public ItemRequestBuilder withId(Integer id) {
        this.id = id;
        return this;
    }
}
