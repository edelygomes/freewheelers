package com.freewheelers.controller.dto.builder;

import com.freewheelers.controller.dto.PaymentByCreditCardRequest;

import java.math.BigDecimal;

public class PaymentByCreditCardRequestBuilder {
    private String creditCardNumber;
    private String csc;
    private String expireDate;
    private Integer itemId;
    
    public PaymentByCreditCardRequestBuilder() {
        this.creditCardNumber = "4111111111111111";
        this.csc = "534";
        this.expireDate = "11-2025";
        this.itemId = 1;
    }

    public PaymentByCreditCardRequest build() {
        return new PaymentByCreditCardRequest(creditCardNumber, csc, expireDate, itemId);
    }

    public PaymentByCreditCardRequestBuilder withCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
        return this;
    }

    public PaymentByCreditCardRequestBuilder withCsc(String csc) {
        this.csc = csc;
        return this;
    }

    public PaymentByCreditCardRequestBuilder withExpireDate(String expireDate) {
        this.expireDate = expireDate;
        return this;
    }

		public PaymentByCreditCardRequestBuilder withItemId(Integer itemId) {
        this.itemId = itemId;
        return this;
		}
}