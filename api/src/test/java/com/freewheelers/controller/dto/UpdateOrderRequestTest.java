package com.freewheelers.controller.dto;

import com.freewheelers.model.OrderStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class UpdateOrderRequestTest {
    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setNote("");
        updateOrderRequest.setOrderStatus(OrderStatus.IN_PROGRESS);

        Set<ConstraintViolation<UpdateOrderRequest>> violations = getValidator().validate(updateOrderRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void noOrderNotesShouldFailValidator() {
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setOrderStatus(OrderStatus.IN_PROGRESS);

        Set<ConstraintViolation<UpdateOrderRequest>> violations = getValidator().validate(updateOrderRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void noOrderStatusShouldFailValidator() {
        UpdateOrderRequest updateOrderRequest = new UpdateOrderRequest();
        updateOrderRequest.setNote("");

        Set<ConstraintViolation<UpdateOrderRequest>> violations = getValidator().validate(updateOrderRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }
}