package com.freewheelers.controller.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Locale;

class RequestValidatorTestHelper implements BeforeAllCallback, AfterAllCallback {
    private Locale locale;

    @Override
    public void beforeAll(ExtensionContext context) {
        locale = Locale.getDefault();
        Locale.setDefault(Locale.ENGLISH);
    }

    @Override
    public void afterAll(ExtensionContext context) {
        Locale.setDefault(locale);
    }

    static Validator getValidator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }
}