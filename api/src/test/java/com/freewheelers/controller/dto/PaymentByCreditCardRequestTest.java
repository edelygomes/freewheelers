package com.freewheelers.controller.dto;

import com.freewheelers.controller.dto.builder.PaymentByCreditCardRequestBuilder;
import com.freewheelers.model.CreditCardPaymentDetails;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

@ExtendWith(RequestValidatorTestHelper.class)
public class PaymentByCreditCardRequestTest {
    @Test
    public void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder().build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    public void creditCardNumberWithLessThan15DigitsShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withCreditCardNumber("41111111111111")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid credit card number"));
    }

    @Test
    public void creditCardNumberWithMoreThan16DigitsShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withCreditCardNumber("41111111111113122")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid credit card number"));
    }

    @Test
    public void cscNumberWithLessThan3DigitsShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withCsc("12")
                .build();
        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid csc number"));
    }

    @Test
    public void cscNumberWithMoreThan4DigitsShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withCsc("12555")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid csc number"));
    }

    @Test
    public void blankExpireDateShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withExpireDate("")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid expire date"));
    }

    @Test
    public void expireDateWithoutSeparatorShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withExpireDate("112025")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid expire date"));
    }

    @Test
    public void expireDateWithTwoDigitsForYearShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withExpireDate("11-20")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid expire date"));
    }

    @Test
    public void expireDateWithOneDigitForMonthShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withExpireDate("9-20")
                .build();

        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("invalid expire date"));
    }
    
    @Test
    public void nullItemIdShouldFailValidator() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withItemId(null)
                .build();
    
        Set<ConstraintViolation<PaymentByCreditCardRequest>> violations = getValidator().validate(paymentByCreditCardRequest);
    
        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("null item ID"));
    }
    
    @Test
    public void returnsNewCreditCard() {
        PaymentByCreditCardRequest paymentByCreditCardRequest = new PaymentByCreditCardRequestBuilder()
                .withItemId(2)
                .build();

        CreditCardPaymentDetails creditCardPaymentDetails = paymentByCreditCardRequest.toCreditCardPaymentDetails();

        assertThat(creditCardPaymentDetails.getCardNumber(), is("4111111111111111"));
        assertThat(creditCardPaymentDetails.getCsc(), is("534"));
        assertThat(creditCardPaymentDetails.getExpiry(), is("11-2025"));
        assertThat(creditCardPaymentDetails.getItemId(), is(2));
    }
}