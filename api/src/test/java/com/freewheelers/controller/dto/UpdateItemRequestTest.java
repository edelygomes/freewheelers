package com.freewheelers.controller.dto;

import com.freewheelers.controller.dto.builder.ItemRequestBuilder;
import com.freewheelers.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static com.freewheelers.model.ItemType.FRAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class UpdateItemRequestTest {
    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        UpdateItemRequest updateItemRequest = new ItemRequestBuilder().withId(1).buildUpdateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(updateItemRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void noItemIdShouldFailValidator() {
        UpdateItemRequest updateItemRequest = new ItemRequestBuilder()
                .withId(null)
                .buildUpdateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(updateItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void negativeItemIdShouldFailValidator() {
        UpdateItemRequest updateItemRequest = new ItemRequestBuilder()
                .withId(-1)
                .buildUpdateItemRequest();

        Set<ConstraintViolation<CreateItemRequest>> violations = getValidator().validate(updateItemRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be greater than 0"));
    }

    @Test
    void shouldConvertUpdateItemRequestToItem() {
        UpdateItemRequest updateItemRequest = new ItemRequestBuilder().withId(1).buildUpdateItemRequest();

        Item item = updateItemRequest.toItem();

        assertThat(item.getId(), is(1));
        assertThat(item.getDescription(), is("some description"));
        assertThat(item.getName(), is("some name"));
        assertThat(item.getPrice(), is(BigDecimal.ONE));
        assertThat(item.getQuantity(), is(1L));
        assertThat(item.getType(), is(FRAME));
    }
}