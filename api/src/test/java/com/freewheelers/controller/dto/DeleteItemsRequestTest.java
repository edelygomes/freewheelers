package com.freewheelers.controller.dto;

import com.freewheelers.model.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class DeleteItemsRequestTest {
    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        DeleteItemsRequest deleteItemsRequest = new DeleteItemsRequest(asList(1, 2));

        Set<ConstraintViolation<DeleteItemsRequest>> violations = getValidator().validate(deleteItemsRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void noItemIdShouldFailValidator() {
        DeleteItemsRequest deleteItemsRequest = new DeleteItemsRequest(emptyList());

        Set<ConstraintViolation<DeleteItemsRequest>> violations = getValidator().validate(deleteItemsRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be empty"));
    }

    @Test
    void emptyItemIdShouldFailValidator() {
        DeleteItemsRequest deleteItemsRequest = new DeleteItemsRequest(asList(1, null));

        Set<ConstraintViolation<DeleteItemsRequest>> violations = getValidator().validate(deleteItemsRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void itemIdRequestCouldTransferToItemModel() {
        DeleteItemsRequest deleteItemsRequest = new DeleteItemsRequest(asList(1, 2));


        List<Item> items = deleteItemsRequest.toItems();
        assertThat(items.size(), is(2));
        assertThat(items.get(0).getId(), is(1));
        assertThat(items.get(1).getId(), is(2));
    }
}