package com.freewheelers.controller.dto;

import com.freewheelers.controller.dto.builder.ItemRequestBuilder;
import com.freewheelers.model.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;

import static com.freewheelers.model.ItemType.FRAME;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class UpdateItemsRequestTest {

    private UpdateItemsRequest updateItemsRequest;

    @BeforeEach
    void setUp() {
        updateItemsRequest = new UpdateItemsRequest();

    }

    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        updateItemsRequest.setItems(singletonList(new ItemRequestBuilder().withId(1).buildUpdateItemRequest()));

        Set<ConstraintViolation<UpdateItemsRequest>> violations = getValidator().validate(updateItemsRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void noItemsShouldFailValidator() {
        updateItemsRequest.setItems(emptyList());

        Set<ConstraintViolation<UpdateItemsRequest>> violations = getValidator().validate(updateItemsRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be empty"));
    }

    @Test
    void noItemIdShouldFailValidator() {
        updateItemsRequest.setItems(singletonList(new ItemRequestBuilder().buildUpdateItemRequest()));

        Set<ConstraintViolation<UpdateItemsRequest>> violations = getValidator().validate(updateItemsRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be null"));
    }

    @Test
    void noDescriptionShouldFailValidator() {

        UpdateItemRequest updateItemRequest = new ItemRequestBuilder().withId(1)
                .withDescription(null).buildUpdateItemRequest();
        updateItemsRequest.setItems(singletonList(updateItemRequest));

        Set<ConstraintViolation<UpdateItemsRequest>> violations = getValidator().validate(updateItemsRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be blank"));
    }

    @Test
    void shouldConvertUpdateItemsRequestToItems() {
        updateItemsRequest.setItems(singletonList(new ItemRequestBuilder().withId(1).buildUpdateItemRequest()));

        List<Item> items = updateItemsRequest.toItems();

        assertThat(items.size(), is(1));
        assertThat(items.get(0).getId(), is(1));
        assertThat(items.get(0).getDescription(), is("some description"));
        assertThat(items.get(0).getName(), is("some name"));
        assertThat(items.get(0).getPrice(), is(BigDecimal.ONE));
        assertThat(items.get(0).getQuantity(), is(1L));
        assertThat(items.get(0).getType(), is(FRAME));
    }
}