package com.freewheelers.controller.dto.builder;

import com.freewheelers.controller.dto.CreateAccountRequest;

public class CreateAccountRequestBuilder {
    private String country;
    private String emailAddress;
    private String accountName;
    private String password;
    private String phoneNumber;
    private String city;
    private String zipcode;
    private String street;
    private String streetNumber;
    private String unitNumber;
    private boolean termsAndConditions;

    public CreateAccountRequestBuilder() {
        this.emailAddress = "johnsmith@example.com";
        this.accountName = "John Smith";
        this.password = "Password987";
        this.phoneNumber = "9998887777";
        this.country = "UK";
        this.city = "London";
        this.zipcode = "BATMAN 1234";
        this.street = "Elm Street";
        this.streetNumber = "97";
        this.unitNumber = "402";
        this.termsAndConditions = true;
    }

    public CreateAccountRequest build() {
        return new CreateAccountRequest(emailAddress, accountName, password, phoneNumber, country, city, zipcode,street, streetNumber,unitNumber, termsAndConditions);
    }

    public CreateAccountRequestBuilder withEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public CreateAccountRequestBuilder withAccountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public CreateAccountRequestBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public CreateAccountRequestBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
    
    public CreateAccountRequestBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public CreateAccountRequestBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public CreateAccountRequestBuilder withZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }
    public CreateAccountRequestBuilder withStreet(String street) {
        this.street = street;
        return this;
    }
    public CreateAccountRequestBuilder withStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public CreateAccountRequestBuilder withUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
        return this;
    }



    public CreateAccountRequestBuilder withTermsAndConditions(boolean termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
        return this;
    }
}
