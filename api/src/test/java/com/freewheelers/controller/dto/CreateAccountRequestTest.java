package com.freewheelers.controller.dto;

import com.freewheelers.controller.dto.builder.CreateAccountRequestBuilder;
import com.freewheelers.model.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static com.freewheelers.controller.dto.RequestValidatorTestHelper.getValidator;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@ExtendWith(RequestValidatorTestHelper.class)
class CreateAccountRequestTest {
    @Test
    void shouldNotReturnViolationsWhenAllFieldsAreValid() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder().build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(0));
    }

    @Test
    void blankAccountNameShouldFailValidator() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withAccountName("")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must not be blank"));
    }

    @Test
    void nonWellFormedEmailShouldFailValidator() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withEmailAddress("bad email")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be a well-formed email address"));
    }

    @Test
    void passwordThatsTooShortShouldFailValidator() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withPassword("2Short")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must be at least 8 characters long"));
    }

    @Test
    void passwordWithoutAtLeastALowercaseLetterShouldFail() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withPassword("PASSWORD1")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must have at least one lower case letter"));
    }

    @Test
    void passwordWithoutAtLeastAnUppercaseLetterShouldFail() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withPassword("password1")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must have at least one upper case letter"));
    }

    @Test
    void passwordWithoutAtLeastANumberShouldFail() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withPassword("Password")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must have at least one number"));
    }

    @Test
    void blankPhoneNumberShouldFail() {
        CreateAccountRequest createAccountRequest = new CreateAccountRequestBuilder()
                .withPhoneNumber("")
                .build();

        Set<ConstraintViolation<CreateAccountRequest>> violations = getValidator().validate(createAccountRequest);

        assertThat(violations.size(), is(1));
        assertThat(violations.iterator().next().getMessage(), is("must enter a valid phone number containing only numbers"));
    }
}
