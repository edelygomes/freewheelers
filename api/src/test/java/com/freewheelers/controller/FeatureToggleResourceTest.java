package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.service.FeatureToggleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(FeatureToggleResource.class)
class FeatureToggleResourceTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    FeatureToggleService featureToggleService;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @Test
    @WithMockUser(username = "user@example.com", authorities = "ROLE_USER")
    void shouldReturnFeatureToggles() throws Exception{
        HashMap<String, Boolean> featureToggles = new HashMap<>();
        featureToggles.put("test1", true);
        featureToggles.put("test2", false);

        given(featureToggleService.findAll()).willReturn(featureToggles);
        mockMvc.perform(get("/toggles")
                    .accept(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.test1").value(true))
               .andExpect(jsonPath("$.test2").value(false));

    }

    @Test
    @WithMockUser(username = "admin@freewheelers.bike", authorities = "ROLE_ADMIN")
    void adminShouldBeAllowedToUpdateFeatureToggles() throws Exception{
        String requestBody = "true";

        mockMvc.perform(put("/toggles/test1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isNoContent());

        verify(featureToggleService).update("test1", true);
    }

    @Test
    @WithMockUser(username = "user@example.com", authorities = "ROLE_USER")
    void nonAdminUserShouldNotBeAllowedToUpdateFeatureToggles() throws Exception {
        String requestBody = "true";

        mockMvc.perform(put("/toggles/test1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isForbidden());
    }
}
