package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.model.Item;
import com.freewheelers.model.ItemType;
import com.freewheelers.model.builder.ItemBuilder;
import com.freewheelers.service.FeatureToggleService;
import com.freewheelers.service.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.freewheelers.model.ItemType.FRAME;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(ItemResource.class)
class ItemResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ItemService itemService;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @MockBean
    FeatureToggleService featureToggleService;

    @Test
    void thisTestAlwaysPasses() {
        assertTrue(true);
    }

    @Test
    void shouldGetAllItems() throws Exception {
        List<Item> expectedItemList = singletonList(new ItemBuilder().withName("My Item").build());
        when(itemService.findAllAvailable()).thenReturn(expectedItemList);

        mockMvc.perform(get("/items")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.items", hasSize(1)))
                .andExpect(jsonPath("$.items[0].name").value("My Item"));
    }

    @Test
    void getsAnItemById() throws Exception {
        Integer itemId = 123;
        Item expectedItem = new ItemBuilder().withItemId(itemId).build();
        when(itemService.findById(itemId)).thenReturn(Optional.of(expectedItem));

        mockMvc.perform(get("/items/" + itemId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(itemId));
    }

    @Test
    void itemNotFoundGets404() throws Exception {
        Integer itemId = 123;
        when(itemService.findById(itemId)).thenReturn(Optional.empty());

        mockMvc.perform(get("/items/" + itemId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserCanCreatesAnItemWhenItemIsValid() throws Exception {

        String itemName = "some item name";
        String itemDescription = "some item description";
        BigDecimal itemPrice = BigDecimal.valueOf(12.95);
        long itemQuantity = 4L;
        ItemType itemType = FRAME;

        String itemAsJson = "{ \"name\": \"" + itemName + "\", " +
                "\"description\": \"" + itemDescription + "\", " +
                "\"price\": " + itemPrice + ", " +
                "\"quantity\": " + itemQuantity + ", " +
                "\"type\": \"" + itemType + "\"" +
                "}";

        Item createdItem = new ItemBuilder()
                .withName(itemName)
                .withItemId(null)
                .withDescription(itemDescription)
                .withPrice(itemPrice)
                .withQuantity(itemQuantity)
                .withType(itemType)
                .build();

        when(itemService.save(createdItem)).thenReturn(92);

        mockMvc.perform(
                post("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, endsWith("/items/92")));

        verify(itemService).save(createdItem);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserAreNotAllowedToCreateItemsWhenItemIsInvalid() throws Exception {
        String itemAsJson = "{ \"name\": \"\", " +
                "\"description\": \"some item description\", " +
                "\"price\": -1, " +
                "\"quantity\": -1, " +
                "\"type\": \"FRAME\"" +
                "}";

        mockMvc.perform(
                post("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void usersAreNotAllowedToCreateItems() throws Exception {

        String itemAsJson = "{ \"name\": \"some item name\", " +
                "\"description\": \"some item description\", " +
                "\"price\": 12.95, " +
                "\"quantity\": 4, " +
                "\"type\": \"FRAME\"" +
                "}";

        mockMvc.perform(
                post("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserCouldUpdateItemsWhenItemsAreValid() throws Exception {

        String itemAsJson = "{" +
                "\"items\": [{" +
                "\"id\": 1," +
                "\"name\": \"some name\"," +
                "\"description\": \"some item description\"," +
                "\"price\": 1," +
                "\"quantity\": 1," +
                "\"type\": \"FRAME\"" +
                "}]" +
                "}";

        Item item = new ItemBuilder().withItemId(1)
                .withName("some name")
                .withDescription("some item description")
                .withPrice(BigDecimal.valueOf(1))
                .withQuantity(1L)
                .withType(FRAME).build();


        doNothing().when(itemService).saveAll(singletonList(item));


        mockMvc.perform(
                patch("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserAreNotAllowedToUpdateItemsWhenItemsAreInvalid() throws Exception {
        String itemAsJson = "{" +
                "\"items\": [{" +
                "\"name\": \"some name\"," +
                "\"description\": \"some item description\"," +
                "\"price\": 1," +
                "\"quantity\": 1," +
                "\"type\": \"FRAME\"" +
                "}]" +
                "}";

        mockMvc.perform(
                post("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void usersAreNotAllowedToUpdateItems() throws Exception {
        String itemAsJson = "{" +
                "\"items\": [{" +
                "\"itemId\": 1," +
                "\"name\": \"some name\"," +
                "\"description\": \"some item description\"," +
                "\"price\": 1," +
                "\"quantity\": 1," +
                "\"type\": \"FRAME\"" +
                "}]" +
                "}";

        mockMvc.perform(
                post("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserCouldDeleteItemsWhenItemsAreValid() throws Exception {

        String itemAsJson = "{" +
                "\"itemIds\": [1]" +
                "}";

        Item item = new ItemBuilder().withItemId(1)
                .withName("")
                .withDescription("")
                .withPrice(null)
                .withQuantity(null)
                .withType(null).build();

        doNothing().when(itemService).deleteAll(singletonList(item));


        mockMvc.perform(
                delete("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void adminUserAreNotAllowedToDeleteItemsWhenItemsAreInvalid() throws Exception {
        String itemAsJson = "{" +
                "\"itemIds\": []" +
                "}";

        mockMvc.perform(
                delete("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void usersAreNotAllowedToDeleteItems() throws Exception {
        String itemAsJson = "{" +
                "\"itemIds\": [1]" +
                "}";

        mockMvc.perform(
                delete("/admin/items")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemAsJson))
                .andExpect(status().isForbidden());
    }


}
