package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.model.*;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.model.builder.ItemBuilder;
import com.freewheelers.model.builder.OrderBuilder;
import com.freewheelers.service.OrderService;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.endsWith;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderResource.class)
class OrderResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    OrderService orderService;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @Test
    @WithMockUser(username = "someUser@example.com")
    void createsAnOrder() throws Exception {
        Integer itemId = 4;

        String orderRequest = "{ \"itemId\": " + itemId + " }";

        when(orderService.createForUser(itemId, "someUser@example.com", PaymentMethod.CASH))
                .thenReturn(new OrderBuilder().withOrderId(22)
                .build());

        mockMvc.perform(
                post("/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(orderRequest))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, endsWith("/orders/22")));
    }

    @Test
    @WithMockUser(username = "someUserWithAnOrder@example.com")
    void shouldRetrieveOrderForUser() throws Exception {

        int orderId = 32;
        int itemId = 45;
        String emailAddress = "someUserWithAnOrder@example.com";

        Date orderDate = new Date();
        Account userAccount = new AccountBuilder().withEmailAddress("someUserWithAnOrder@example.com").build();
        Order order = new OrderBuilder()
                .withAccount(userAccount)
                .withOrderId(orderId)
                .withReserveTime(orderDate)
                .withItem(new ItemBuilder().withItemId(itemId).build())
                .build();

        when(orderService.findById(orderId)).thenReturn(
                Optional.of(order)
        );

        mockMvc.perform(
                get("/orders/" + orderId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(orderId))
                .andExpect(jsonPath("$.date").value(orderDate.toString()))
                .andExpect(jsonPath("$.item.itemId").value(itemId))
                .andExpect(jsonPath("$.emailAddress").value(emailAddress));
    }

    @Test
    @WithMockUser(username = "someOtherUser@example.com")
    void shouldNotAllowAccessToOrderForDifferentUser() throws Exception {
        int orderId = 33;

        Account userAccount = new AccountBuilder().withEmailAddress("someUserWithAnOrder@example.com").build();
        when(orderService.findById(orderId)).thenReturn(
                Optional.of(new OrderBuilder().withAccount(userAccount).withOrderId(orderId).build())
        );

        mockMvc.perform(
                get("/orders/" + orderId))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "me@example.com")
    void itemNotFoundResultsIn404() throws Exception {
        Integer itemId = 1;
        String email = "me@example.com";
        String orderRequest = "{ \"itemId\": " + itemId + " }";

        when(orderService.createForUser(itemId, email, PaymentMethod.CASH)).thenThrow(new ItemNotFoundException());

        mockMvc.perform(post("/orders")
                .content(orderRequest)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "me@example.com")
    void orderNotFoundResultsGets404() throws Exception {
        Integer orderId = 1;

        when(orderService.findById(orderId)).thenReturn(Optional.empty());

        mockMvc.perform(get(String.format("/orders/%d", orderId)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "me@example.com")
    void shouldRetrieveAllOrdersForUser() throws Exception {
        int orderId = 32;
        int itemId = 45;
        String emailAddress = "me@example.com";
        Date orderDate = new Date();

        Account userAccount = new AccountBuilder().withEmailAddress(emailAddress).build();
        Item item = new ItemBuilder().withItemId(itemId).build();
        Order order = new OrderBuilder()
                .withAccount(userAccount)
                .withOrderId(orderId)
                .withReserveTime(orderDate)
                .withItem(item)
                .build();

        when(orderService.findAllByAccountEmail(emailAddress)).thenReturn(singletonList(order));

        mockMvc.perform(
                get("/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").isArray())
                .andExpect(jsonPath("$.orders.[0].id").value(orderId))
                .andExpect(jsonPath("$.orders.[0].date").value(orderDate.toString()))
                .andExpect(jsonPath("$.orders.[0].item.itemId").value(itemId))
                .andExpect(jsonPath("$.orders.[0].emailAddress").value(emailAddress));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldRetrieveAllCustomerOrders() throws Exception {
        int orderId1 = 32;
        int orderId2 = 45;

        Order order1 = new OrderBuilder().withOrderId(orderId1).build();
        Order order2 = new OrderBuilder().withOrderId(orderId2).build();

        when(orderService.findAll()).thenReturn(asList(order1, order2));

        mockMvc.perform(
                get("/admin/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").isArray())
                .andExpect(jsonPath("$.orders.[0].id").value(orderId1))
                .andExpect(jsonPath("$.orders.[1].id").value(orderId2));
    }

    @Test
    @WithMockUser(roles = "USER")
    void returns403WhenNonAdminUserTriesToGetAllCustomerOrders() throws Exception {
        mockMvc.perform(
                get("/admin/orders"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser()
    void shouldRetrieveSpecialCustomerOrders() throws Exception {
        int orderId1 = 32;
        int orderId2 = 45;

        Order order1 = new OrderBuilder().withOrderId(orderId1).build();
        Order order2 = new OrderBuilder().withOrderId(orderId2).build();

        when(orderService.findAllByAccountEmail("test@email.com")).thenReturn(asList(order1, order2));

        mockMvc.perform(
                get("/test@email.com/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders").isArray())
                .andExpect(jsonPath("$.orders.[0].id").value(orderId1))
                .andExpect(jsonPath("$.orders.[1].id").value(orderId2));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldAdminCouldUpdateCustomerOrder() throws Exception {
        String updateOrderRequest = "{ \"note\": \"test\", \"orderStatus\": \"NEW\" }";

        doNothing().when(orderService).updateDetails(32, OrderStatus.NEW, "test");

        mockMvc.perform(
                patch("/admin/orders/32")
                        .content(updateOrderRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldAdminUpdateCustomerOrderFailedWhenMissInfo() throws Exception {
        String updateOrderRequest = "{}";

        doNothing().when(orderService).updateDetails(32, OrderStatus.NEW, "test");

        mockMvc.perform(
                patch("/admin/orders/32")
                        .content(updateOrderRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void shouldAdminUpdateCustomerGets404WhenOrderNotFound() throws Exception {
        String updateOrderRequest = "{ \"note\": \"test\", \"orderStatus\": \"NEW\" }";

        doThrow(new OrderNotFoundException()).when(orderService).updateDetails(any(), any(), any());

        mockMvc.perform(
                patch("/admin/orders/32")
                        .content(updateOrderRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "USER")
    void returns403WhenNonAdminUserTriesToUpdateCustomerOrder() throws Exception {
        String updateOrderRequest = "{ \"note\": \"test\", \"orderStatus\": \"NEW\" }";

        mockMvc.perform(
                patch("/admin/orders/32")
                        .content(updateOrderRequest)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
