package com.freewheelers.controller;

import com.freewheelers.configuration.UserAccountDetailsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PrincipalController.class)
class PrincipalControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @Test
    @WithMockUser(username = "user@example.com", authorities = "ROLE_USER")
    void getPrincipal() throws Exception {
        mockMvc.perform(get("/principal"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.username").value("user@example.com"))
            .andExpect(jsonPath("$.role").value("ROLE_USER"));
    }
}