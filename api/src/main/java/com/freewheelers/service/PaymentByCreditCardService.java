package com.freewheelers.service;

import com.freewheelers.client.CreditCardPaymentAPIClient;
import com.freewheelers.client.dto.CreditCardPaymentAPIRequest;
import com.freewheelers.client.dto.CreditCardPaymentAPIResponse;
import com.freewheelers.model.CreditCardPaymentDetails;
import com.freewheelers.model.Order;
import com.freewheelers.model.OrderStatus;
import com.freewheelers.model.PaymentMethod;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.CreditCardPaymentDeclinedException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class PaymentByCreditCardService {
    private final OrderService orderService;
    private final CreditCardPaymentAPIClient creditCardPaymentAPIClient;

    public PaymentByCreditCardService(OrderService orderService, CreditCardPaymentAPIClient creditCardPaymentAPIClient) {
        this.orderService = orderService;
        this.creditCardPaymentAPIClient = creditCardPaymentAPIClient;
    }

    @Transactional(rollbackFor = CreditCardPaymentDeclinedException.class)
    public int submitPaymentForUser(CreditCardPaymentDetails creditCardPaymentDetails, String accountEmail)
            throws ItemNotFoundException, CreditCardPaymentDeclinedException, AccountNotFoundException, OrderNotFoundException {
        
        Order newOrder = orderService.createForUser(creditCardPaymentDetails.getItemId(), accountEmail, PaymentMethod.CREDIT_CARD);
        submitPayment(creditCardPaymentDetails, newOrder.getTotalPrice());
        orderService.updateStatus(newOrder.getId(), OrderStatus.PAID);

        return newOrder.getId();
    }
    
    private void submitPayment(CreditCardPaymentDetails creditCardPaymentDetails, BigDecimal price) throws CreditCardPaymentDeclinedException {
        CreditCardPaymentAPIResponse creditCardPaymentAPIResponse = creditCardPaymentAPIClient.pay(
                new CreditCardPaymentAPIRequest(creditCardPaymentDetails, price)
        );
        
        if (!creditCardPaymentAPIResponse.getStatus().equals("SUCCESS")) {
            throw new CreditCardPaymentDeclinedException();
        }
    }
}
