package com.freewheelers.service;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.AccountRoleMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.AccountRole;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {
    private static final String USER = "ROLE_USER";

    private final AccountRoleMapper accountRoleMapper;
    private final AccountMapper accountMapper;
    private final FeatureToggleService featureToggleService;
    
    public AccountService(AccountMapper accountMapper, AccountRoleMapper accountRoleMapper, FeatureToggleService featureToggleService) {
        this.accountMapper = accountMapper;
        this.accountRoleMapper = accountRoleMapper;
        this.featureToggleService = featureToggleService;
    }

    public Integer createUser(Account account) throws DataAccessException {
        if(!featureToggleService.isEnabled("address")) {
            account.setCity("");
            account.setZipcode("");
            account.setStreet("");
            account.setStreetNumber("");
            account.setUnitNumber("");
        }

        if(!featureToggleService.isEnabled("termsAndConditions")) {
            account.setTermsAndConditions(false);
        }

        accountMapper.insert(account);
        accountRoleMapper.insert(new AccountRole(account.getId(), AccountService.USER));
        return account.getId();
    }

    public Optional<Account> findByEmailAddress(String emailAddress) {
        return accountMapper.findByEmailAddress(emailAddress);
    }
}
