package com.freewheelers.service;

import com.freewheelers.mapper.ItemMapper;
import com.freewheelers.model.Item;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    private final ItemMapper itemMapper;

    public ItemService(ItemMapper itemMapper) {
        this.itemMapper = itemMapper;
    }

    public Optional<Item> findById(Integer itemId) {
        return itemMapper.findById(itemId);
    }

    public void delete(Item item) {
        itemMapper.delete(item);
    }

    public List<Item> findAll() {
        return itemMapper.findAll();
    }

    public List<Item> findAllAvailable() {
        return itemMapper.findAllAvailable();
    }

    public void saveAll(List<Item> items) {
        for (Item item : items) {
            insertOrUpdate(item);
        }
    }

    public void deleteAll(List<Item> items) {
        for (Item item : items) {
            delete(item);
        }
    }

    public void decreaseQuantityByOne(Item item) {
        item.setQuantity(item.getQuantity() - 1);
        itemMapper.update(item);
    }

    public Integer save(Item item) {
        insertOrUpdate(item);
        return item.getId();
    }

    private void insertOrUpdate(Item item) {
        if (item.getId() == null) {
            itemMapper.insert(item);
        } else {
            itemMapper.update(item);
        }
    }
}
