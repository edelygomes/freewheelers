package com.freewheelers.service;

import com.freewheelers.mapper.OrderMapper;
import com.freewheelers.model.*;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    private final OrderMapper orderMapper;
    private final ItemService itemService;
    private final AccountService accountService;
    private final CalculationPricesService calculationPricesService;

    public OrderService(OrderMapper orderMapper, ItemService itemService, AccountService accountService, CalculationPricesService calculationPricesService) {
        this.orderMapper = orderMapper;
        this.itemService = itemService;
        this.accountService = accountService;
        this.calculationPricesService = calculationPricesService;
    }

    public void save(Order order) {
        if (order.getId() == null) {
            orderMapper.insert(order);
        } else {
            orderMapper.save(order);
        }
    }

    public void updateDetails(Integer orderId, OrderStatus status, String note) throws OrderNotFoundException {
        Order order = orderMapper.findById(orderId).orElseThrow(OrderNotFoundException::new);

        order.setStatus(status);
        order.setNote(note);

        orderMapper.save(order);
    }

    public Optional<Order> findById(Integer id) {
        return orderMapper.findById(id);
    }

    public Order createForUser(Integer itemId, String accountEmail, PaymentMethod paymentMethod) throws ItemNotFoundException, AccountNotFoundException {
        Account account = accountService.findByEmailAddress(accountEmail)
                .orElseThrow(AccountNotFoundException::new);
        Item item = itemService.findById(itemId)
                .orElseThrow(ItemNotFoundException::new);

        if (!item.isAvailable()) {
            throw new ItemNotFoundException();
        }

        BigDecimal totalPrice = calculationPricesService.calculateFinalPrice(item);
        Order newOrder = new Order(account, item, new Date(), totalPrice, paymentMethod);
        itemService.decreaseQuantityByOne(item);
        orderMapper.insert(newOrder);
        return newOrder;
    }

    public List<Order> findAllByAccountEmail(String accountEmail) throws AccountNotFoundException {
        Account account = accountService.findByEmailAddress(accountEmail)
                .orElseThrow(AccountNotFoundException::new);
        return orderMapper.findAllByAccountId(account.getId());
    }

    public List<Order> findAll() {
        return orderMapper.findAll();
    }
    
    public void updateStatus(Integer orderId, OrderStatus status) throws OrderNotFoundException {
        Order order = orderMapper.findById(orderId).orElseThrow(OrderNotFoundException::new);
    
        order.setStatus(status);
    
        orderMapper.save(order);
    }
}
