package com.freewheelers.service;

import com.freewheelers.model.Item;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CalculationPricesService {

    public BigDecimal calculateFinalPrice(Item item) {
        BigDecimal price = item.getPrice();

        BigDecimal vatPercent = this.getVATPercent();

        BigDecimal finalPrice = price.divide(BigDecimal.valueOf(1).subtract(vatPercent));

        return finalPrice.setScale(2, RoundingMode.HALF_DOWN);
    }

    private BigDecimal getVATPercent(){
                return BigDecimal.valueOf(0.2);
    }
}
