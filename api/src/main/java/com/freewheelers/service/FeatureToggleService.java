package com.freewheelers.service;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class FeatureToggleService {
    private final Map<String, Boolean> featureTogglesByName;

    public FeatureToggleService(Map<String, Boolean> featureToggles) {
        this.featureTogglesByName = featureToggles;
    }

    public Map<String, Boolean> findAll() {
        return this.featureTogglesByName;
    }

    public void update(String name, Boolean value) {
        this.featureTogglesByName.put(name, value);
    }

    public boolean isEnabled(String featureName) {
        if (this.featureTogglesByName.containsKey(featureName)) {
            return this.featureTogglesByName.get(featureName);
        }
        return false;
    }
}
