package com.freewheelers.controller;

import com.freewheelers.controller.dto.CreateItemRequest;
import com.freewheelers.controller.dto.DeleteItemsRequest;
import com.freewheelers.controller.dto.ItemsResponse;
import com.freewheelers.controller.dto.UpdateItemsRequest;
import com.freewheelers.model.Item;
import com.freewheelers.service.ItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
public class ItemResource {

    private final ItemService itemService;

    public ItemResource(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/items")
    public ResponseEntity<ItemsResponse> getAvailableItems() {
        List<Item> items = itemService.findAllAvailable();
        ItemsResponse itemsResponse = new ItemsResponse(items);

        return ResponseEntity.ok(itemsResponse);
    }

    @PostMapping("/admin/items")
    public ResponseEntity<Void> createItem(UriComponentsBuilder uriComponentsBuilder, @Valid @RequestBody CreateItemRequest createItemRequest) {
        Item item = createItemRequest.toItem();
        Integer newItemId = itemService.save(item);

        UriComponents uriComponent = uriComponentsBuilder.path(String.format("/items/%d", newItemId)).build();
        URI uri = uriComponent.toUri();

        return ResponseEntity.created(uri).build();
    }

    @PatchMapping("/admin/items")
    public ResponseEntity<Void> updateItems(@Valid @RequestBody UpdateItemsRequest updateItemsRequest) {
        List<Item> items = updateItemsRequest.toItems();
        itemService.saveAll(items);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/items/{id}")
    public ResponseEntity<Item> getItem(@PathVariable Integer id) {
        return itemService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/admin/items")
    public ResponseEntity<Void> deleteItem(@Valid @RequestBody DeleteItemsRequest deleteItemsRequest) {
        List<Item> items = deleteItemsRequest.toItems();
        itemService.deleteAll(items);

        return ResponseEntity.noContent().build();
    }
}
