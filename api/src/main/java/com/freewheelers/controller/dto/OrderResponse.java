package com.freewheelers.controller.dto;

import com.freewheelers.model.Order;
import com.freewheelers.model.OrderStatus;
import com.freewheelers.model.OrderedItem;

import java.math.BigDecimal;

public class OrderResponse {
    private Integer id;
    private String date;
    private OrderedItem item;
    private String emailAddress;
    private String note;
    private OrderStatus orderStatus;
    private BigDecimal totalPrice;

    public OrderResponse(Order order) {
        this.id = order.getId();
        this.item = order.getItem();
        this.emailAddress = order.getAccount().getEmailAddress();
        this.date = order.getReserveTime().toString();
        this.note = order.getNote();
        this.orderStatus = order.getStatus();
        this.totalPrice = order.getTotalPrice();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OrderedItem getItem() {
        return item;
    }

    public void setItem(OrderedItem item) {
        this.item = item;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public BigDecimal getTotalPrice(){ return totalPrice; }
}
