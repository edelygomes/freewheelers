package com.freewheelers.controller.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class PrincipalResponse {
    private final String username;

    private final String role;

    public PrincipalResponse(UserDetails userDetails) {
        this.username = userDetails.getUsername();
        this.role = userDetails.getAuthorities().stream()
                .findFirst()
                .map(GrantedAuthority::getAuthority)
                .orElse(null);
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }
}
