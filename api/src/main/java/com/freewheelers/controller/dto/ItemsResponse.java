package com.freewheelers.controller.dto;

import com.freewheelers.model.Item;

import java.util.List;

public class ItemsResponse {
    private final List<Item> items;

    public ItemsResponse(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }
}
