package com.freewheelers.controller.dto;

import com.freewheelers.model.OrderStatus;

import javax.validation.constraints.NotNull;

public class UpdateOrderRequest {

    @NotNull
    private String note;

    @NotNull
    private OrderStatus orderStatus;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}
