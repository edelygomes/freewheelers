package com.freewheelers.controller.dto;

import com.freewheelers.model.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrdersResponse {
    private final List<OrderResponse> orders;

    public OrdersResponse(List<Order> orders) {
        this.orders = orders.stream()
                .map(OrderResponse::new)
                .collect(Collectors.toList());
    }

    public List<OrderResponse> getOrders() {
        return orders;
    }
}
