package com.freewheelers.controller.dto;

import com.freewheelers.model.Item;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public class DeleteItemsRequest {

    @NotEmpty
    private List<@NotNull Integer> itemIds;

    @JsonCreator
    public DeleteItemsRequest(@JsonProperty("itemIds") List<Integer> itemIds) {
        this.itemIds = itemIds;
    }

    public List<Item> toItems() {
        return itemIds.stream().map(itemId -> {
            Item item = new Item();
            item.setId(itemId);
            return item;
        }).collect(Collectors.toList());
    }
}
