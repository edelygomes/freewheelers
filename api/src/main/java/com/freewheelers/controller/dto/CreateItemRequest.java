package com.freewheelers.controller.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.freewheelers.model.Item;
import com.freewheelers.model.ItemType;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class CreateItemRequest {
    @NotBlank
    private final String name;

    @NotNull
    @Positive
    private final BigDecimal price;

    @NotBlank
    private final String description;

    @NotNull
    private final ItemType type;

    @NotNull
    @Min(value = 1L)
    private final Long quantity;

    @JsonCreator
    public CreateItemRequest(
            @JsonProperty("name") String name,
            @JsonProperty("price") BigDecimal price,
            @JsonProperty("description") String description,
            @JsonProperty("type") ItemType type,
            @JsonProperty("quantity") Long quantity) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.type = type;
        this.quantity = quantity;
    }

    public Item toItem() {
        Item item = new Item();
        item.setName(this.name);
        item.setPrice(this.price);
        item.setDescription(this.description);
        item.setType(this.type);
        item.setQuantity(this.quantity);
        return item;
    }
}
