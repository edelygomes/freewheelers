package com.freewheelers.controller.dto;

import com.freewheelers.model.Account;

public class AccountResponse {

    private final String city;
    private final String zipcode;
    private final String street;
    private final String streetNumber;
    private final String unitNumber;
    private final String country;
    private String accountName;
    private String emailAddress;
    private String phoneNumber;


    public AccountResponse(Account account) {
        this.accountName = account.getAccountName();
        this.emailAddress = account.getEmailAddress();
        this.phoneNumber = account.getPhoneNumber();
        this.country = account.getCountry();
        this.city = account.getCity();
        this.zipcode = account.getZipcode();
        this.street = account.getStreet();
        this.streetNumber = account.getStreetNumber();
        this.unitNumber = account.getUnitNumber();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }
    public String getCity() {
        return city;
    }

    public  String getZipcode() {
        return zipcode;
    }

    public String getStreet(){
        return  street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getUnitNumber(){
        return unitNumber;
    }


}
