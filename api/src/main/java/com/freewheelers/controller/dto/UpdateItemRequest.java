package com.freewheelers.controller.dto;

import com.freewheelers.model.Item;
import com.freewheelers.model.ItemType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

public class UpdateItemRequest extends CreateItemRequest {
    @NotNull
    @Positive
    private final Integer id;

    @JsonCreator
    public UpdateItemRequest(
            @JsonProperty("name") String name,
            @JsonProperty("price") BigDecimal price,
            @JsonProperty("description") String description,
            @JsonProperty("type") ItemType type,
            @JsonProperty("quantity") Long quantity,
            @JsonProperty("id") Integer id) {

        super(name, price, description, type, quantity);
        this.id = id;
    }

    @Override
    public Item toItem() {
        Item item = super.toItem();
        item.setId(id);
        return item;
    }
}
