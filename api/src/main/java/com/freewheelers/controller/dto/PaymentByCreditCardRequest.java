package com.freewheelers.controller.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.freewheelers.model.CreditCardPaymentDetails;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class PaymentByCreditCardRequest {
    @Pattern(regexp = "^([0-9]{15}|[0-9]{16})$", message = "invalid credit card number")
    private final String creditCardNumber;

    @Pattern(regexp = "^([0-9]{3}|[0-9]{4})$", message = "invalid csc number")
    private final String csc;

    @Pattern(regexp = "^[0-9]{2}-[0-9]{4}$", message = "invalid expire date")
    private final String expireDate;
    
    @NotNull(message = "null item ID")
    private final Integer itemId;
    
    @JsonCreator
    public PaymentByCreditCardRequest(
            @JsonProperty("creditCardNumber") String creditCardNumber,
            @JsonProperty("csc") String csc,
            @JsonProperty("expireDate") String expireDate,
            @JsonProperty("itemId") Integer itemId)
    {
        this.creditCardNumber = creditCardNumber;
        this.csc = csc;
        this.expireDate = expireDate;
        this.itemId = itemId;
    }

    public CreditCardPaymentDetails toCreditCardPaymentDetails() {
        return new CreditCardPaymentDetails(creditCardNumber, csc, expireDate, itemId);
    }
}