package com.freewheelers.controller.dto;

import com.freewheelers.model.Item;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.stream.Collectors;


public class UpdateItemsRequest {

    @Valid
    @NotEmpty
    private List<UpdateItemRequest> items;

    public List<Item> toItems() {
        return items.stream().map(UpdateItemRequest::toItem).collect(Collectors.toList());
    }

    public void setItems(List<UpdateItemRequest> updateItemRequests) {
        this.items = updateItemRequests;
    }
}
