package com.freewheelers.controller.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.freewheelers.model.Account;

import javax.validation.constraints.*;

public class CreateAccountRequest {
    @NotBlank
    @Email
    private final String emailAddress;

    @NotBlank
    private final String accountName;

    @Size(min = 8, message = "must be at least 8 characters long")
    @Pattern(regexp = ".*[a-z].*", message = "must have at least one lower case letter")
    @Pattern(regexp = ".*[A-Z].*", message = "must have at least one upper case letter")
    @Pattern(regexp = ".*[\\d].*", message = "must have at least one number")
    private final String password;

    @Pattern(regexp = "^[0-9]+$", message = "must enter a valid phone number containing only numbers")
    private final String phoneNumber;

    @NotBlank
    private final String country;

    private final String city;

    private final String zipcode;

    private  final String street;
    
    private final String streetNumber;

    private final String unitNumber;

    private boolean termsAndConditions;

    @JsonCreator
    public CreateAccountRequest(
            @JsonProperty("emailAddress") String emailAddress,
            @JsonProperty("accountName") String accountName,
            @JsonProperty("password") String password,
            @JsonProperty("phoneNumber") String phoneNumber,
            @JsonProperty("country") String country,
            @JsonProperty("city") String city,
            @JsonProperty("zipcode") String zipcode,
            @JsonProperty("street") String street,
            @JsonProperty("streetNumber") String streetNumber,
            @JsonProperty("unitNumber") String unitNumber,
            @JsonProperty("termsAndConditions") boolean termsAndConditions)
    {
        this.emailAddress = emailAddress;
        this.accountName = accountName;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.zipcode = zipcode;
        this.street = street;
        this.streetNumber = streetNumber;
        this.unitNumber = unitNumber;
        this.termsAndConditions = termsAndConditions;
    }
   
    public Account toAccount() {
        Account account = new Account();
        account.setEmailAddress(this.emailAddress);
        account.setAccountName(this.accountName);
        account.setPassword(this.password);
        account.setPhoneNumber(this.phoneNumber);
        account.setCountry(this.country);
        account.setCity(this.city);
        account.setZipcode(this.zipcode);
        account.setStreet(this.street);
        account.setStreetNumber(this.streetNumber);
        account.setUnitNumber(this.unitNumber);
        account.setTermsAndConditions(this.termsAndConditions);
        return account;
    }
}
