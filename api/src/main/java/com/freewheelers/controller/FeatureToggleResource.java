package com.freewheelers.controller;

import com.freewheelers.service.FeatureToggleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/toggles")
public class FeatureToggleResource {
    private final FeatureToggleService featureToggleService;

    public FeatureToggleResource(FeatureToggleService featureToggleService) {
        this.featureToggleService = featureToggleService;
    }

    @GetMapping
    public ResponseEntity<Map<String, Boolean>> getAllFeatureToggles() {
        return ResponseEntity.ok(featureToggleService.findAll());
    }

    @PutMapping("/{name}")
    public ResponseEntity<Void> updateFeatureToggle(@PathVariable String name, @RequestBody Boolean value) {
        featureToggleService.update(name, value);
        return ResponseEntity.noContent().build();
    }
}
