package com.freewheelers.controller;

import com.freewheelers.controller.dto.PaymentByCreditCardRequest;
import com.freewheelers.model.CreditCardPaymentDetails;
import com.freewheelers.service.FeatureToggleService;
import com.freewheelers.service.PaymentByCreditCardService;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.CreditCardPaymentDeclinedException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/paymentCreditCard")
public class PaymentCreditCardResource {
    private final FeatureToggleService featureToggleService;
    private final PaymentByCreditCardService paymentByCreditCardService;

    public PaymentCreditCardResource(FeatureToggleService featureToggleService, PaymentByCreditCardService paymentByCreditCardService) {
        this.featureToggleService = featureToggleService;
        this.paymentByCreditCardService = paymentByCreditCardService;
    }

    @PostMapping
    public ResponseEntity<Void> submitCreditCardPayment(@Valid @RequestBody  PaymentByCreditCardRequest paymentByCreditCardRequest, Principal principal) {
        if(!featureToggleService.isEnabled("payment")) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build();
        }

        CreditCardPaymentDetails creditCardPaymentDetails = paymentByCreditCardRequest.toCreditCardPaymentDetails();

        try {
            paymentByCreditCardService.submitPaymentForUser(creditCardPaymentDetails, principal.getName());
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (ItemNotFoundException | CreditCardPaymentDeclinedException | AccountNotFoundException | OrderNotFoundException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
