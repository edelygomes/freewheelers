package com.freewheelers.controller;

import com.freewheelers.controller.dto.AccountResponse;
import com.freewheelers.controller.dto.CreateAccountRequest;
import com.freewheelers.model.Account;
import com.freewheelers.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/accounts")
public class AccountResource {
    private final AccountService accountService;

    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<Void> createAccount(@Valid @RequestBody CreateAccountRequest createAccountRequest) {
        Account validAccount = createAccountRequest.toAccount();
        accountService.createUser(validAccount);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/{email}")
    public ResponseEntity<AccountResponse> getAccount(@PathVariable String email) {
        return accountService.findByEmailAddress(email)
                .map(AccountResponse::new)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
