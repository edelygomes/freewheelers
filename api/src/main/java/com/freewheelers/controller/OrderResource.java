package com.freewheelers.controller;

import com.freewheelers.controller.dto.OrderRequest;
import com.freewheelers.controller.dto.OrderResponse;
import com.freewheelers.controller.dto.OrdersResponse;
import com.freewheelers.controller.dto.UpdateOrderRequest;
import com.freewheelers.model.Order;
import com.freewheelers.model.PaymentMethod;
import com.freewheelers.service.OrderService;
import com.freewheelers.service.exception.AccountNotFoundException;
import com.freewheelers.service.exception.ItemNotFoundException;
import com.freewheelers.service.exception.OrderNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

@Controller
public class OrderResource {
    private final OrderService orderService;

    public OrderResource(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/orders")
    public ResponseEntity<OrdersResponse> getOrders(Principal principal) {
        try {
            List<Order> orders = orderService.findAllByAccountEmail(principal.getName());
            return ResponseEntity.ok(new OrdersResponse(orders));
        } catch (AccountNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/orders")
    public ResponseEntity<Void> createOrderForItem(@RequestBody OrderRequest order, Principal principal, UriComponentsBuilder uriComponentsBuilder) {
        try {
            Order newOrder = orderService.createForUser(order.getItemId(), principal.getName(), PaymentMethod.CASH);
            URI uri = uriComponentsBuilder.path(String.format("/orders/%d", newOrder.getId())).build().toUri();
            return ResponseEntity.created(uri).build();
        } catch (AccountNotFoundException | ItemNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/orders/{id}")
    @PostAuthorize("returnObject.body == null || returnObject.body.emailAddress == principal.username")
    public ResponseEntity<OrderResponse> getOrderById(@PathVariable Integer id) {
        return orderService.findById(id)
                .map(OrderResponse::new)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/admin/orders")
    public ResponseEntity<OrdersResponse> getAllCustomerOrders() {
        List<Order> orders = orderService.findAll();
        return ResponseEntity.ok(new OrdersResponse(orders));
    }

    @GetMapping(value = "/{email}/orders")
    public ResponseEntity<OrdersResponse> getSpecialCustomerOrders(@PathVariable String email) {
        try {
            List<Order> orders = orderService.findAllByAccountEmail(email);
            return ResponseEntity.ok(new OrdersResponse(orders));
        } catch (AccountNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping("/admin/orders/{id}")
    public ResponseEntity<Void> getOrderById(@PathVariable Integer id, @Valid @RequestBody UpdateOrderRequest updateOrderRequest) {
        try {
            orderService.updateDetails(id, updateOrderRequest.getOrderStatus(), updateOrderRequest.getNote());
            return ResponseEntity.noContent().build();
        } catch (OrderNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
