package com.freewheelers.controller;

import com.freewheelers.controller.dto.PrincipalResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrincipalController {
    @GetMapping("/principal")
    public PrincipalResponse getPrincipal(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return new PrincipalResponse(userDetails);
    }
}
