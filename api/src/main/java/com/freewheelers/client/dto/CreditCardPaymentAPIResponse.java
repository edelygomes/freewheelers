package com.freewheelers.client.dto;

public class CreditCardPaymentAPIResponse {
    private final String status;
    private final String id;

    public CreditCardPaymentAPIResponse(String status, String id) {
        this.status = status;
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }
}
