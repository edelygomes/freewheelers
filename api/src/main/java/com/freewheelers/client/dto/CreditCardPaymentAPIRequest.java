package com.freewheelers.client.dto;

import com.freewheelers.model.CreditCardPaymentDetails;

import java.math.BigDecimal;

public class CreditCardPaymentAPIRequest {
		private final String cardNumber;
		private final String csc;
		private final BigDecimal amount;
		private final String expiry;
		
		public CreditCardPaymentAPIRequest(CreditCardPaymentDetails creditCardPaymentDetails, BigDecimal amount) {
				this.cardNumber = creditCardPaymentDetails.getCardNumber();
				this.csc = creditCardPaymentDetails.getCsc();
				this.amount = amount;
				this.expiry = creditCardPaymentDetails.getExpiry();
		}
		
		public String getCardNumber() {
				return cardNumber;
		}
		
		public String getCsc() {
				return csc;
		}
		
		public BigDecimal getAmount() {
				return amount;
		}
		
		public String getExpiry() {
				return expiry;
		}
}
