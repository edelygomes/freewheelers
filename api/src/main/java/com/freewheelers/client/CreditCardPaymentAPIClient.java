package com.freewheelers.client;

import com.freewheelers.client.dto.CreditCardPaymentAPIRequest;
import com.freewheelers.client.dto.CreditCardPaymentAPIResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CreditCardPaymentAPIClient {
		private final RestTemplate restTemplate;
		private final String url;
		
		public CreditCardPaymentAPIClient(RestTemplate restTemplate, @Value("${paymentApi.url}") String url) {
				this.restTemplate = restTemplate;
				this.url = url;
		}

		public CreditCardPaymentAPIResponse pay(CreditCardPaymentAPIRequest creditCardPaymentAPIRequest) {
				return restTemplate.postForObject(url + "/authorise", creditCardPaymentAPIRequest, CreditCardPaymentAPIResponse.class);
		}
}
