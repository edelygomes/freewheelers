package com.freewheelers.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.lang.Boolean.parseBoolean;
import static java.util.function.Function.identity;

@Configuration
public class FeatureToggleConfiguration {
    private static final String ENV_KEY = "ENV";
    private static final String LOCAL_ENV = "local";

    @Bean
    public Map<String, Boolean> featureToggles() throws IOException {
        Properties properties = readFeaturesProperties();
        return properties.stringPropertyNames().stream()
                .collect(Collectors.toMap(identity(), property -> parseBoolean(properties.getProperty(property))));
    }

    private Properties readFeaturesProperties() throws IOException {
        String featuresFilePath = String.format("features/features-%s.properties", getActiveEnvironment());
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(featuresFilePath));
        return properties;
    }

    private String getActiveEnvironment() {
        String activeProfile = System.getenv(ENV_KEY);
        return StringUtils.isEmpty(activeProfile) ? LOCAL_ENV : activeProfile;
    }
}
