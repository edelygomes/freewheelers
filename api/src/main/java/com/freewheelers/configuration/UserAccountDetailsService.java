package com.freewheelers.configuration;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.AccountRoleMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.AccountRole;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class UserAccountDetailsService implements UserDetailsService {
    private final AccountMapper accountMapper;
    private final AccountRoleMapper accountRoleMapper;

    public UserAccountDetailsService(AccountMapper accountMapper, AccountRoleMapper accountRoleMapper) {
        this.accountMapper = accountMapper;
        this.accountRoleMapper = accountRoleMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = this.accountMapper.findByEmailAddress(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        List<SimpleGrantedAuthority> authorities = getAuthorities(account);

        return createUserDetails(account, authorities);
    }

    private UserDetails createUserDetails(Account account, List<SimpleGrantedAuthority> authorities) {
        return new User(
                account.getEmailAddress(),
                account.getPassword(),
                true,
                true,
                true,
                true,
                authorities
        );
    }

    private List<SimpleGrantedAuthority> getAuthorities(Account account) {
        return accountRoleMapper.findByAccountId(account.getId())
                .map(this::createSimpleGrantedAuthority)
                .map(Collections::singletonList)
                .orElseGet(Collections::emptyList);
    }

    private SimpleGrantedAuthority createSimpleGrantedAuthority(AccountRole accountRole) {
        return new SimpleGrantedAuthority(accountRole.getRole());
    }
}
