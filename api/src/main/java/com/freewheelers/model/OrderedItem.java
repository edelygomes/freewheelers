package com.freewheelers.model;

import java.math.BigDecimal;

public class OrderedItem {
    private Integer itemId;
    private String name;
    private BigDecimal price;
    private String description;
    private ItemType type;

    private OrderedItem() {
    }

    public OrderedItem(Item item) {
        this.itemId = item.getId();
        this.name = item.getName();
        this.price = item.getPrice();
        this.description = item.getDescription();
        this.type = item.getType();
    }

    public Integer getItemId() {
        return itemId;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public ItemType getType() {
        return type;
    }
}
