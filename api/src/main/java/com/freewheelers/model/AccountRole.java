package com.freewheelers.model;

public class AccountRole{

    private Integer id;
    private Integer accountId;
    private String role;

    public AccountRole(Integer accountId, String role) {
        this.accountId = accountId;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
