package com.freewheelers.model;

import java.util.Objects;

public class Account{

    private Integer id;
    private String accountName;
    private String password;
    private String emailAddress;
    private String phoneNumber;
    private String country;
    private String city;
    private String zipcode;
    private String street;
    private String streetNumber;
    private String unitNumber;
    private boolean termsAndConditions;

    public Account(String accountName, String password, String emailAddress, String phoneNumber, String country, String city, String zipcode, String street, String streetNumber, String unitNumber, boolean termsAndConditions) {
        this.accountName = accountName;
        this.password = password;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.zipcode = zipcode;
        this.street = street;
        this.streetNumber = streetNumber;
        this.unitNumber = unitNumber;
        this.termsAndConditions = termsAndConditions;
    }

    public Account(Integer id, String accountName, String password, String emailAddress, String phoneNumber, String country, String city, String zipcode, String street, String streetNumber, String unitNumber, boolean termsAndConditions) {
        this(accountName, password, emailAddress, phoneNumber, country, city, zipcode, street, streetNumber, unitNumber, termsAndConditions);
        this.id = id;
    }


    public Account() {

    }

    public Integer getId() {
        return id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }

    public void setZipcode(String zipcode) { this.zipcode = zipcode; }

    public String getZipcode() { return this.zipcode; }

    public String getStreet() { return this.street;}
    
    public void setStreet(String street) {this.street = street;}


    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getUnitNumber() { return unitNumber; }

    public void setUnitNumber(String unitNumber) { this.unitNumber = unitNumber; }

    public boolean getTermsAndConditions() {
        return this.termsAndConditions;
    }

    public void setTermsAndConditions(boolean termsAndConditions){
        this.termsAndConditions = termsAndConditions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(accountName, account.accountName) &&
                Objects.equals(password, account.password) &&
                Objects.equals(emailAddress, account.emailAddress) &&
                Objects.equals(phoneNumber, account.phoneNumber);
    }



    @Override
    public int hashCode() {
        return Objects.hash(id, accountName, password, emailAddress, phoneNumber);
    }


}
