package com.freewheelers.model;

public enum PaymentMethod {
    CREDIT_CARD,
    CASH
}
