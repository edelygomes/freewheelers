package com.freewheelers.model;

public enum ItemType {
    FRAME,
    ACCESSORIES
}
