package com.freewheelers.model;

public class CreditCardPaymentDetails {
    private final String cardNumber;
    private final String csc;
    private final String expiry;
    private final Integer itemId;

    public CreditCardPaymentDetails(String cardNumber, String csc, String expiry, Integer itemId) {
        this.cardNumber = cardNumber;
        this.csc = csc;
        this.expiry = expiry;
        this.itemId = itemId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCsc() {
        return csc;
    }

    public String getExpiry() {
        return expiry;
    }
    
    public Integer getItemId() {
        return itemId;
    }
}
