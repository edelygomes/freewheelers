package com.freewheelers.model;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
    private Integer id;
    private Account account;
    private OrderedItem item;
    private final Date reserveTime;
    private OrderStatus status = OrderStatus.NEW;
    private String note = "";
    private BigDecimal totalPrice;
    private PaymentMethod paymentMethod;

    public Order() {
        this.reserveTime = new Date();
    }

    public Order(Account account, Item item, Date reserveTime, BigDecimal totalPrice) {
        this.account = account;
        this.item = new OrderedItem(item);
        this.reserveTime = new Date(reserveTime.getTime());
        this.totalPrice = totalPrice;
    }

    public Order(Account account, Item item, Date reserveTime, BigDecimal totalPrice, PaymentMethod paymentMethod) {
        this.account = account;
        this.item = new OrderedItem(item);
        this.reserveTime = new Date(reserveTime.getTime());
        this.totalPrice = totalPrice;
        this.paymentMethod = paymentMethod;
    }

    public OrderedItem getItem() {
        return item;
    }

    public void setItem(OrderedItem item) {
        this.item = item;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public final Date getReserveTime() {
        return new Date(reserveTime.getTime());
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public OrderStatus[] getStatusOptions() {
        return OrderStatus.values();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return this.paymentMethod.name();
    }
}
