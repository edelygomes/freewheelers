package com.freewheelers.mapper;

import com.freewheelers.model.Account;
import org.apache.ibatis.annotations.*;

import java.util.Optional;

public interface AccountMapper {
    String ACCOUNT_RESULTS_MAP = "accountResultsMap";

    @Insert(
        "INSERT INTO account (account_name, email_address, password, phone_number, country, city, zipcode, street, street_number, unit_number, terms_and_conditions) " +
        "VALUES (#{accountName}, #{emailAddress}, #{password}, #{phoneNumber}, #{country}, #{city}, #{zipcode}, #{street}, #{streetNumber}, #{unitNumber} , #{termsAndConditions})"
    )
    @Options(keyProperty = "id", useGeneratedKeys = true)
    void insert(Account account);

    @Select(
        "SELECT account_id, account_name, password, email_address, phone_number, country, city, zipcode, street, street_number,unit_number, terms_and_conditions " +
        "FROM account " +
        "WHERE account_id = #{id}"
    )
    @Results(id = ACCOUNT_RESULTS_MAP, value = {
            @Result(property= "id", column="account_id"),
            @Result(property= "accountName", column="account_name"),
            @Result(property= "emailAddress", column="email_address"),
            @Result(property= "phoneNumber", column="phone_number"),
            @Result(property= "country", column="country"),
            @Result(property= "city", column="city"),
            @Result(property = "zipcode", column = "zipcode"),
            @Result(property = "street", column = "street"),
            @Result(property = "streetNumber", column = "street_number"),
            @Result(property = "unitNumber", column = "unit_number"),
            @Result(property= "termsAndConditions", column= "terms_and_conditions")
    })
    Optional<Account> findById(Integer id);

    @Select(
            "SELECT account_id, account_name, password, email_address, phone_number, country, city, zipcode, street, street_number,unit_number, terms_and_conditions " +
                    "FROM account " +
                    "WHERE email_address = #{emailAddress}"
    )
    @ResultMap(ACCOUNT_RESULTS_MAP)
    Optional<Account> findByEmailAddress(String emailAddress);

    @Delete(
        "DELETE FROM account WHERE account_id = #{id}"
    )
    void delete(Account account);
}
