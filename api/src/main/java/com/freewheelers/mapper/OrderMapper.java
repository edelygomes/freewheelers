package com.freewheelers.mapper;

import com.freewheelers.model.Account;
import com.freewheelers.model.Order;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

public interface OrderMapper {

    String ORDER_ID_PROPERTY = "id";
    String ORDER_RESULT_MAPPING = "orderResultMapping";

    @Insert(
            "INSERT INTO reserve_order (account_id, item_id, total_price, status, payment_method, note, reservation_timestamp, item_description, item_name, item_price, item_type) " +
                    "VALUES (#{account.id}, #{item.itemId}, #{totalPrice}, #{status}, #{paymentMethod}, #{note}, #{reserveTime}, #{item.description}, #{item.name}, #{item.price}, #{item.type})"
    )
    @Options(keyProperty = ORDER_ID_PROPERTY, useGeneratedKeys = true)
    void insert(Order order);

    @Select(
        "SELECT order_id, account_id, item_id, item_name, item_description, item_price, total_price, item_type, status, payment_method, note, reservation_timestamp " +
        "FROM reserve_order " +
        "WHERE order_id = #{id}"
    )
    @Results(id= ORDER_RESULT_MAPPING, value = {
            @Result(property = ORDER_ID_PROPERTY, column="order_id"),
            @Result(property= "account", column = "account_id", javaType = Account.class,
                    one = @One(select = "com.freewheelers.mapper.AccountMapper.findById")),
            @Result(property= "item.itemId", column = "item_id"),
            @Result(property= "item.name", column = "item_name"),
            @Result(property= "item.description", column = "item_description"),
            @Result(property= "item.price", column = "item_price"),
            @Result(property= "item.type", column = "item_type"),
            @Result(property= "reserveTime", column="reservation_timestamp"),
            @Result(property= "totalPrice", column="total_price"),
            @Result(property= "paymentMethod", column="payment_method")

    })
    Optional<Order> findById(Integer id);

    @Delete(
        "DELETE FROM reserve_order WHERE order_id = #{id}"
    )
    void delete(Order order);

    @Update(
            "UPDATE reserve_order " +
                    "SET account_id=#{account.id}, item_id=#{item.itemId}, status=#{status}, note=#{note}, reservation_timestamp=#{reserveTime} " +
                    "WHERE order_id=#{id}"
    )
    void save(Order order);

    @Select(
        "SELECT order_id, account_id, total_price, item_id, item_name, item_description,item_price, item_type, status, payment_method, note, reservation_timestamp " +
        "FROM reserve_order " +
        "ORDER BY account_id, order_id DESC"
    )
    @ResultMap(ORDER_RESULT_MAPPING)
    List<Order> findAll();

    @Select(
            "SELECT order_id, account_id, item_id, total_price, item_name, item_description, item_price, item_type, status, payment_method, note, reservation_timestamp " +
            "FROM reserve_order " +
            "WHERE account_id=#{accountId}" +
            "ORDER BY order_id DESC"
    )
    @ResultMap({ORDER_RESULT_MAPPING})
    List<Order> findAllByAccountId(Integer accountId);
}
