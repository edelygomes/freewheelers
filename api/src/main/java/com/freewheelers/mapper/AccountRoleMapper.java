package com.freewheelers.mapper;

import com.freewheelers.model.AccountRole;
import org.apache.ibatis.annotations.*;
import org.springframework.dao.DataAccessException;

import java.util.Optional;

public interface AccountRoleMapper {
    String ACCOUNT_ROLE_RESULTS_MAP = "accountRoleResultsMap";

    @Insert(
        "INSERT INTO account_role (account_id, role) VALUES (#{accountId}, #{role})"
    )
    @Options(keyProperty = "id", useGeneratedKeys = true)
    void insert(AccountRole accountRole) throws DataAccessException;

    @Select(
        "SELECT account_id, role FROM account_role WHERE account_id = #{accountId}"
    )
    @Results(id = ACCOUNT_ROLE_RESULTS_MAP, value = {
        @Result(property = "id", column = "id"),
        @Result(property = "accountId", column = "account_id"),
        @Result(property = "role", column = "role"),
    })
    Optional<AccountRole> findByAccountId(Integer accountId);
}
