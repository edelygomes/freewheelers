package com.freewheelers.mapper;

import com.freewheelers.model.Item;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Optional;

public interface ItemMapper {

    String ITEM_RESULT_MAPPING = "itemResultMapping";

    @Insert(
        "INSERT INTO item (description, name, price, type, quantity) " +
        "VALUES (#{description}, #{name}, #{price}, #{type}, #{quantity})"
    )
    @Options(keyProperty = "id", keyColumn = "item_id", useGeneratedKeys = true)
    void insert(Item item);

    @Select(
        "SELECT item_id, name, price, type, quantity, description " +
        "FROM item " +
        "WHERE item_id = #{id}"
    )
    @Results(id = ITEM_RESULT_MAPPING, value = {
            @Result(property= "id", column = "item_id"),
    })
    Optional<Item> findById(Integer id);

    @Delete(
        "DELETE FROM item WHERE item_id = #{id}"
    )
    void delete(Item item);

    @Update(
        "UPDATE item " +
        "SET description=#{description}, name=#{name}, price=#{price}, type=#{type}, quantity=#{quantity} " +
        "WHERE item_id=#{id}"
    )
    void update(Item item);

    @Select(
        "SELECT item_id, name, price, type, quantity, description FROM item ORDER BY item_id"
    )
    @ResultMap(ITEM_RESULT_MAPPING)
    List<Item> findAll();

    @Select(
        "SELECT item_id, name, price, type, quantity, description FROM item WHERE quantity > 0 ORDER BY item_id"
    )
    @ResultMap(ITEM_RESULT_MAPPING)
    List<Item> findAllAvailable();

}
