package com.freewheelers.security;

import com.freewheelers.configuration.UserAccountDetailsService;
import com.freewheelers.controller.ItemResource;
import com.freewheelers.controller.PrincipalController;
import com.freewheelers.service.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest({PrincipalController.class, ItemResource.class})
class AuthenticationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserAccountDetailsService userAccountDetailsService;

    @MockBean
    ItemService itemService;

    @Test
    void loginAuthenticatesAndReturns200() throws Exception {
        String emailAddress = "user@email.com";
        String password = "password";
        UserDetails userDetails = new User(emailAddress, password, emptyList());
        when(userAccountDetailsService.loadUserByUsername(emailAddress)).thenReturn(userDetails);

        mockMvc.perform(post("/login")
            .param("username", emailAddress)
            .param("password", password))
            .andExpect(status().isOk());
    }

    @Test
    void loginReturns401WhenCredentialsAreWrong() throws Exception {
        String emailAddress = "user@email.com";
        String password = "password";
        String wrongPassword = "wrong_password";
        UserDetails userDetails = new User(emailAddress, password, emptyList());
        when(userAccountDetailsService.loadUserByUsername(emailAddress)).thenReturn(userDetails);

        mockMvc.perform(post("/login")
            .param("username", emailAddress)
            .param("password", wrongPassword))
            .andExpect(status().isUnauthorized());
    }

    @Test
    void logoutReturns200() throws Exception {
        mockMvc.perform(get("/logout"))
            .andExpect(status().isOk());
    }

    @Test
    void returns200WhenTryToAccessApiNotRequiringAuthentication() throws Exception {
        when(itemService.findAllAvailable()).thenReturn(emptyList());

        mockMvc.perform(get("/items"))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void returns200WhenTryToAccessApiAndAuthenticated() throws Exception {
        mockMvc.perform(get("/principal"))
            .andExpect(status().isOk());
    }

    @Test
    void returns401WhenTryToAccessApiAndNotAuthenticated() throws Exception {
        when(userAccountDetailsService.loadUserByUsername(any())).thenReturn(null);

        mockMvc.perform(get("/principal"))
            .andExpect(status().isUnauthorized());
    }
}
