package com.freewheelers.persistence;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.builder.AccountBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class AccountMapperTest extends MapperTestBase {
    private AccountMapper accountMapper;

    @BeforeEach
    void setUp() throws Exception {
        super.setUp();
        accountMapper = getSqlSession().getMapper(AccountMapper.class);
    }

    @Test
    void shouldInsert() throws SQLException {
        Account account = new AccountBuilder().withAccountName("Johnny Cash").build();

        accountMapper.insert(account);
        assertThat(account.getId(), is(notNullValue()));

        ResultSet resultSet = execute("select * from account where account_id = "+account.getId()+";");
        resultSet.next();
        assertAccount(account, resultSet);
    }

    @Test
    void shouldGetAccountById() {
        Account account = new AccountBuilder().withAccountName("Johnny Cash").build();

        accountMapper.insert(account);

        Account fetchedFromDB = accountMapper.findById(account.getId()).get();
        assertAccount(account, fetchedFromDB);
    }

    @Test
    void shouldGetAccountByEmailAddress() {
        Account account = new AccountBuilder().withEmailAddress("user@email.com").build();

        accountMapper.insert(account);

        Account fetchedFromDB = accountMapper.findByEmailAddress(account.getEmailAddress()).get();
        assertAccount(account, fetchedFromDB);
    }

    @Test
    void shouldReturnNullIfAnAccountIdDoesNotExist() {
        assertThat(accountMapper.findById(-1), is(Optional.empty()));
    }

    @Test
    void shouldDeleteAccount() throws SQLException {
        Account account = new AccountBuilder().build();
        accountMapper.insert(account);

        accountMapper.delete(account);

        ResultSet resultSet = execute("select count(*) as entry_count from account where account_id = "+account.getId()+";");
        resultSet.next();
        assertThat(resultSet.getInt("entry_count"), is(0));
    }

    private void assertAccount(Account expected, Account actual) {
        assertThat(expected.getId(), is(actual.getId()));
        assertThat(expected.getAccountName(), is(actual.getAccountName()));
        assertThat(expected.getPhoneNumber(), is(actual.getPhoneNumber()));
        assertThat(expected.getEmailAddress(), is(actual.getEmailAddress()));
        assertThat(expected.getPassword(), is(actual.getPassword()));
        assertThat(expected.getCountry(), is(actual.getCountry()));
    }

    private void assertAccount(Account expected, ResultSet actual) throws SQLException {
        assertThat(expected.getId(), is(actual.getInt("account_id")));
        assertThat(expected.getAccountName(), is(actual.getString("account_name")));
        assertThat(expected.getPhoneNumber(), is(actual.getString("phone_number")));
        assertThat(expected.getEmailAddress(), is(actual.getString("email_address")));
        assertThat(expected.getPassword(), is(actual.getString("password")));
        assertThat(expected.getCountry(), is(actual.getString("country")));
    }
}

