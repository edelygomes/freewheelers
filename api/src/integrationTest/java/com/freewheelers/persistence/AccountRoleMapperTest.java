package com.freewheelers.persistence;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.AccountRoleMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.AccountRole;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.model.builder.AccountRoleBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AccountRoleMapperTest extends MapperTestBase {

    private AccountRoleMapper accountRoleMapper;
    private AccountMapper accountMapper;

    @BeforeEach
    void setUp() throws Exception {
        super.setUp();
        accountRoleMapper = getSqlSession().getMapper(AccountRoleMapper.class);
        accountMapper = getSqlSession().getMapper(AccountMapper.class);
    }

    @Test
    void shouldInsertAnAccountRole() {
        Account account = new AccountBuilder().build();
        accountMapper.insert(account);

        AccountRole accountRole = new AccountRoleBuilder()
            .withAccountId(account.getId())
            .withRole("Some Role")
            .withRoleId(null)
            .build();

        accountRoleMapper.insert(accountRole);

        assertThat(accountRole.getId()).isNotNull();
    }

    @Test
    void getAllByAccountId() {
        String role = "USER";

        Account account = new AccountBuilder().build();
        accountMapper.insert(account);
        Integer accountId = account.getId();

        AccountRole accountRole = new AccountRoleBuilder()
            .withAccountId(accountId)
            .withRole(role)
            .build();
        accountRoleMapper.insert(accountRole);

        AccountRole accountRoles = accountRoleMapper.findByAccountId(accountId).get();
        assertThat(accountRoles).isNotNull();
        assertThat(accountRoles.getAccountId()).isEqualTo(accountId);
    }
}
