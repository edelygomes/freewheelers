package com.freewheelers.persistence;

import com.freewheelers.mapper.AccountMapper;
import com.freewheelers.mapper.ItemMapper;
import com.freewheelers.mapper.OrderMapper;
import com.freewheelers.model.Account;
import com.freewheelers.model.Item;
import com.freewheelers.model.Order;
import com.freewheelers.model.PaymentMethod;
import com.freewheelers.model.builder.AccountBuilder;
import com.freewheelers.model.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderMapperTest extends MapperTestBase {

    private OrderMapper orderMapper;
    private Account account;
    private Item item;
    private AccountMapper accountMapper;
    private ItemMapper itemMapper;

    @BeforeEach
    void setUp() throws Exception {
        super.setUp();
        orderMapper = getSqlSession().getMapper(OrderMapper.class);

        accountMapper = getSqlSession().getMapper(AccountMapper.class);
        account = new AccountBuilder().build();
        accountMapper.insert(account);

        itemMapper = getSqlSession().getMapper(ItemMapper.class);
        item = new ItemBuilder().build();
        itemMapper.insert(item);

    }

    private Order someOrderDetail() { return new Order(account, item, new Date(), BigDecimal.valueOf(100.99), PaymentMethod.CREDIT_CARD); }

    private void assertReserveOrder(Order expected, Order actual) {
        assertThat(expected.getId(), is(actual.getId()));
        assertThat(expected.getAccount().getId(), is(actual.getAccount().getId()));
        assertThat(expected.getNote(), is(actual.getNote()));
        assertThat(expected.getReserveTime(), is(actual.getReserveTime()));
        assertThat(expected.getStatus(), is(actual.getStatus()));
        assertThat(expected.getPaymentMethod(), is(actual.getPaymentMethod()));
        assertThat(expected.getTotalPrice(), is(actual.getTotalPrice()));

        assertThat(actual.getItem().getItemId(), is(expected.getItem().getItemId()));
        assertThat(actual.getItem().getDescription(), is(expected.getItem().getDescription()));
        assertThat(actual.getItem().getName(), is(expected.getItem().getName()));
        assertThat(actual.getItem().getPrice(), is(expected.getItem().getPrice()));
        assertThat(actual.getItem().getType(), is(expected.getItem().getType()));
    }

    private void assertReserveOrder(Order expected, ResultSet actual) throws SQLException {
        assertThat(expected.getId(), is(actual.getInt("order_id")));
        assertThat(expected.getAccount().getId(), is(actual.getInt("account_id")));
        assertThat(expected.getNote(), is(actual.getString("note")));
        assertThat(expected.getPaymentMethod().toString(), is(actual.getString("payment_method")));
        assertThat(expected.getReserveTime(), is(actual.getTimestamp("reservation_timestamp")));
        assertThat(expected.getStatus().toString(), is(actual.getString("status")));
        assertThat(expected.getTotalPrice().toString(), is(actual.getString("total_price")));

        assertThat(expected.getItem().getItemId(), is(actual.getInt("item_id")));
        assertThat(expected.getItem().getDescription(), is(actual.getString("item_description")));
        assertThat(expected.getItem().getName(), is(actual.getString("item_name")));
        assertThat(expected.getItem().getPrice(), is(actual.getBigDecimal("item_price")));
        assertThat(expected.getItem().getType().toString(), is(actual.getString("item_type")));
    }

    @Test
    void shouldInsertAnOrder() throws SQLException {
        Order tobeInserted = someOrderDetail();

        orderMapper.insert(tobeInserted);
        assertThat(tobeInserted, is(not(nullValue())));

        ResultSet resultSet = execute("select * from reserve_order where order_id = "+tobeInserted.getId()+";");
        resultSet.next();
        assertReserveOrder(tobeInserted, resultSet);
    }

    @Test
    void shouldGetAnOrder() {
        Order tobeInserted = someOrderDetail();

        orderMapper.insert(tobeInserted);
        Optional<Order> fetched = orderMapper.findById(tobeInserted.getId());

        assertTrue(fetched.isPresent());
        assertReserveOrder(tobeInserted, fetched.get());
    }

    @Test
    void shouldDeleteAnOrder() throws SQLException {
        Order tobeDeleted = someOrderDetail();
        orderMapper.insert(tobeDeleted);

        orderMapper.delete(tobeDeleted);

        ResultSet resultSet = execute("select count(*) as entry_count from reserve_order where order_id = "+tobeDeleted.getId()+";");
        resultSet.next();
        assertThat(resultSet.getInt("entry_count"), is(0));
    }

    @Test
    void shouldUpdateAnOrder() throws SQLException {
        Order toBeUpdated = someOrderDetail();
        toBeUpdated.setNote("");
        orderMapper.insert(toBeUpdated);

        toBeUpdated.setNote("A very important note.");
        orderMapper.save(toBeUpdated);

        ResultSet resultSet = execute("select * from reserve_order where order_id = "+toBeUpdated.getId()+";");
        resultSet.next();
        assertReserveOrder(toBeUpdated, resultSet);
    }

    @Test
    void shouldFindAllOrderDetails() {
        int before = orderMapper.findAll().size();
        orderMapper.insert(someOrderDetail());

        List<Order> all = orderMapper.findAll();

        assertThat(all.size(), is(before + 1));
    }

    @Test
    void shouldFindAllOrdersForAnAccount() {
        Account anotherAccount = new AccountBuilder().build();
        accountMapper.insert(anotherAccount);

        int before = orderMapper.findAllByAccountId(account.getId()).size();

        orderMapper.insert(new Order(account, item, new Date(), BigDecimal.valueOf(100.00), PaymentMethod.CASH));
        orderMapper.insert(new Order(account, item, new Date(), BigDecimal.valueOf(100.00), PaymentMethod.CREDIT_CARD));
        orderMapper.insert(new Order(anotherAccount, item, new Date(), BigDecimal.valueOf(100.00), PaymentMethod.CASH));

        List<Order> all = orderMapper.findAllByAccountId(account.getId());

        assertThat(all.size(), is(before + 2));
    }
}
