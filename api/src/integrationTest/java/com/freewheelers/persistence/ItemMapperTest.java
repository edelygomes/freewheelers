package com.freewheelers.persistence;

import com.freewheelers.mapper.ItemMapper;
import com.freewheelers.model.Item;
import com.freewheelers.model.ItemType;
import com.freewheelers.model.builder.ItemBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static java.math.BigDecimal.valueOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class ItemMapperTest extends MapperTestBase {

    public static final long AVAILABLE = 1L;
    public static final long UNAVAILABLE = 0L;
    private ItemMapper itemMapper;

    @Override
    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        itemMapper = getSqlSession().getMapper(ItemMapper.class);
    }

    @Test
    void shouldInsertANewItem() throws SQLException {
        Item item = new ItemBuilder().withItemId(null).build();
        itemMapper.insert(item);
        assertThat(item.getId(), is(not(nullValue())));

        ResultSet resultSet = execute("select * from item where item_id = "+item.getId()+";");
        resultSet.next();
        assertItem(item, resultSet);
    }

    @Test
    void shouldFetchAnItemById() {
        Item item = new ItemBuilder().withName("Awesome Item").build();
        itemMapper.insert(item);
        Optional<Item> fetched = itemMapper.findById(item.getId());

        assertThat(item, is(fetched.get()));
    }

    @Test
    void shouldReturnNullIfAnItemIdDoesNotExist() {
        assertThat(itemMapper.findById(-1), is(Optional.empty()));
    }

    @Test
    void shouldDeleteItem() throws SQLException {
        Item item = new ItemBuilder().build();
        itemMapper.insert(item);

        itemMapper.delete(item);

        ResultSet resultSet = execute("select count(*) as entry_count from item where item_id = "+item.getId()+";");
        resultSet.next();
        assertThat(resultSet.getInt("entry_count"), is(0));
    }

    @Test
    void shouldUpdateAnItem() throws SQLException {
        Item item = new ItemBuilder().build();
        itemMapper.insert(item);

        item.setPrice(valueOf(99.99));
        item.setName("another name");
        item.setDescription("another description");
        item.setQuantity(UNAVAILABLE);
        item.setType(ItemType.FRAME);
        itemMapper.update(item);

        ResultSet resultSet = execute("select * from item where item_id = "+item.getId()+";");
        resultSet.next();
        assertItem(item, resultSet);
    }

    @Test
    void shouldFindAllItems() {
        int before = itemMapper.findAll().size();

        itemMapper.insert(new ItemBuilder().build());

        assertThat(itemMapper.findAll().size(), is(before + 1));
    }

    @Test
    void shouldFindAllAvailableItems() {
        int before = itemMapper.findAllAvailable().size();

        itemMapper.insert(new ItemBuilder().withQuantity(AVAILABLE).build());
        itemMapper.insert(new ItemBuilder().withQuantity(UNAVAILABLE).build());
        itemMapper.insert(new ItemBuilder().withQuantity(UNAVAILABLE).build());

        assertThat(itemMapper.findAllAvailable().size(), is(before + 1));
    }

    private void assertItem(Item expected, ResultSet actual) throws SQLException {
        assertThat(expected.getId(), is(actual.getInt("item_id")));
        assertThat(expected.getName(), is(actual.getString("name")));
        assertThat(expected.getDescription(), is(actual.getString("description")));
        assertThat(expected.getPrice(), is(actual.getBigDecimal("price")));
        assertThat(expected.getQuantity(), is(actual.getLong("quantity")));
        assertThat(expected.getType().name(), is(actual.getString("type")));
    }
}
