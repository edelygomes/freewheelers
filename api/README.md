# Freewheelers API

## Tech Stack

* Java 11 __needs to be installed__ and __JAVA_HOME__ set
* Postgres 12 __needs to be installed__
* Spring Boot
* MyBatis
* Gradle
* JUnit5


## Getting Started

1. Start postgres server and setup database
    * check that the freewheelers superuser exists
      run `psql -c '\du'`
    * if user does not exist
      run `createuser -s freewheelers` (no need to set a password, defaults to 'postgres')
    * create database "freewheelers"
      run `./gradlew createDb migrateDb`
    * to add some data (optional)
      run `./gradlew loadSeedData`
    * to open a terminal
       run `psql -h localhost -U freewheelers --password -d freewheelers`
        (enter`\q` to quit the terminal)

2. Check out existing tasks
    * go to the project root folder
    * `./gradlew tasks`

3. Run local build
    * `./gradlew clean build`
    *  __NOTE__: The first time this is invoked Gradle will automatically download 
    all the dependencies required by the application code, and cache the
    downloaded files for future use.  This will take a few minutes.
    
4. Check testing coverage
    * open build/reports/coverage/unit/index.html

5. Start the application
    * `./gradlew bootRun`
    * go to http://localhost:8080/items

## Setting Up IntelliJ 

1. Open IntelliJ IDEA
2. Click "Import Project”
  a. Choose the “api” folder in the project that you just checked out from source control
  b. On the next screen, select "Import project from external model" and select "Gradle"
  c. On the next screen, leave the defaults as they are and click "Finish"
3. If you see a popup in which IntelliJ wants to use your confidential information in your keychain, click “Allow” or “Always Allow”
4. If you see an error with the Java SDK being set incorrectly, follow the instructions in IntelliJ to set the Project SDK to Java 11
5. If IntelliJ pops up a dialogue "Unregistered VCS root detected", select "Add root".
6. To verify that the import was successful just run any of the tests in IntelliJ.  If everything is green, you are good to go!

## Debugging in IntelliJ
1. Navigate to [FreewheelersApplication.java](src/main/java/com/freewheelers/FreewheelersApplication.java)
2. Select `Debug FreeWheelersApplication` and wait for the application to start in debug mode
3. Set breakpoints in the application code 

## Linting
1. PMD and Spotbugs have been setup for this project.
2. PMD rules have been declared: `pmd/RuleSets.xml`
3. Reports can be found: `build/reports/spotbugs`, `build/reports/pmd`

## Tips
#### To kill a Java process that is blocking port 8080:

```shell
kill $(lsof -i :8080 -t)
```
#### To use the IntelliJ runner rather than the Gradle runner: 
- Edit configurations > remove all existing Gradle configurations for tests
- Preferences > Build, Execution, Deployment > Build Tools > Gradle
   - Build and run using: IntelliJ Runner
   - Run tests using: Choose per test or IntelliJ runner
 - Do not re-run using existing RUN window as this will restore the Gradle configuration. 
 Instead, use Ctrl + Shift + R or manually click the tests to run via IntelliJ.  
   
In order to run the integration tests using the IntelliJ runner, we need to set up the environment 
variable ENV=test within our JUnit test runner configuration. This can be applied to the JUnit template.

Known issue, inconsistently reproduced and introduced:
- There seems to be some inconsistency created within our JSON -> object mapping when using the
IntelliJ test runner, leading to breaking MockMVC tests. Tests will continue to pass using the Gradle runner. Exception: 
```
Request processing failed; nested exception is org.springframework.http.converter.HttpMessageConversionException 
Type definition error: nested exception is com.fasterxml.jackson.databind.exc.InvalidDefinitionException
``` 

- Workaround: introduce setters and default constructors for any models breaking tests.  