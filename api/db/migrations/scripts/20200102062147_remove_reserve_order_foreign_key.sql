-- // remove reserve order foreign key
-- Migration SQL that makes the change goes here.

ALTER TABLE reserve_order DROP CONSTRAINT "reserve_order_item_id_fkey";

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE reserve_order ADD FOREIGN KEY (item_id) REFERENCES item(item_id);

