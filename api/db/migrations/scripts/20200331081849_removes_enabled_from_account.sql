-- // removes enabled from account
-- Migration SQL that makes the change goes here.

ALTER TABLE account DROP COLUMN enabled;

-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE account ADD COLUMN enabled NOT NULL DEFAULT true;

