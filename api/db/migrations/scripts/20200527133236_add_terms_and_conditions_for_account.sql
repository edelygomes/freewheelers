-- // add_terms_and_conditions_for_account
-- Migration SQL that makes the change goes here.

ALTER TABLE account
ADD COLUMN terms_and_conditions boolean NOT NULL DEFAULT FALSE;

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE account
DROP COLUMN terms_and_conditions;
