-- // add_unique_clause_for_account_id_in_account_role
-- Migration SQL that makes the change goes here.

ALTER TABLE account_role ADD CONSTRAINT unique_account_id UNIQUE (account_id);

-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE account_role DROP CONSTRAINT unique_account_id;


