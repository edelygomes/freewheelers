-- // add item fields for order
-- Migration SQL that makes the change goes here.

ALTER TABLE reserve_order
ADD COLUMN payment_method CHARACTER VARYING(255) NOT NULL DEFAULT 'CASH';


-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE reserve_order
DROP COLUMN payment_method;