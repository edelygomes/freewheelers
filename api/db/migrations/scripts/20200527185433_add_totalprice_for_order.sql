-- // add_totalprice_for_order
-- Migration SQL that makes the change goes here.
ALTER TABLE reserve_order
ADD COLUMN total_price NUMERIC(19,2) NOT NULL DEFAULT 0;

UPDATE reserve_order a SET total_price = (SELECT (item_price / 0.8) FROM reserve_order b WHERE a.order_id = b.order_id );

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE reserve_order
DROP COLUMN total_price;


