-- // move account role over to reference only account email
-- Migration SQL that makes the change goes here.

ALTER TABLE account_role ADD FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE account_role DROP COLUMN account_name;

-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE account_role ADD COLUMN account_name CHARACTER VARYING(255);

UPDATE account_role
SET account_name = account.account_name
FROM account
WHERE account.account_id = account_role.account_id;

ALTER TABLE account_role DROP COLUMN account_id;
ALTER TABLE account_role ALTER COLUMN account_name SET NOT NULL;