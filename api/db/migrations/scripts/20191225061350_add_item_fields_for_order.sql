-- // add item fields for order
-- Migration SQL that makes the change goes here.

ALTER TABLE reserve_order
ADD COLUMN item_description CHARACTER VARYING(255),
ADD COLUMN item_name CHARACTER VARYING(255),
ADD COLUMN item_price NUMERIC(19,2),
ADD COLUMN item_type CHARACTER VARYING(255);


UPDATE reserve_order
SET item_description = item.description,
item_name = item.name,
item_price = item.price,
item_type = item.type
FROM item
WHERE reserve_order.item_id = item.item_id;


-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE reserve_order
DROP COLUMN item_description,
DROP COLUMN item_name,
DROP COLUMN item_price,
DROP COLUMN item_type;

