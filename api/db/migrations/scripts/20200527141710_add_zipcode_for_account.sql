-- // add_zipcde_for_account
-- Migration SQL that makes the change goes here.

ALTER TABLE account
ADD COLUMN zipcode CHARACTER VARYING(12) NOT NULL DEFAULT '';

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE account
DROP COLUMN zipcode;
