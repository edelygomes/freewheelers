-- // add_street_number_for_account
-- Migration SQL that makes the change goes here.

ALTER TABLE account
ADD COLUMN street_number CHARACTER VARYING(25) NOT NULL DEFAULT '';

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE account
DROP COLUMN street_number;
