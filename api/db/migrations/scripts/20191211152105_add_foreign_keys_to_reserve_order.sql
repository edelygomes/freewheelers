-- // add foreign keys to reserve order
-- Migration SQL that makes the change goes here.

ALTER TABLE reserve_order ADD FOREIGN KEY (account_id) REFERENCES account(account_id);
ALTER TABLE reserve_order ADD FOREIGN KEY (item_id) REFERENCES item(item_id);

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE reserve_order DROP CONSTRAINT "reserve_order_account_id_fkey";
ALTER TABLE reserve_order DROP CONSTRAINT "reserve_order_item_id_fkey";

