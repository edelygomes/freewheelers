-- // move account role to reference account email
-- Migration SQL that makes the change goes here.
ALTER TABLE account_role ADD COLUMN account_id INTEGER;

UPDATE account_role
SET account_id = account.account_id
FROM account
WHERE account.account_name = account_role.account_name;

-- //@UNDO
-- SQL to undo the change goes here.

ALTER TABLE account_role DROP COLUMN account_id;

