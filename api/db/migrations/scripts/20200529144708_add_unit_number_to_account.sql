-- // add_unit_number_for_account
-- Migration SQL that makes the change goes here.

ALTER TABLE account
ADD COLUMN unit_number CHARACTER VARYING(25) NOT NULL DEFAULT '';

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE account
DROP COLUMN unit_number;
