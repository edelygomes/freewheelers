-- // add_country_for_account
-- Migration SQL that makes the change goes here.

ALTER TABLE account
ADD COLUMN country CHARACTER VARYING(255) NOT NULL DEFAULT '';

-- //@UNDO
-- SQL to undo the change goes here.
ALTER TABLE account
DROP COLUMN country;


