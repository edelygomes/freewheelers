# Freewheelers 

## Applications

* api
* ui
* functional-tests

## Architecture

![Component View](./application-architecture/dist/component-view-web-service.png "Component View")

## Follow the instructions

* [api/README.md](api/README.md)
* [ui/README.md](ui/README.md)
* [functional-test/README.md](functional-test/README.md)

## Run All Tests

If you already have everything set up
```
./run-all-tests.sh
```
