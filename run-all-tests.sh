#!/bin/bash

set -e

REPOSITORY_DIR=$(pwd)

cd $REPOSITORY_DIR/ui
npm run test:once

cd $REPOSITORY_DIR/api
./gradlew clean build

cd $REPOSITORY_DIR/functional-test
HEADLESS=true ./scripts/run-tests.sh
